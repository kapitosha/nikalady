<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "outmessg".
 *
 * The followings are the available columns in table 'outmessg':
 *
 * @property integer $Id
 * @property string $date
 * @property string $subject
 * @property string $text
 * @property string $email
 * @property integer $idmessg
 * @property string $note
 */
class Outmessg extends BaseOutmessg
{
    use RequestOfModel;
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseOutmessg the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'outmessg';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['text, email, subject', 'required'],
			['idmessg', 'numerical', 'integerOnly' => true],
			['subject', 'length', 'max' => 100],
			['email', 'length', 'max' => 200],
      ['email', 'email'],    
			['date,note', 'safe'],
		];
    }
       /**
     * @see CModel::relation()
     */
    public function relations()
    {
        return [
            'inmessgR' => [self::BELONGS_TO, 'inmessg',    'idmessg',
                'joinType' => 'INNER JOIN', 
                //'on' => 'inmessgR.Id=t.idmessg',
                'together'=>true,],
        ];
    }
	
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'Id'),
			'date' => Yii::t('application', 'date'),
			'subject' => Yii::t('application', 'subject'),
			'text' => Yii::t('application', 'text'),
			'email' => Yii::t('application', 'Email'),
			'idmessg' => Yii::t('application', 'idmessg'),
			'note' => Yii::t('application', 'note'),
		];
    }
    
    public function sendMessageToUser(Outmessg $model)
    {
        $message = new YiiMailMessage;
        $message->view = 'sendmail';
        $message->setSubject($model->subject);
        $message->setBody(['model' => $model], 'text/html');
        $message->addTo($model->email, ' сайт Ника Леди');
        $message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);
        Yii::app()->mail->send($message);
    }
    
//    public function sendMessageToAdmin(Inmessg $model)
//    {
//        $message = new YiiMailMessage;
//        $message->view = 'feedback';
//        $message->setSubject('сообщение от '.$model->email);
//        $message->setBody(['model' => $model], 'text/html');
//        $message->addTo(Yii::app()->params['robotEmailTo']);
//        $message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);
//
//        if (Yii::app()->mail->send($message)) {
//          //якщо повідомлення відправлена встановлюю йому відповідний статус = 1
//          $model->idstatus=2;
//          $model->save();
//        };
//    }       
    
  public function validateEmail($attribute, $value)
    {
      $length = mb_strlen($this->email);
      if ($length<3) {
        $this->addError('email', 'email слишком короткий');
      }
      $instr=stristr($this->email,'@');
      $length = mb_strlen($instr);
      if ( $instr===FALSE ) {
        $this->addError('email', 'Введен неверный email');
      } elseif ($length<2) {
        $this->addError('email', 'Введен неверный email');
      } 
    } 
    /**
     * @return CActiveDataProvider
     */
    public function dataProvider()
    {
        $criteria = new CDbCriteria;
        //$criteria->order = 'Id';
        $criteria->with=['inmessgR' => ['together' => true],
                    //=>[
                    //      'joinType'=>'INNER JOIN'],
                         // 'compare'=>['inmessgR.name',$this->inmessgR.name, true], 
                        ];

        $criteria->compare('t.Id', $this->Id, true);
        $criteria->compare('t.date', $this->date, true);
        $criteria->compare('t.subject', $this->subject, true);
        $criteria->compare('t.text', $this->text, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.idmessg', $this->idstatus, true);
        $criteria->compare('t.note', $this->note, true);
        // $criteria->compare('inmessgR.name', $this->inmessgR['name'], true);
          return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => 't.Id',
            ]
        ]);
}

}