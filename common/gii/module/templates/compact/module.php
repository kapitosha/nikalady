<?php echo "<?php\n"; ?>
/**
* @link http://web-systems.com.ua/
* @link http://price-r.ru/
* @copyright Copyright (c) 2013 - 2014 price-R.ru
* @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
*/

/**
* @author <?php echo Yii::app()->params['author'] . "\n"; ?>
* @since 0.1
*/
class <?php echo $this->moduleClass; ?> extends CWebModule
{
	public function init()
	{
		// import the module-level models and controllers
		$this->setImport([
			'<?php echo $this->moduleID; ?>.models.*',
			'<?php echo $this->moduleID; ?>.controllers.*',
		]);
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		} else {
			return false;
		}
	}
}
