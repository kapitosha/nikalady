<?php
$this->pageTitle = $model->name . ', заказ обратного звонка';
?>
<p>Был заказан обратный звонок от : <?= CHtml::encode($model->name); ?></p>
<br/>
<p>Пожалуста перезвоните мне на номер : <?= CHtml::encode($model->phonenumber); ?></p>
<br/>
<p>Если вы этого не делали, просто удалите это письмо.</p>
<br/>
