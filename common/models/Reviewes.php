<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "reviews".
 *
 * The followings are the available columns in table 'reviews':
 *
 * @property integer $Id
 * @property string $name
 * @property string $text
 */
class Reviewes extends BaseReviewes
{
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseReviews the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'reviewes';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['text', 'required'],
			['name', 'length', 'max' => 50],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'Id'),
			'name' => Yii::t('application', 'fbname'),
			'text' => Yii::t('application', 'text'),
		];
    }

    public function sendMessageToAdmin(Reviewes $model) 

    {
        $message = new YiiMailMessage;
        $message->view = 'reviewes';
        $message->setSubject('О вас оставили отзыв!!!');
        $message->setBody(['model' => $model], 'text/html');
        $message->addTo(Yii::app()->params['robotEmailTo']);
        $message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);
        
        
        Yii::app()->mail->send($message);
    }
}