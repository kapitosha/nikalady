<?php
/**
 * @link http://web-systems.com.ua/
 * @link http://price-r.ru/
 * @copyright Copyright (c) 2013 - 2014 price-R.ru
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * Перегляд сторінок, альтернатива CViewAction тільки сторінки в базі
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class ViewAction extends CAction
{
	/**
	 * @var string файл представлення по замовчуванню
	 */
	public $defaultView = 'page';

	/**
	 * @param $alias string аліас сторінки
	 * @throws CHttpException виняток якщо сторінка не існує
	 */
	public function run($alias)
	{
		$page = Page::model()->loadPageByAlias($alias);
		if (!isset($page)) {
			throw new CHttpException(404, Yii::t('yii', 'The requested alias "{alias}" was not found.', ['{alias}' => $alias]));
		}
		$this->controller->render($this->defaultView, ['page' => $page]);
	}
} 