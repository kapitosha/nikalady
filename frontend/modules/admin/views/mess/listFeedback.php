<?php $this->breadcrumbs = [
	'Словарь: Работа с обратными звонками',
]; ?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', [
	'type' => 'primary',
	'buttons' => [
	   ['label' => 'Добавить обратный звонок', 'url' => ['AddFeedback']],
	],
]);?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider->dataProvider(),
    'fixedHeader' => true,
    'sortableRows'=>true,
    'filter' => $dataProvider,
    'type'=>'striped bordered condensed',
    'columns' => $columns,
    'responsiveTable' => true,
    'pager' => [
        'class' => 'bootstrap.widgets.TbPager',
        'displayFirstAndLast' => true
    ]
]); ?>
