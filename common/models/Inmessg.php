<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "inmessg".
 *
 * The followings are the available columns in table 'inmessg':
 *
 * @property integer $Id
 * @property string $date
 * @property string $subject
 * @property string $text
 * @property string $email
 * @property integer $idstatus
 * @property string $note
 */
class Inmessg extends BaseInmessg
{
    use RequestOfModel;
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return Inmessg the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'inmessg';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
      return [
        ['text, email, subject', 'required'],
        ['subject', 'length', 'max' => 100],
        ['email', 'length', 'max' => 200],
        ['email', 'email'],
        //['email', 'validateEmail'],
        ['date,note', 'safe'],
        ['idstatus','numerical','integerOnly' => true],   
      ];
    }
      /**
     * @see CModel::relation()
     */
    public function relations()
    {
        return [
            'statuS' => [self::BELONGS_TO, 'status',    'idstatus',
                'joinType' => 'INNER JOIN', 
                //'on' => 'statuS.Id=t.idstatus',
                'together'=>true,],
        ];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'Id'),
			'date' => Yii::t('application', 'date'),
			'subject' => Yii::t('application', 'subject'),
			'text' => Yii::t('application', 'text'),
			'email' => Yii::t('application', 'Email'),
			'idstatus' => Yii::t('application', 'idstatus'),
                        'note' => Yii::t('application', 'note'),
		];
    }
    
    protected function sendMessageToUser(Inmessg $model)
    {
        $message = new YiiMailMessage;
        $message->view = 'user/register';
        $message->setSubject('сообщение от'.$model->email);
        $message->setBody(['model' => $model], 'text/html');
        $message->addTo(Yii::app()->params['robotEmailTo'], $model->contactPerson);
        $message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);
        Yii::app()->mail->send($message);
    }
    
    public function sendMessageToAdmin(Inmessg $model)
    {
        $message = new YiiMailMessage;
        $message->view = 'feedback';
        $message->setSubject('сообщение от '.$model->email);
        $message->setBody(['model' => $model], 'text/html');
        $message->addTo(Yii::app()->params['robotEmailTo']);
        $message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);

        if (Yii::app()->mail->send($message)) {
          //якщо повідомлення відправлена встановлюю йому відповідний статус = 1
          $model->idstatus=2;
          $model->save();
        };
    }       
    
  public function validateEmail($attribute, $value)
    {
      $length = mb_strlen($this->email);
      if ($length<3) {
        $this->addError('email', 'email слишком короткий');
      }
      $instr=stristr($this->email,'@');
      $length = mb_strlen($instr);
      if ( $instr===FALSE ) {
        $this->addError('email', 'Введен неверный email');
      } elseif ($length<2) {
        $this->addError('email', 'Введен неверный email');
      } 
    } 
    /**
     * @return CActiveDataProvider
     */
    public function dataProvider()
    {
        $criteria = new CDbCriteria;
        //$criteria->order = 'Id';
        $criteria->with=['statuS' => ['together' => true],
                    //=>[
                    //      'joinType'=>'INNER JOIN'],
                         // 'compare'=>['statuS.name',$this->statuS.name, true], 
                        ];

        $criteria->compare('t.Id', $this->Id, true);
        $criteria->compare('t.date', $this->date, true);
        $criteria->compare('t.subject', $this->subject, true);
        $criteria->compare('t.text', $this->text, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.idstatus', $this->idstatus, true);
        $criteria->compare('t.note', $this->note, true);
        // $criteria->compare('statuS.name', $this->statuS['name'], true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => 't.Id',
            ]
        ]);
    }
    
  /**
   * устанавливаем параметры фильтрации  на обьект модели dataProvider
   * @param type $dataProvider
   */  
  public function setFilter($dataProvider) {
		$dataProvider->unsetAttributes(); // clear any default values

      if(isset($_GET[get_class($dataProvider)])) {
        foreach ($_GET[get_class($dataProvider)] as $attr => $val) {
          $dataProvider->$attr = $val;
        }
      }    
    }
        
    
}

