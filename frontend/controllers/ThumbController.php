<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * Створення thumbs
 *
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class ThumbController extends Controller
{
	/**
	 * Resize the image
	 * @param $path string url to image, for example : "/files/profile/my.img"
	 * @param $width int new width image
	 * @param $height int new height image
	 */
	public function actionResize($path, $width, $height)
	{
		Thumb::model()->getPathImageWithResize($path, $width, $height);
	}
} 