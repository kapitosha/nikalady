<?php
/**
 * @link http://web-systems.com.ua/
 * @link http://price-r.ru/
 * @copyright Copyright (c) 2013 price-R.ru
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class WebUser extends CWebUser
{
    /**
     * @var null flowing values for result of model
     */
    private $_model = null;

	public function getIsAccepted()
	{
		if ($this->role != User::ROLE_USER) {
			return true;
		}
		if($user = $this->getModel()){
			return $user->acceptUserData == true;
		}
	}

	/**
	 * @return bool чи є це підтверджений користувач (роль юзер)?
	 */
	public function getIsAcceptedUser()
	{
		return $this->isAccepted && ($this->role == User::ROLE_USER);
	}

	/**
	 * @return bool чи це оператор / адмін?
	 */
	public function getIsNotUser()
	{
		return !$this->isGuest && ($this->role != User::ROLE_USER);
	}

    /**
     * Receiving username of the current user.
     *
     * @return array|mixed|null
     */
    public function getUsername()
    {
        if ($user = $this->getModel()) {
            return $user->email;
        }
    }
    
    public function getFullOrganizationName()
    {
        if ($user = $this->getModel()) {
          if (!empty($user->typeOrganization))
             {return User::model()->listTypes($user->typeOrganization) . ' "' . $user->nameOrganization . '"';}
            else { if (!empty($user->nameOrganization)) {return '"' . $user->nameOrganization . '"'; } 
                        else { return $user->email;}
              
                  }
        }
    }


	public function getRole() {
		if($user = $this->getModel()){
			return $user->role;
		}
	}

    /**
     * Data acquisition from base
     *
     * @return CActiveRecord|null
     */
    private function getModel()
    {
        if (!$this->isGuest && $this->_model === null) {
            $this->_model = User::model()->findByPk($this->id, ['select' => ['email', 'role', 'acceptUserData', 'typeOrganization','nameOrganization']]);
        }
        return $this->_model;
    }
    
    
	/**
	 * Logs in a user.
	 *
	 * The user identity information will be saved in storage that is
	 * persistent during the user session. By default, the storage is simply
	 * the session storage. If the duration parameter is greater than 0,
	 * a cookie will be sent to prepare for cookie-based login in future.
	 *
	 * Note, you have to set {@link allowAutoLogin} to true
	 * if you want to allow user to be authenticated based on the cookie information.
	 *
	 * @param IUserIdentity $identity the user identity (which should already be authenticated)
	 * @param integer $duration number of seconds that the user can remain in logged-in status. Defaults to 0, meaning login till the user closes the browser.
	 * If greater than 0, cookie-based login will be used. In this case, {@link allowAutoLogin}
	 * must be set true, otherwise an exception will be thrown.
	 * @return boolean whether the user is logged in
	 */
	public function login($identity,$duration=0)
	{
		$id=$identity->getId();
		$states=$identity->getPersistentStates();
		if($this->beforeLogin($id,$states,false))
		{
          //print_r($_SESSION);
			$this->changeIdentity($id,$identity->getName(),$states);
          //print_r($_SESSION);
			if($duration>0)
			{
				if($this->allowAutoLogin)
					$this->saveToCookie($duration);
				else
					throw new CException(Yii::t('yii','{class}.allowAutoLogin must be set true in order to use cookie-based authentication.',
						array('{class}'=>get_class($this))));
			}

             
			if ($this->absoluteAuthTimeout)
				$this->setState(self::AUTH_ABSOLUTE_TIMEOUT_VAR, time()+$this->absoluteAuthTimeout);
            //echo $this->getId().' - '.$this->getName().$this->getIsGuest();
            
			$this->afterLogin(false);
            
            //echo $this->getId().' - '.$this->getName().$this->getIsGuest();exit;
            
            //echo $id.' '.$identity->getName().' '.$this->absoluteAuthTimeout.' '.$this->getIsGuest(); print_r($states);//exit;
		}
		return !$this->getIsGuest();
	}    
    
}