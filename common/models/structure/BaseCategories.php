<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "categories".
 *
 * The followings are the available columns in table 'categories':
 *
 * @property integer $Id
 * @property string $name
 * @property string $catid
 */
class BaseCategories extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseCategories the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'categories';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['name, catid', 'length', 'max' => 100],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'Id'),
			'name' => Yii::t('application', 'name'),
			'catid' => Yii::t('application', 'shname'),
		];
    }
    
    
    
    
    
}

