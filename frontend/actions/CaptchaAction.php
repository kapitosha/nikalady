<?php
/**
 * @link http://web-systems.com.ua/
 * @link http://price-r.ru/
 * @copyright Copyright (c) 2013 - 2014 price-R.ru
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * Переопредільоний action обробки каптчі, з урахуваням ajax валідації
 *
 * @see CCaptchaAction
 *
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class CaptchaAction extends CCaptchaAction
{
	/**
	 * @see CCaptchaAction::minLength
	 */
	public $minLength = 3;
	/**
	 * @see CCaptchaAction::maxLength
	 */
	public $maxLength = 5;

	/**
	 * @see CCaptchaAction::validate()
	 */
	public function validate($input, $caseSensitive)
	{
		$code = $this->getVerifyCode();
		$valid = $caseSensitive ? ($input === $code) : strcasecmp($input, $code) === 0;
		if (Yii::app()->request->isAjaxRequest) {
			return $valid;
		}
		$session = Yii::app()->session;
		$session->open();
		$name = $this->getSessionKey() . 'count';
		$session[$name] = $session[$name] + 1;
		if ($session[$name] > $this->testLimit && $this->testLimit > 0) {
			$this->getVerifyCode(true);
		}
		return $valid;
	}

	/**
	 * Generates a new verification code.
	 *
	 * @return string the generated verification code
	 */
	protected function generateVerifyCode()
	{
		if ($this->minLength < 3) {
			$this->minLength = 3;
		}
		if ($this->maxLength > 20) {
			$this->maxLength = 20;
		}
		if ($this->minLength > $this->maxLength) {
			$this->maxLength = $this->minLength;
		}
		$length = rand($this->minLength, $this->maxLength);

		$letters = '1234567890';
		$code = '';
		for ($i = 0; $i < $length; ++$i) {
			$code .= $letters[rand(0, strlen($letters) - 1)];
		}
		return $code;
	}
} 