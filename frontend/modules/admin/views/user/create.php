<?php
$this->breadcrumbs += array(Yii::t('pm','Compose message')); 
?> 

<h2><?php echo Yii::t('pm','Compose message'); ?></h2>

<p>
	<?php echo Yii::t('pm','Compose message for user'); echo ($model->recipient_id==null) ? ' ВСЕМ' : ' '.$model->recipient->getFullOrganizationName() ;//.CHtml::encode($model->recipientName); ?>
</p>

<?php $this->renderPartial('_form', array('model' => $model)); ?>

