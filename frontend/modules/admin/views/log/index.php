<?php $this->breadcrumbs = [
	'Логи',
]; ?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
	'dataProvider' => $dataProvider->dataProvider(),
    'filter' => $dataProvider,
	'fixedHeader' => true,
	'type'=>'striped bordered condensed',
	'columns' => $columns,
	'responsiveTable' => true,
	'pager' => [
		'class' => 'bootstrap.widgets.TbPager',
		'displayFirstAndLast' => true
	]
]); ?>
