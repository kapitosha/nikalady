<?php
return CMap::mergeArray(
    require(__DIR__ . '/../../common/config/main.php'),
    [
        'aliases' => [
            'frontend' => 'application',
            'www' => 'frontend.www',
            'wwwfiles' => 'frontend.www.files',
            'GridView' => 'frontend.widgets.GridView',
            'bootstrap' => realpath(__DIR__ . '/../../common/vendor/clevertech/yii-booster/src'),
            'thumb' => 'common.vendor.weotch.phpthumb.src',
        ],
        'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'frontend',
        'preload' => [
            'log',
        ],
        'name' => 'NikaLady',
        'language' => 'ru',
        'import' => [
            'application.components.*',
            'application.actions.*',
            'application.controllers.*',
            'application.models.*',
            'bootstrap.helpers.TbHtml',
            'bootstrap.widgets.*',
        ],
        'modules' => [
            'admin'
        ],
        'components' => [
          'sms' => [
                'class' => 'application.components.turbosms',
            ],
            'logator' => [
                'class' => 'application.components.LogatorBurator',
            ],
            'mail' => [
                'class' => 'application.components.YiiMail',
                'transportType' => 'smtp',
                'transportOptions' => [
                    'host' => 'smtp.gmail.com',//'smtp.localhost',
                    'username' => 'nikalady123@gmail.com',//'info@agropoisk.net',//'im-demetrodon', //noreply.agropoisk  
                    'password' => 'n1k@l@dy',//'info@1', //  '836215de', //1qsxzaw2
                    'port' => '587',
                    'encryption' =>'tls' //'ssl', // 'tls',
                ],
                'viewPath' => 'application.views.mail',
                'logging' => true,
                'dryRun' => false
            ],
            'user' => [
                'class' => 'WebUser',
                'allowAutoLogin' => true,
                'autoRenewCookie' => true,
                'loginUrl' => ['user/signIn'],
            ],
            'bootstrap' => [
                'class' => 'bootstrap.components.Bootstrap',
//				'enableCdn' => true,
                'enableBootboxJS' => false,
                'enableNotifierJS' => false,
            ],
            'errorHandler' => [
                'errorAction' => 'site/error'
            ],
            'format' => [
                'dateFormat' => 'd.m.y',
                'timeFormat' => 'h:i',
                'datetimeFormat' => 'd.m.Y h:i:s',
                'numberFormat' => ['decimals' => 2, 'decimalSeparator' => ',', 'thousandSeparator' => ' '],
                'booleanFormat' => ['Нет', 'Да'],
            ],
            'urlManager' => [
                'rules' => [
                    'about1/'=>'site/about1',
                    'categories/'=>'site/categories',
                    'about/'=>'site/about',
                    'product/'=>'site/product',
                    'checkout/'=>'site/checkout',
                    'index/'=>'site/index',
                    'page/<alias>' => 'site/page',
                    '<controller:\w+>/<id:\d+>' => '<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                ]
            ],
            'log' => [
                'class' => 'CLogRouter',
                'routes' => [
                    [
                        'class' => 'CFileLogRoute',
                        'levels' => 'error, warning',
                    ],
                ],
            ],
            'clientScript' => [
                'coreScriptPosition' => CClientScript::POS_END,
                'packages' => [
                    'fancybox' => [
                        'baseUrl' => 'js/fancybox',
                        'js' => [
                            'lib/jquery.mousewheel-3.0.6.pack.js',
                            'source/jquery.fancybox.pack.js?v=2.1.5',
                            'source/helpers/jquery.fancybox-buttons.js?v=1.0.5',
                            'source/helpers/jquery.fancybox-media.js?v=1.0.6',
                            'source/helpers/jquery.fancybox-thumbs.js?v=1.0.7',
                        ],
                        'css' => [
                            'source/helpers/jquery.fancybox-thumbs.css?v=1.0.7',
                            'source/helpers/jquery.fancybox-buttons.css?v=1.0.5',
                            'source/jquery.fancybox.css?v=2.1.5',
                        ],
                        'depends' => ['jquery'],
                    ],
                  
                ]
            ],
        ],
        'params' => [
          'robotEmail' =>'nikalady123@gmail.com', //'info@agropoisk.net',
          'robotEmailTo' =>'anton.karmeluck@gmail.com',  
          'login' =>'admin', 
          'pwd' =>'qszxaw', 
        ],
      
    ]
);
