<?php $this->breadcrumbs = [
	'Словарь: Работа с сообщениями',
]; ?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', [
	'type' => 'primary',
	'buttons' => [
	   ['label' => 'Добавить сообщение', 'url' => ['AddInmessg']],
	],
]);?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider->dataProvider(),
    'fixedHeader' => true,
    'filter' => $dataProvider,
    'type'=>'striped bordered condensed',
    'columns' => $columns,
    'responsiveTable' => true,
    'pager' => [
        'class' => 'bootstrap.widgets.TbPager',
        'displayFirstAndLast' => true
    ]
]); ?>
