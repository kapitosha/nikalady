<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "delivery".
 *
 * The followings are the available columns in table 'delivery':
 *
 * @property integer $Id
 * @property string $date
 * @property string $text
 */
class BaseDelivery extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseDelivery the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'delivery';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['date, text', 'required'],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'ID'),
			'date' => Yii::t('application', 'дата'),
			'text' => Yii::t('application', 'текст сообщения'),
		];
    }
}

