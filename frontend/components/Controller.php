<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * Base controller in frontend. Extended from CommonController
 * All controller classes in frontend should extend from this class.
 *
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
abstract class Controller extends CommonController
{
	public $layout = '//layouts/main';

    /**
     * Loading of scripts and styles that will be used in each controller
     * in the manage panel
     * - users css (main.css)
     * - users js (main.js)
     * - plugins (plugins.js)
     *
     * @return void
     */
    private function loadClientScript()
    {
//		$assetsScript = Yii::app()->assetManager->publish(Yii::app()->request->baseUrl . 'js' . DIRECTORY_SEPARATOR);

        /**
         * @var CClientScript $cs
         */
        $cs = Yii::app()->getClientScript()
            ->registerCoreScript('jquery.ui', CClientScript::POS_HEAD);

        // bootstrap css
//		$cs->registerCssFile((false) ? Yii::app()->request->baseUrl . DIRECTORY_SEPARATOR . 'css'. DIRECTORY_SEPARATOR . 'bootstrap.min.css' :'//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css');
        
        //validators
//		$cs->registerScriptFile('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js', CClientScript::POS_END);
//		$cs->registerScriptFile('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/additional-methods.min.js', CClientScript::POS_END);
//		$cs->registerScriptFile('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/localization/messages_RU.js', CClientScript::POS_END);

        // user styles and js
//        $cs->registerCssFile(Yii::app()->request->baseUrl . DIRECTORY_SEPARATOR . 'css'. DIRECTORY_SEPARATOR . 'main.css');
//        $cs->registerScriptFile($assetsScript . DIRECTORY_SEPARATOR . 'plugins.js', CClientScript::POS_END);
//        $cs->registerScriptFile($assetsScript . DIRECTORY_SEPARATOR . 'main.js', CClientScript::POS_END);

        
    }

    protected function beforeAction($action)
    {
		$this->loadClientScript();

//		Yii::app()->user->returnUrl = Yii::app()->request->urlReferrer;

        return parent::beforeAction($action);
    }
    
  
    
}
