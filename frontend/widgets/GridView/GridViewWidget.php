<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class GridViewWidget extends CWidget
{
	public $modelClass;
	public $headers;
	public $itemView;
	public $criteria;
	public $pageSize = 10;

	public function init()
	{}

	public function run()
	{
		$modelClass = $this->modelClass;
		if (empty($this->criteria)) {
			$this->criteria = new CDbCriteria;
			$this->criteria->order = 'id DESC';
		}
		$count = $modelClass::model()->count($this->criteria);
		$pages = new CPagination($count);

		// results per page
		$pages->pageSize = $this->pageSize;
		$pages->applyLimit($this->criteria);
		$models = $modelClass::model()->findAll($this->criteria);
		$this->render('list', [
			'models' => $models,
			'pages' => $pages,
			'headers' => $this->headers,
			'itemView' => $this->itemView
		]);
	}
} 