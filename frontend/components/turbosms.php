<?php
header ('Content-type: text/html; charset=utf-8'); 

class TurboSMS extends CComponent
{
    //переменна для подключени к сервису
    private static  $client; 
    
    //логин и пароль для подключения к сервису  
    protected  $auth = Array ( 
                      'login' => 'Agropoisk', //ваш_логин 
                      'password' => '600965' //ваш_пароль
                      );     
    
    //отправитель что будет отображаться
    //Подпись отправителя может содержать английские буквы и цифры. Максимальная длина - 11 символов.
    protected $sender='AGROPOISK'; 
    // инициализация модуля подключение к сервису turbosms
    public function init()
	{
    self::$client = new SoapClient ('http://turbosms.in.ua/api/wsdl.html'); 
    }
    
   /**
    * просмотреть список доступных функций сервера 
    * @return array
    */ 
    public function getFunctions() 
    {
      return self::$client->__getFunctions ();
    }
   
   /**
    * Авторизируемся на сервере
    * @return mixed результаты авторизации на сервере
    */
    public function login()
    {
      if (self::$client-> Auth ($this->auth)->AuthResult=='Вы успешно авторизировались') {
      return true; } else { return false;};
      
    } 
    /**
     * получить ваш остаток кредиток
     * @return mixed
     */
    public function getCredits()
    {
      if ($this->login()) {
      return self::$client->GetCreditBalance()->GetCreditBalanceResult;
      } else { return false; };
    }    
    
    /**
     * функция отправки сообщения
     * $destination - телефон получателя '+380XXXXXXXXX'
     * чтобы отправляем сообщение на несколько номеров. 
     * Номера разделены запятыми без пробелов. например: '+380XXXXXXXX1,+380XXXXXXXX2,+380XXXXXXXX3', 
     * Номер указывается в полном формате, включая плюс и код страны 
     * $textSMS Текст сообщения ОБЯЗАТЕЛЬНО отправлять в кодировке UTF-8 
     * $wappush Отправляем сообщение с WAPPush ссылкой Ссылка должна включать http:// 
     * на пример $wappush = 'http://super-site.com' 
     * $toUTF если текст сообщения в 1251 для перекодировки поставить TRUE
     * 
     */
    public function sendSMS($destination, $textSMS, $toUTF=false, $wappush=null) 
    {
//      try {
//            $pdo = new PDO ("mysql:host=77.120.116.10;dbname=users","{$this->auth['login']}","{$this->auth['password']}");
//                $pdo->query("SET NAMES utf8;");
//                  $pdo->query("INSERT INTO `{$this->auth->login}` (`number`,`message`,`sign`) VALUES ('$destination','$textSMS','{$this->sender}')");
//            return true;      
//      } 
//      catch(Exception $e)
      {
          if ($this->login())
          {
              if ($toUTF ) $textSMS = iconv ('windows-1251', 'utf-8', $textSMS); 

              $sms=['sender' => $this->sender, 
                    'destination' => $destination, 
                      'text' => $textSMS];
              if (!empty($wappush)) $sms['wappush']=$wappush;

                $result=self::$client->SendSMS($sms);
                if (isset($result->ResultArray[0]) && $result->ResultArray[0]=='Сообщения успешно отправлены') {
                                    return $result->ResultArray[1];} else return false;
          } else return false;
      }
    }    
//stdClass Object ( [SendSMSResult] => stdClass Object ( [ResultArray] => Array ( [0] => Сообщения успешно отправлены [1] => c9e9c1e7-54c8-37a5-63cf-4f5a77c46227 ) ) )
    // Запрашиваем статус конкретного сообщения по ID 
    public function getMesStus($MessID)
    {    
      if ($this->login()) {
       // $sms = Array ('MessageId' => 'c9482a41-27d1-44f8-bd5c-d34104ca5ba9'); 
          return self::$client->GetMessageStatus (['MessageId' => $MessID])->GetMessageStatusResult; 
      } else return false;
    }
    
    
    // Запрашиваем массив ID сообщений, у которых неизвестен статус отправки
    public function getNewMessages()
    {    
      if ($this->login()) {
        return self::$client->GetNewMessages ()->GetNewMessagesResult; 
      } else return false; 
    }
    
    
}
