<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
abstract class AdminController extends Controller
{
	public $layout = 'main';

	/**
	 * @see CController::filters()
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return [
//			['allow', // allow only admin and operators
//				'roles' => ['admin'],
//			],
//			['deny', // deny all users
//				'users' => ['*'],
//			],
		];
	}

	protected function registerCPAssets()
	{
		$assetsCSS = Yii::app()->assetManager->publish(Yii::app()->request->baseUrl . 'css' . DIRECTORY_SEPARATOR);

		/**
		 * @var CClientScript $cs
		 */
		$cs = Yii::app()->getClientScript();
		$cs->registerCssFile(Yii::app()->baseUrl. DIRECTORY_SEPARATOR. 'css'  . DIRECTORY_SEPARATOR . 'bsstyles.css');
    $cs->registerScriptFile(Yii::app()->baseUrl. DIRECTORY_SEPARATOR. 'js'  . DIRECTORY_SEPARATOR . 'bsinterface.js', CClientScript::POS_END);
	}

	protected function beforeAction($action)
	{
		$this->registerCPAssets();

		return parent::beforeAction($action);
	}
}