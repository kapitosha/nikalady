<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "inmessg".
 *
 * The followings are the available columns in table 'inmessg':
 *
 * @property integer $Id
 * @property string $date
 * @property string $subject
 * @property string $text
 * @property string $email
 * @property string $idstatus
 * @property string $note
 */
class sInmessg extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return sInmessg the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'inmessg';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['text', 'required'],
			['subject, idstatus', 'length', 'max' => 100],
			['email', 'length', 'max' => 200],
			['date, note', 'safe'],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'ідентифікатор'),
			'date' => Yii::t('application', 'дата'),
			'subject' => Yii::t('application', 'тема'),
			'text' => Yii::t('application', 'текст повідомлення'),
			'email' => Yii::t('application', 'електронна адреса відправника'),
			'idstatus' => Yii::t('application', 'статус повідомлення'),
			'note' => Yii::t('application', 'примітка'),
		];
    }
}

