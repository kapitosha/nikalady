<?php $this->breadcrumbs = [
	'Словарь: Категории товаров',
]; ?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', [
	'type' => 'primary',
	'buttons' => [
	   ['label' => 'Добавить категорию', 'url' => ['AddCategories']],
	],
]);?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider->dataProvider(),
    'fixedHeader' => true,
    'filter' => $dataProvider,
    'type'=>'striped bordered condensed',
    'columns' => $columns,
    'responsiveTable' => true,
    'pager' => [
        'class' => 'bootstrap.widgets.TbPager',
        'displayFirstAndLast' => true
    ]
]); ?>
