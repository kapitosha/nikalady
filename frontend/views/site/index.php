      <!--Start of Home Content-->
      <div class="main">
        <div class="col-main">
          <div class="std"> 
            
            <!--Start Banner-->
            <div class="banner_box">
              <div class="slider-wrapper banner"> 
                <!--Place your banner images-->
                <div id="slider" class="banner_slider"> 
                  <a href="#"><img src="images/banner_1.jpg" alt="" /></a> 
                  <a href="#"><img src="images/banner_2.jpg" alt="" /></a> 
                  <a href="#"><img src="images/banner_3.jpg" alt="" /></a> 
                  <a href="#"><img src="images/banner_4.jpg" alt="" /></a> 
                </div>
              </div>
              <div class="promotional_block"> <!--Place your promotional images-->
                <div class="block_one"> <a href="#"><img src="images/promo1.jpg" alt="" /></a> </div>
                <div class="block_one"> <a href="#"><img src="images/promo2.jpg" alt="" /></a> </div>
              </div>
            </div>
            <!--End Banner--> 
            
            <!--Start New Products-->
            <div class="box-center">
              <div class="special">
                <div style="visibility: visible;" id="mix_container" class="mix_container">
                  <h1 class="category_page_title"><center>Новинки</center></h1>
                  <div class="mix_nav"> <span id="mix_prev" class="mix_prev">Назад</span> <span id="mix_next" class="mix_next">Далее</span> </div>
                  <div id="container" class="mix_wrapper">
                    <!-- тут має бути вставка завантаження динамічна  -->
                    <!-- row1 -->
                    <ul style="position: relative;" class="mix_gallery">
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro1.jpg" alt="Imperdiet id tincidunt " /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар</a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a rel="example_group" href="./images/pro1.jpg" class="fancybox quickllook" id="fancybox170">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro2.jpg" alt="Imperdiet id tincidunt " /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro3.jpg" alt="Imperdiet id tincidunt " /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro4.jpg" alt="Imperdiet id tincidunt " /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row last">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro5.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro6.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro7.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro8.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар</a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro9.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row last">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro10.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro11.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro12.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro13.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар</a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro14.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар</a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                      <li class="item mix_row last">
                        <div class="outer box"> <a href="#" class="product-image"><img src="images/pro15.jpg" alt="Imperdiet id tincidunt" /></a>
                          <div class="ic_caption">
                            <h2 class="product-name"><a href="#" title="Imperdiet id tincidunt ">Товар </a></h2>
                            <div class="actions">
                              <button style="display:none;" type="button" title="Add to Cart" class="button btn-cart"> <span> <span>В корзину</span> </span> </button>
                              <a href="#" class="fancybox quickllook">Посмотреть</a>
                              <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!--End New Products--> 
            
          </div>
        </div>
      </div>
      <!--End of Home Content--> 
      <div style="display: none;" id="back-top"> <a href="#"><img alt="" src="images/backtop.png" /></a> </div>
