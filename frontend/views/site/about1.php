<div id="simpleFadeSlideShow_container">
  <div id="simpleFadeSlideShow_images"  class="curved-hz-1" style="width: 980px; overflow:none; position: relative; padding: 0px; height: 149px;">
    <div>
      <a href="#" title="Слайд 1">
        <img src='/images/slider/all/Slide-1.png' alt='Слайд 1' width='980' height='150' />
        <div class="slide-content">
          <h4>Ваш бизнес в Ваших руках!</h4>  
          <p>Подготовьтесь к сезону. Найдите исполнителя работ заранее, будь то уборка урожая, обработка почвы или перевозки.</p>
        </div>
      </a>
    </div>
    <div>
      <a href="#" title="Слайд 2">
        <img src='/images/slider/all/Slide-2.png' alt='Слайд 2' width='980' height='150' />
        <div class="slide-content">
          <h4>Никаких простоев!</h4>  
          <p>Мы поможем Вам управлять и составлять график оказания услуг заблаговременно.</p>
        </div>
      </a>
    </div>
    <div>
      <a href="#" title="Слайд 3">
        <img src='/images/slider/all/Slide-3.png' alt='Слайд 3' width='980' height='150' />
        <div class="slide-content">
          <h4>Множество техники. Удобный поиск!</h4>  
          <p>Пользователи портала размещают свою технику на собственной странице,  предлагая свои услуги.</p>
        </div>
      </a>
    </div>
    <div>
      <a href="#" title="Слайд 4">
        <img src='/images/slider/all/Slide-4.png' alt='Слайд 4' width='980' height='150' />
        <div class="slide-content">
          <h4>Все виды сельхоз услуг от участников!</h4>  
          <p>На сайте легко найти работу для имеющейся у Вас сельхоз техники или транспорта, достаточно добавить в парк и подать заявку.</p>
        </div>
      </a>
    </div>
    <div>
      <a href="#" title="Слайд 5">
        <img src='/images/slider/all/Slide-5.png' alt='Слайд 4' width='980' height='150' />
        <div class="slide-content">
          <h4>Ваши потенциальные партнеры уже с нами!</h4>  
          <p>Все участники проекта отображаются в Каталоге фирм. Доступен поиск по роду деятельности, моделям имеющейся техники и региональной принадлежности.</p>
        </div>
      </a>
    </div>
    <div>
      <a href="#" title="Слайд 6">
        <img src='/images/slider/all/Slide-6.png' alt='Слайд 4' width='980' height='150' />
        <div class="slide-content">
          <h4>Заявите о своем желании сотрудничать!</h4>  
          <p>Просматривайте и отвечайте на поданные пользователями заявки. Также подавайте заявки самостоятельно, это значительно эффективнее.</p>
        </div>
      </a>
    </div>
  </div>
</div>

<div class="mainDescription w100   curved-hz-1">
   <div class="contact-info">
     <h2 class="green-title">Контактная информация:</h2>
     <address>
       <p class="info1">Адрес офиса:<strong>Украина, г. Хмельницкий, ул. Лесонивецкая, 24, 29000</strong></p>
       <p class="info2">Время работы офиса:<strong>Пн.-Сб.: 9:00 — 18:00</strong></p>
       <p class="info3">Телефон:<strong> +38 (098) 811-21-21</strong></p>
       <p class="info4">Факс:<strong> +38 (098) 811-21-21</strong></p>
       <p class="info5">E-mail:<strong> <a href="mailto:support@agropoisk.net">support@agropoisk.net</a></strong></p>
     </address>
     <p>
       Доступ ко всем сервисам портала предоставляется круглосуточно. По всем возникшим вопросам обращайтесь в техническую поддержку по телефонам, указанным на сайте или по эл. почте.
     </p>
     <p>
Портал «Агропоиск» совершенно новый проект, и, поэтому мы ни дня не стоим на месте. К нам поступает множество идей, пожеланий и предложений от пользователей сайта. Многие из них воплощаются. Только с Вашей помощью мы сможем сделать сайт лучше, эргономичнее и максимально полезным для участников проекта «Агропоиск».</p>
     <p>Все пожелания принимаются на e-mail: <strong><a class="greean-link" href="mailto:admin@agropoisk.net">admin@agropoisk.net</a></strong> или же по телефону.</p>
     <p>Всегда рады Вам помочь,<br>
команда Agropoisk</p>
    <h2 class="green-title">С НАМИ РАБОТАЮТ:</h2>
   </div>
   <div class="work-with-as" >
      <?php foreach ($model as $key => $data) : ?>
      <div>
        <div style=" border:1px solid #e6e6e6; height: 84px; width:84px; 
             background-image: url(../files/logos/<?=$data->logo ?>); background-size: contain; background-repeat: no-repeat; background-position: center;" 
             title="<?=$data->name ?>" onclick="window.open('<?=$data->url;?>','_blank');">
        </div>  
        <div class="text">Название
предприятия</div>
      </div>
      <?php endforeach; ?>
   </div>    
</div>


<div class="g-hidden">
  <div id="view_popup" class="box-modal">
    <?php // $this->renderPartial?>
  </div>
</div> 