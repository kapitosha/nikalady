<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "availability".
 *
 * The followings are the available columns in table 'availability':
 *
 * @property integer $Id
 * @property string $name
 * @property string $note
 */
class BaseAvailability extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseAvailability the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'availability';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['name', 'length', 'max' => 100],
			['note', 'length', 'max' => 255],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'ідентифікатор'),
			'name' => Yii::t('application', 'Назва'),
			'note' => Yii::t('application', 'примітка'),
		];
    }
}

