<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class ListWidget extends CWidget
{
	/**
	 * @var string item view name
	 */
	public $itemView;

	/**
	 * @var array data with findAll or children methods
	 */
	public $data;

	public function init()
	{}

	public function run()
	{
		if (count($this->data) > 0) {
			$this->renderItems();
		}
	}

	/**
	 * Render items
	 * @throws CException
	 */
	protected function renderItems()
	{
		foreach ($this->data as $item) {
			$this->render($this->itemView, ['data' => $item]);
		}
	}
} 