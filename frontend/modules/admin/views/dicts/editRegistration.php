<?php $this->breadcrumbs = [
	'Словарь реестрации' => ['listRegistration'],
($model->isNewRecord) ? 'Добавление' : 'Изменение' +' типа реестрации'
]; ?>

<?php
/**
 * @var CController $this
 * @var TbActiveForm $form
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
	'id' => get_class($model),
	'type' => 'horizontal',
	'htmlOptions' => ['class' => 'well'],
	'enableAjaxValidation' => true,
]); ?>

<div class="control-group">
  <?= $form->LabelEx($model, 'email', ['class' => 'control-label']); ?>
  <div class = "controls span4 lbl_span"> <?= $model->email; ?>  </div>
</div>
<div class="control-group">
    <?= $form->labelEx($model, 'note', ['class'=>'control-label']); ?>
  <div class="controls">
    <!--<?= $form->telField($model, 'note', ['class' => 'right']); ?> -->
    <?= $form->textarea($model, 'note', ['cols' => '47', 'rows' => '8', 'class'=>'span4']) ;?>
    </div>
    <?= $form->error($model, 'note'); ?>
    <div class="clear"></div>  
  </div>  
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => ($model->isNewRecord) ? 'Добавить' : 'Сохранить'
		]); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'reset',
			'label' => 'Сбросить'
		]); ?>
	</div>

<?php $this->endWidget(); ?>