<?php $this->breadcrumbs = [
	'Пользователи',
]; ?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', [
	'type' => 'primary',
    'buttons' => [
	['label' => 'Не проверенные', 'url' => ['notActivated'] ],
	//['label' => 'посмотреть историю', 'url' => ['history'] ],
    ['label' => 'сообщения всем', 'url' => ['sendMessAll'] ],  
    ]  
    ]);
?>


<?php 
//$this->widget('bootstrap.widgets.TbButton', [
//	'type' => 'primary',
//	'label' => 'Не проверенные',
//	'url' => ['notActivated']
//]);
?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
	'dataProvider' => $dataProvider->dataProvider(),
    'filter' => $dataProvider,
	'fixedHeader' => true,
	'type'=>'striped bordered condensed',
	'columns' => $columns,
	'responsiveTable' => true,
	'pager' => [
		'class' => 'bootstrap.widgets.TbPager',
		'displayFirstAndLast' => true
	]
]); ?>
