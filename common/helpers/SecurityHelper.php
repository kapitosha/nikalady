<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * Security helper
 *
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class SecurityHelper
{
	/**
	 * Generates a salt that can be used to generate a password hash.
	 *
	 * The {@link http://php.net/manual/en/function.crypt.php PHP `crypt()` built-in function}
	 * requires, for the Blowfish hash algorithm, a salt string in a specific format:
	 *  - "$2a$"
	 *  - a two digit cost parameter
	 *  - "$"
	 *  - 22 characters from the alphabet "./0-9A-Za-z".
	 *
	 * @param $cost int cost parameter for Blowfish hash algorithm
	 * @return string the salt
	 * @throws CException if improper cost passed
	 */
	public static function generateSalt($cost = 10)
	{
		if (!is_numeric($cost) || $cost < 4 || $cost > 31) {
			throw new CException('Cost parameter must be between 4 and 31.');
		}
		// Get some pseudo-random data from mt_rand().
		$rand = '';
		for ($i = 0; $i < 8; ++$i) {
			$rand .= pack('S', mt_rand(0, 0xffff));
		}
		// Add the microtime for a little more entropy.
		$rand .= microtime();
		// Mix the bits cryptographically.
		$rand = sha1($rand, true);
		// Form the prefix that specifies hash algorithm type and cost parameter.
		$salt = '$2a$' . sprintf('%02d', $cost) . '$';
		// Append the random salt string in the required base64 format.
		$salt .= strtr(substr(base64_encode($rand), 0, 22), ['+' => '.']);
		return $salt;
	}

	/**
	 * Generate random combination
	 *
	 * @param int $length int length combination
	 * @return string random combination
	 */
	public static function generateRandomCombination($length = 6)
	{
		$listChars = ['a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'r', 's',
			't', 'u', 'v', 'x', 'y', 'z',
			'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'R', 'S',
			'T', 'U', 'V', 'X', 'Y', 'Z',
			'1', '2', '3', '4', '5', '6',
			'7', '8', '9', '0'];
		$combination = '';
		for ($i = 0; $i < $length; $i++) {
			$index = rand(0, count($listChars) - 1);
			$combination .= $listChars[$index];
		}
		return $combination;
	}

	/**
	 * Addition generate combination
	 *
	 * @param int $length length first part combination
	 * @param array $additionData array addition data to be added from result
	 * @return string some combination
	 * Algorithm sha1(combination + md5(combination))
	 */
	public static function additiongenerateRandomCombination(array $additionData = [], $length = 15)
	{
		$combination = self::generateRandomCombination($length);
		if (count($additionData) > 0) {
			foreach ($additionData as $data) {
				$combination .= $data;
			}
		}
		return sha1(($combination . md5($combination)));
	}
} 