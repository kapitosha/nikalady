<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class UserController extends Controller
{
	/**
	 * @see CController::actions()
	 */
	public function actions()
	{
		return [
			'captcha' => [
				'class' => 'CaptchaAction',
			],
		];
	}


	/**
	 * Авторизація
	 *
	 * @param bool $popup bool де здійснюється авторизація в popup чи в іншій сторінці
	 * По дефалту в іншій сторінці.
	 * Якщо в popup то ми обробляємо тільки post запит без render view
	 * @throws CHttpException
	 */
	public function actionSignIn($popup = false)
    {
		if (!defined('CRYPT_BLOWFISH') || !CRYPT_BLOWFISH) {
			throw new CHttpException(500, 'This application requires that PHP was compiled with Blowfish support for crypt().');
		}

        $model = new LoginForm;

        $this->performAjaxValidation($model, get_class($model));

        // collect user input data
        if (isset($_POST['LoginForm'])) {

            $model->attributes = $_POST['LoginForm'];
            if ($model->login()) {
                Yii::app()->user->setFlash('success', 'Вы авторизировались в системе');
				if ($popup == true) {
					$this->redirect('/');
				} else {
                      //$this->redirect(Yii::app()->user->returnUrl);
                  $this->redirect('/personal/info/index/');
				}
            }
        }

		if ($popup == false) {
			$this->render('signIn', ['model' => $model]);
		}
    }

	/**
	 * Закінчення роботи сесії юзера
	 */
	public function actionSignOut()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

	// AJAX actions

 
    /** функция опеределения уровня доступа пользователя из аякса
     *  возвращает:
     *  0 - пользователь гость
     *  1 - пользователь зарегистрироваан но не активирован
     *  2 - пользователь актививрован но не оплатил абонплату за использование ресурса
     *  3 - пользователь активирован и оплати использование ресурса
     *  9 - пользователь с админправами
     *  
     * 
     */
    public function actionUserAcess(){
      $grant=0;
      //гость
      if (Yii::app()->user->isGuest) $grant=0;
      //админимстрация сайта
      else if (Yii::app()->user->IsNotUser) $grant=9;
      //пользователь не актививрован
      else if (!Yii::app()->user->isGuest && !Yii::app()->user->IsAccepted) $grant=1;
      //пользователь  актививрован неоплатил пользование или полный доступ
      else if (!Yii::app()->user->isGuest && Yii::app()->user->IsAccepted) 
      {
        if (Payments::model()->accesFull(Yii::app()->user->id)) {$grant=3;} else {$grant=2;};
      }
      echo $grant;    
    }

    
    /**
     * Сповіщення юзера, що його підтвердили
     * @param User $model User
     */
    protected function sendMessageToUser(User $model)
    {
        $message = new YiiMailMessage;
        $message->view = 'user/register';
        $message->setSubject('Регистрация');
        $message->setBody(['model' => $model], 'text/html');
        $message->addTo($model->email, $model->contactPerson);
        $message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);
        Yii::app()->mail->send($message);
    }    
    
} 