<?php
/**
 * @link http://web-systems.com.ua/
 * @link http://price-r.ru/
 * @copyright Copyright (c) 2013 price-R.ru
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * Контролер дій які стосуються відновлення даних користувача
 *
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class RecoveryController extends Controller
{
	/**
	 * Ініціалізуюча дія для відновлення паролю по переданому email
	 * Користувач вводить email, після чого звіряється чи існує такий користувач
	 * і якщо так відсилається на email посилання з token для выдновлення
	 * Ну а інакше виключення що такого користувача немає
	 * @throws CHttpException виклюення якщо користувача по переданому email не існує
	 */
	public function actionPassword()
	{
		$model = new RecoveryForm;
		$this->performAjaxValidation($model, get_class($model));
		if (isset($_POST['RecoveryForm'])) {
			$model->attributes = $_POST['RecoveryForm'];
			if ($model->validate()) {
				/**
				 * @var User $modelUser
				 */
				$modelUser = User::model()->findByEmail($model->email);
				if ($modelUser->saveAttributes(['passwordResetToken' => $modelUser->generatePasswordResetToken()])) {
					// відсилання повідомлення на переданий email
					$this->sendConfirmRecovery($modelUser);
                                        Yii::app()->logator->log('Востановление пароля пользователя','Для пользователя '.$modelUser->username.' выслано сообщение на email о изменении пароля' );
					Yii::app()->user->setFlash('success', 'На указанный email было выслано подтверждение о изменении, пожалуйста, проверьте свой email.');
				}
			}
            $this->redirect('/');
		}
	}

	/**
	 * Встановлення нового паролю коритсувача по переданому token
	 * @param $token string token для пошуку користувача в системі
	 * @throws CHttpException виключення якщо token не вірний
	 */
	public function actionReset($token)
	{
		/**
		 * @var User $model
		 */
		$model = User::model()->findByResetToken($token);
		if (isset($model)) {
			$model->scenario = 'recovery';
			$this->performAjaxValidation($model, get_class($model));
			if (isset($_POST['User'])) {
				$model->attributes = $_POST['User'];
				if ($model->save()) {
					// відправка нового пароля користувачеві
					$this->sendNewPassword($model);
                                        Yii::app()->logator->log('Сброс пароля пользователя','Для пользователя '.$model->username.' выполнено изменении/сброс пароля' );    
					Yii::app()->user->setFlash('success', "Уважаемый(ая) {$model->contactPerson}, ваш пароль был изменен.");
					$this->redirect('/');
				}
			}
			$this->render('resetPassword', ['model' => $model]);
		} else {
			Yii::app()->user->setFlash('error', 'Ключ подтверждения не верный, обратитесь в тех. поддержку');
			$this->redirect('/');
		}
	}

	/**
	 * Повідомлення з новим паролем юзеру
	 * @param User $model
	 */
	protected function sendNewPassword(User $model)
	{
		$message = new YiiMailMessage;
		$message->view = 'recovery/newPassword';
		$message->setSubject('Ваш новый пароль');
		$message->setBody(['model' => $model], 'text/html');
		$message->addTo($model->email, $model->contactPerson);
		$message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);
		Yii::app()->mail->send($message);
	}

	/**
	 * Повідомлення підтвердження відновлення паролю юзеру
	 * @param User $model
	 */
	protected function sendConfirmRecovery(User $model)
	{
		$message = new YiiMailMessage;
		$message->view = 'recovery/confirm';
		$message->setSubject('Подтверждение восстановления пароля');
		$message->setBody(['model' => $model], 'text/html');
		$message->addTo($model->email, $model->contactPerson);
		$message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);
		Yii::app()->mail->send($message);
	}
} 