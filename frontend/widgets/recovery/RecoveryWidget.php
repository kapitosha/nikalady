<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class RecoveryWidget extends CWidget
{
	public function run()
	{
		$model = new RecoveryForm;

		$this->render('recovery', ['model' => $model]);
	}
} 