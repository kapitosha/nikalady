<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ProductsController extends AdminController {
  
  public function actionIndex()
	{
    
   $this->render('index'); 
    }
    
    //відображення гріда товарів
  public function actionListProducts()
	{
    //готую дані з моделі
    $dataProvider = new Products('dataProvider');
    //формую перелік колонок для відображення
		$columns = [
			'Id',
                        'productcode',
			'tytle',
			'description',
                        'price',
                        'wholesaleprice',
                        'pricefrom',
                        'image',
                        'image2',
                        'image3',
                        'image4',
                        'image5',
                    
                          ['name'=>'catgrR.name', 'filter'=>true, 
       'header' => 'Категория',
       //'class'=>'bootstrap.widgets.TbRelationalColumn',
      ],
                          ['name'=>'subcatgrR.name', 'filter'=>true, 
       'header' => 'Подкатегория',
       //'class'=>'bootstrap.widgets.TbRelationalColumn',
      ],
			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}{delete}',
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'editProducts\', [\'Id\' => $data->Id])',
				'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeProducts\', [\'Id\' => $data->Id])',
			]
		];    
    Products::model()->SetFilter($dataProvider);
    
   //викликаю в'юшку і передаю підготовлені дані і опис колонок 
   $this->render('listProducts',['dataProvider' => $dataProvider, 'columns' => $columns]); 
    }
    
  //видаляю запис з таблиці  
  public function actionRemoveProducts($Id)
	{
    //видаляю запис з таблиці
    Products::model()->deleteByPk($Id);
    //перемальовую форму
    Yii::app()->end();
    
    //$this->redirect('/');
  }  

  //викликаю форму зміни запису в таблиці  
  public function actionEditProducts($Id)
    {
        if (!$model = Products::model()->findByPk($Id)) {
                throw new CHttpException(404, 'Такой категоии товаров не найдено в базе');
        }
        
        if ($model->setFromPost()   )    { //&& $model->save()) {
            $model->attributes=$_POST[get_class($model)];
          if (empty($model->wholesaleprice)) {
             //print_r($model->attributes);
             $model->wholesaleprice=null;
             }            
            
          $image =CUploadedFile::getInstance($model,'img1' );
          if ($image) {
            $sourcePath = pathinfo($image->getName());
            $fileName = $model->Id.'-img1.'.$sourcePath['extension'];
            $model->image = $fileName; 
          } else $model->img1=null;  
          $image2=CUploadedFile::getInstance($model,'img2');
          if ($image2) {
            $sourcePath = pathinfo($image2->getName());
            $fileName = $model->Id.'-img2.'.$sourcePath['extension'];
            $model->image2 = $fileName;
          };  
          $image3=CUploadedFile::getInstance($model,'img3');
          if ($image3) {
            $sourcePath = pathinfo($image3->getName());
            $fileName = $model->Id.'-img3.'.$sourcePath['extension'];
            $model->image3 = $fileName; 
          };  
          $image4=CUploadedFile::getInstance($model,'img4');
          if ($image4) {
            $sourcePath = pathinfo($image4->getName());
            $fileName = $model->Id.'-img4.'.$sourcePath['extension'];
            $model->image4 = $fileName; 
          };  
          $image5=CUploadedFile::getInstance($model,'img5');
          if ($image5) {
            $sourcePath = pathinfo($image5->getName());
            $fileName = $model->Id.'-img5.'.$sourcePath['extension'];
            $model->image5 = $fileName; 
          };  

            //echo $fileName.'=='.$model->image; exit;
           if($model->save(false)){
             //echo '== '.$fileName.' == '.$model->image; //exit;
              if ($model->image && $image){
                $file = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/files/'.$model->image;
                $image->saveAs($file);
              };   
              if ($model->image2 && $image2){
                $file = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/files/'.$model->image2;
                $image2->saveAs($file);
              };   
              if ($model->image3 && $image3){
                $file = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/files/'.$model->image3;
                $image3->saveAs($file);
              };   
              if ($model->image4 && $image4){
                $file = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/files/'.$model->image4;
                $image4->saveAs($file);
              };   
              if ($model->image5 && $image5){
                $file = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/files/'.$model->image5;
                $image5->saveAs($file);
              };   
           }; 
           //exit;
                $this->redirect(['ListProducts']);
        }
        $this->render('editProducts', ['model' => $model]);
    
    }
  
  public function actionAddProducts()
    {
  	$model = new Products;
                
    //print_r($model->attributes);
    
//    if (isset($_POST['Products'])) {
//      print_r($_POST['Products']);
//      exit;
//    }
    
    $this->performAjaxValidation($model, 'Products');
    
        if ($model->setFromPost() ) { //&& $model->save()
          $model->attributes=$_POST[get_class($model)];
          
          if (empty($model->wholesaleprice)) {
             //print_r($model->attributes);
             $model->wholesaleprice=null;
             }
         // print_r($model->attributes);
         // Yii::app()->end();
          
          
          if ($model->Id==NULL) {$model->Id=Products::model()->getId();};
          
          $image =CUploadedFile::getInstance($model,'img1' );
          if ($image) {
            $sourcePath = pathinfo($image->getName());
            $fileName = $model->Id.'-img1.'.$sourcePath['extension'];
            $model->image = $fileName; 
          };  
          $image2=CUploadedFile::getInstance($model,'img2');
          if ($image2) {
            $sourcePath = pathinfo($image2->getName());
            $fileName = $model->Id.'-img2.'.$sourcePath['extension'];
            $model->image2 = $fileName;
          };  
          $image3=CUploadedFile::getInstance($model,'img3');
          if ($image3) {
            $sourcePath = pathinfo($image3->getName());
            $fileName = $model->Id.'-img3.'.$sourcePath['extension'];
            $model->image3 = $fileName; 
          };  
          $image4=CUploadedFile::getInstance($model,'img4');
          if ($image4) {
            $sourcePath = pathinfo($image4->getName());
            $fileName = $model->Id.'-img4.'.$sourcePath['extension'];
            $model->image4 = $fileName; 
          };  
          $image5=CUploadedFile::getInstance($model,'img5');
          if ($image5) {
            $sourcePath = pathinfo($image5->getName());
            $fileName = $model->Id.'-img5.'.$sourcePath['extension'];
            $model->image5 = $fileName; 
          };  
          
          
          

            //echo $fileName.'=='.$model->image; exit;
                      
                      
           if($model->save()){
              if ($model->image && $image){
                $file = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/files/'.$model->image;
                $image->saveAs($file);
              };   
              if ($model->image2 && $image2){
                $file = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/files/'.$model->image2;
                $image2->saveAs($file);
              };   
              if ($model->image3 && $image3){
                $file = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/files/'.$model->image3;
                $image3->saveAs($file);
              };   
              if ($model->image4 && $image4){
                $file = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/files/'.$model->image4;
                $image->saveAs($file);
              };   
              if ($model->image5 && $image5){
                $file = $_SERVER['DOCUMENT_ROOT'].Yii::app()->urlManager->baseUrl.'/files/'.$model->image5;
                $image5->saveAs($file);
              };   
           }; 
           //exit;

                $this->redirect(['ListProducts']);
        }
        $this->render('editProducts', ['model' => $model]);
    
    }
   
    
    /*
     * получаю підкатегорію ддя вибору з категорії
     */
    public function actionGetSubCatg($id)
    {
      if ($id!=null) {
        $data =Subcategories::model()->listSubCatgsbyCatg($id);
        $htmlOptions = ['encode' => true,];
        echo CHtml::listOptions('', CHtml::listData($data, 'Id', 'name'), $htmlOptions);
      };

    }    
    

       public function actionCreate(){
        $model= new Products;
        if(isset($_POST['Products'])){
            $model->attributes=$_POST['Products'];
            $model->id_image=CUploadedFile::getInstance($model,'image');
            if($model->save()){
                $model->id_image->saveAs('www/files');
                // перенаправляем на страницу, где выводим сообщение об
                // успешной загрузке
            }
        }
        $this->render('create', array('model'=>$model));
    }

}