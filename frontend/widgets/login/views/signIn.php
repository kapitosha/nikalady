<?php
/**
 * @var CActiveForm $form
 */
?>
<?php if (Yii::app()->user->isGuest) : ?>

    <div id="enter-popup" class="box-modal enter-popup">
        <span class="box-modal_close arcticmodal-close">x</span>
        <div class="popup-logo">
            <img src="/images/popup-logo.jpeg" alt=""/>
        </div>
        <?php $form = $this->beginWidget('CActiveForm', [
            'id' => get_class($model),
            'action' => '/user/signIn/popup/1',
            'enableAjaxValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'validateOnChange' => false, //true,
            ],
        ]); ?>
        <div class="form mini">
			<?= CHtml::hiddenField('from', 'popup'); ?>
            <div class="row">
                <?= $form->labelEx($model, 'email'); ?>
                <?= $form->emailField($model, 'email'); ?>
                <?= $form->error($model, 'email'); ?>
            </div>
            <div class="row">
                <?= $form->labelEx($model, 'password'); ?>
                <?= $form->passwordField($model, 'password'); ?>
                <?= $form->error($model, 'password'); ?>
                <span id="btn-remind-password" class="btn-remind-password">Забыли пароль?</span>
                                <div class="remember-user">
                                  <?php echo $form->checkBox($model,'rememberUser'); ?>
                                  <?php echo $form->label($model,'rememberUser'); ?>
                                  <?php echo $form->error($model,'rememberUser'); ?>
                                  
<!--                                  <input type="checkbox" checked id="rememberUser"/>
                                    <label for="rememberUser">Запомнить</label>-->
                                </div>
            </div>
            <div class="row">
                <span class="btn-grey"><?= CHtml::link('Регистрация', ['/user/signUp']); ?></span>
                <input class="btn-green" type="submit" value="Войти"/>
                <a class="back-to-main" href="/" title=""><i></i>На главную</a>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>

<?php endif; ?>