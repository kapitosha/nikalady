<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "feedback".
 *
 * The followings are the available columns in table 'feedback':
 *
 * @property integer $Id
 * @property string $date
 * @property string $name
 * @property string $phonenumber
 * @property integer $idstatus
 * @property string $note
 */
class BaseFeedback extends CActiveRecord
{
    use RequestOfModel;

    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return Feedback the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'feedback';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['name', 'length', 'max' => 100],
			['phonenumber', 'length', 'max' => 15],
			['note', 'length', 'max' => 255],
      ['idstatus', 'numerical', 'integerOnly' => true],     
			['date', 'safe'],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'ID'),
			'date' => Yii::t('application', 'дата'),
			'name' => Yii::t('application', 'fbName'),
			'phonenumber' => Yii::t('application', 'номер телефона'),
			'idstatus' => Yii::t('application', 'статус'),
			'note' => Yii::t('application', 'примітка'),
		];
    }
    
    /**
     * @see CModel::relation()
     */
    public function relations()
    {
        return [
            'idstatus' => [self::BELONGS_TO, 'DictStatus', 'id'],
        ];
    }


}