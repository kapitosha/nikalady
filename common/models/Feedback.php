<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "feedback".
 *
 * The followings are the available columns in table 'feedback':
 *
 * @property integer $Id
 * @property string $date
 * @property string $name
 * @property string $phonenumber
 * @property integer $idstatus
 * @property string $note
 */
class Feedback extends BaseFeedback
{
     use RequestOfModel;
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return Feedback the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'feedback';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['name', 'length', 'max' => 100],
			['phonenumber', 'length', 'max' => 30],
			['note', 'length', 'max' => 255],
			['date', 'safe'],
                        ['phonenumber, name', 'required'],
                        ['idstatus','numerical','integerOnly' => true],    
                        ['phonenumber', 'validatePhone'], 
          
		];
    }
		    /**
     * @see CModel::relation()
     */
    public function relations()
    {
        return [
            'statuS' => [self::BELONGS_TO, 'status',    'idstatus',
                'joinType' => 'INNER JOIN', 'together'=>true,],
        ];
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'Id'),
			'date' => Yii::t('application', 'fbbate'),
			'name' => Yii::t('application', 'fbname'),
			'phonenumber' => Yii::t('application', 'fbphone'),
			'idstatus' => Yii::t('application', 'status'),
			'note' => Yii::t('application', 'note'),
		];
    }
    
    public function sendMessageToAdmin(Feedback $model)
    {
        $message = new YiiMailMessage;
        $message->view = 'callback';
        $message->setSubject('Заказан обратный звонок!!!');
        $message->setBody(['model' => $model], 'text/html');
        $message->addTo(Yii::app()->params['robotEmailTo']);
        $message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);
        
        
        if (Yii::app()->mail->send($message)) {
          //якщо повідомлення відправлена встановлюю йому відповідний статус = 1
          $model->idstatus=2;
          $model->save();
        };
    }   
    
  public function validatePhone($attribute, $value)
    {
      $length = mb_strlen($this->phonenumber);
      if ($length<7) {
        $this->addError('phonenumber', 'Номер телефона слишком короткий');
      }
    }
/**
     * @return CActiveDataProvider
     */
    public function dataProvider()
    {
        $criteria = new CDbCriteria;
        $criteria->with=['statuS' => ['together' => true],
                    //=>[
                    //      'joinType'=>'INNER JOIN'],
                         // 'compare'=>['statuS.name',$this->statuS.name, true], 
                        ];

        $criteria->compare('t.Id', $this->Id, true);
        $criteria->compare('t.date', $this->date, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.phonenumber', $this->phonenumber, true);
        $criteria->compare('t.idstatus', $this->idstatus, true);
        $criteria->compare('t.note', $this->note, true);
        // $criteria->compare('statuS.name', $this->statuS['name'], true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => 't.Id',
            ]
        ]);
    }
    
 /**
   * устанавливаем параметры фильтрации  на обьект модели dataProvider
   * @param type $dataProvider
   */  
  public function setFilter($dataProvider) {
		$dataProvider->unsetAttributes(); // clear any default values

		if(isset($_GET[get_class($dataProvider)])) {
			foreach ($_GET[get_class($dataProvider)] as $attr => $val) {
				$dataProvider->$attr = $val;
			}
		}    
    }
    
    
    
}