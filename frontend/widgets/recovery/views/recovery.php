<?php
/**
 * @var CActiveForm $form
 */
?>
<?php if (Yii::app()->user->isGuest) : ?>

    <div id="password-popup" class="box-modal password-popup">
        <span class="box-modal_close arcticmodal-close">x</span>
        <div class="popup-logo">
            <img src="/images/popup-logo.jpeg" alt=""/>
        </div>
        <p class="note">
            Введите адрес электронной почты, указанный при регистрации, нажмите кнопку Напомнить и вы получите письмо на указанный адрес с инструкциями для восстановления пароля.
        </p>
        <?php $form = $this->beginWidget('CActiveForm', [
            'id' => get_class($model),
            'action' => '/recovery/password',
            'enableAjaxValidation' => true,
            'clientOptions' => [
                'validateOnSubmit' => true,
                'validateOnChange' => true,
            ],
        ]); ?>
        <div class="form mini">
            <div class="row">
                <?= $form->labelEx($model, 'email'); ?>
                <?= $form->textField($model, 'email'); ?>
                <?= $form->error($model, 'email'); ?>
            </div>
            <div class="row">
                <a class="back-to-main" href="/" title=""><i></i>На главную</a>
                <input class="btn-green" type="submit" value="Напомнить"/>
            </div>
        </div>
        <?php $this->endWidget(); ?>
        <p class="note">
            Если у Вас остались вопросы, или возникли трудности, обратитесь в службу поддержки портала <b>AGROPOISK.NET</b> :
        </p>
        <div class="contact-info">
            <div>
                <p class="label">тел.:</p>
                <p class="info">(0382) 77 77 77</p>
            </div>
            <div>
                <p class="label">эл. адрес:</p>
                <p class="info">support@agropoisk.net</p>
            </div>
        </div>
    </div>

<?php endif; ?>