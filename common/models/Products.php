<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 *
 * @property integer $Id
 * @property string $productcode
 * @property string $c
 * @property string $description
 * @property string $price
 * @property string $wholesaleprice
 * @property string $image
 * @property string $category
 * @property string $subcategory
 * @property string $image2
 * @property string $image3
 * @property string $image4
 * @property string $image5
 * @property string $pricefrom
 */
class Products extends BaseProducts
{
    use RequestOfModel;
   
    public $img1;
    public $oldimg1;
    
    public $img2;
    public $oldimg2;

    public $img3;
    public $oldimg3;

    public $img4;
    public $oldimg4;

    public $img5;
    public $oldimg5;
    

    /**
     * Returns the static model of the specified AR class.
	   *
     * @param string $className active record class name.
     * @return BaseProducts the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'products';
    }
    
    //public $image;
     

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['description, tytle, price,  category,  productcode' , 'required'],
      ['productcode', 'length', 'max' => 50],
			['tytle, image, category, subcategory, image2, image3, image4, image5', 'length', 'max' => 100],
      ['pricefrom', 'length', 'max' => 20],
			['price', 'length', 'max' => 10],
      ['price','numerical', 'min'=>'0.0001'],    
			['wholesaleprice', 'length', 'max' => 15],
                       // ['wholesaleprice', 'default', 'value' => 0],
      ['wholesaleprice',  'default', 'setOnEmpty' => true, 'value' => null],
      ['image, image2, image3, image4, image5', 'file', 'types'=>'jpg, gif, png',
        'maxSize'=>1024 * 1024 * 5, // 5 MB
        'allowEmpty'=>'true',
        'tooLarge'=>'Размер Файла превышает 5 MB. Пожалуйста, загрузите файл меньшего размера.',
      ],
//      ['$img1, $img2, $img3, $img4, $img5','file','types'=>'jpg, gif, png, pdf',
//        'maxSize'=>1024 * 1024 * 5, // 5 MB
//        'allowEmpty'=>'true',
//        'tooLarge'=>'Размер Файла превышает 5 MB. Пожалуйста, загрузите файл меньшего размера.',
//      ],

		];
    }
	public function relations()
    {
        return [
            'catgrR' => [self::BELONGS_TO, 'categories',    'category',
                'joinType' => 'INNER JOIN', 
                //'on' => 'catgrR.Id=t.category',
                'together'=>true,],
            'subcatgrR' => [self::BELONGS_TO, 'subcategories',    'subcategory',
                'joinType' => 'INNER JOIN', 
                //'on' => 'subcatgrR.Id=t.subcategory',
                'together'=>true,],
      
            
        ];
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'Id'),
                        'productcode' => Yii::t('application', 'productcode'),
			'tytle' => Yii::t('application', 'name'),
			'description' => Yii::t('application', 'description'),
			'price' => Yii::t('application', 'price'),
			'wholesaleprice' => Yii::t('application', 'wholesaleprice'),
			'image' => Yii::t('application', 'image'),
			'category' => Yii::t('application', 'category'),
			'subcategory' => Yii::t('application', 'subcategory'),
			'image2' => Yii::t('application', 'image'),
			'image3' => Yii::t('application', 'image'),
			'image4' => Yii::t('application', 'image'),
			'image5' => Yii::t('application', 'image'),
                        'pricefrom' => Yii::t('application', 'pricefrom'),

		];
    }
/**
     * @return CActiveDataProvider
     */
    public function dataProvider()
    {
        $criteria = new CDbCriteria;
            $criteria->with=['catgrR' => ['together' => true],
                            'subcatgrR' => ['together' => true],
                    //=>[
                    //      'joinType'=>'INNER JOIN'],
                         // 'compare'=>['catgrR.name',$this->catgrR.name, true], 
                        ];
              // $criteria->with=['subcatgrR' => ['together' => true],
                    //=>[
                    //      'joinType'=>'INNER JOIN'],
                         // 'compare'=>['subcatgrR.name',$this->subcatgrR.name, true], 
              //          ];
        $criteria->compare('t.Id', $this->Id, true);
        $criteria->compare('t.productcode', $this->productcode, true);
        $criteria->compare('t.tytle', $this->tytle, true);
        $criteria->compare('t.price', $this->price, true);
        $criteria->compare('t.wholesaleprice', $this->wholesaleprice, true);
        $criteria->compare('t.image', $this->image, true);
        $criteria->compare('t.image2', $this->image2, true);
        $criteria->compare('t.image3', $this->image3, true);
        $criteria->compare('t.image4', $this->image4, true);
        $criteria->compare('t.image5', $this->image5, true);
        $criteria->compare('t.category', $this->category, true);
        $criteria->compare('t.subcategory', $this->subcategory, true);
        $criteria->compare('t.pricefrom', $this->pricefrom, true);
          // $criteria->compare('catgrR.name', $this->catgrR['name'], true);
          // $criteria->compare('subcatgrR.name', $this->subcatgrR['name'], true);
        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => 't.Id',
            ]
        ]);
    }
     public function setFilter($dataProvider) {
		$dataProvider->unsetAttributes(); // clear any default values

		if(isset($_GET[get_class($dataProvider)])) {
			foreach ($_GET[get_class($dataProvider)] as $attr => $val) {
				$dataProvider->$attr = $val;
			}
		}    
    }
    
    public function getId() {
      $model= new Products;
      $criteria = new CDbCriteria;
      $criteria->select='max(Id)+1 as Id';
      $maxId=$model->model()->find($criteria);
      
      return $maxId['Id'];
      
    }
    
    
	protected function afterFind()
	{
    $this->oldimg1=$this->image;
    $this->oldimg2=$this->image2;
    $this->oldimg3=$this->image3;
    $this->oldimg4=$this->image4;
    $this->oldimg5=$this->image5;
    
		return parent::afterFind();
	}
    
  
	protected function afterDelete()
	{
    echo $this->image; exit;
		$this->removeImg($this->image);
    $this->removeImg($this->image2);
    $this->removeImg($this->image3);
    $this->removeImg($this->image4);
    $this->removeImg($this->image5);

		return parent::afterDelete();
	}
  
  /*
   * видаляю файл після видалення запису
   */
	protected function removeImg($img)
	{
		$filePath = Yii::getPathOfAlias('www.files') . DIRECTORY_SEPARATOR . $img;
		if (!file_exists($filePath)) {
			return false;
		}
		return unlink($filePath);
	}  
  
  
	/**
	 * @param UploadedFile $instance
	 * @return string
	 */
	public function saveImg(UploadedFile $instance, $filename)
	{
		$filePath = Yii::getPathOfAlias('www.files.') . DIRECTORY_SEPARATOR . $fileName;
		if ($instance->saveAs($filePath)) {
			$filePath = '/files/docs/' . $fileName;
			//Thumb::model()->resizeImage($filePath);
			return $filePath;
		}
	}  


}
