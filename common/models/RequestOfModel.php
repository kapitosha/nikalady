<?php

/**
 *
 * @since 0.1
 */
trait RequestOfModel
{
	public function setFromPost()
	{
    
		if (isset($_POST[__CLASS__])) {
			foreach ($_POST[__CLASS__] as $attr => $val) {
                if (is_array($val)) $val = serialize($val);
				$this->$attr = $val;
			}
			return true;
		}
		return false;
	}
} 