<div class="prod-view">
  <h1 style="text-align: center;"><b>Товар</b></h1>
  <div class="prod-image" >
    <img style='cursor: -webkit-zoom-in;-webkit-user-select: none;' src=<?=Yii::app()->getBaseUrl(true).'/files/'.$model->image?> width="406" height="546">
  </div>
  <div class="prod-options">
    <div class="row-high">
      <div class="left">
        <?=CHtml::activeLabel($model,"tytle", []); ?>
      </div>
      <div class="right" >
        <?=$model->tytle; ?> 
      </div> 
    </div>
    <div class="row-high">
      <div class="left">
        <?=CHtml::activeLabel($model,"productcode", []); ?>
      </div>
      <div class="right" >
        <?=$model->productcode; ?> 
      </div> 
    </div>
    <div style="border-bottom:1px solid #BABABA; width: 470px; height: 4px;"></div>
    <div class="row-high">
      <div class="left">
        <?=CHtml::activeLabel($model,"category", []); ?>
      </div>
      <div class="right" >
        <?=$model->catgrR['name']; ?> 
      </div> 
    </div>
     <div class="row-high">
      <div class="left">
        <?=CHtml::activeLabel($model,"subcategory", []); ?>
      </div>
      <div class="right" >
        <?=$model->subcatgrR['name']; ?> 
      </div> 
    </div>   
    <div style="border-bottom:1px solid #BABABA; width: 470px; height: 4px;"></div>
    
    <div class="row-high">
      <div class="left">
        <?=CHtml::activeLabel($model,"price", []); ?>
      </div>
      <div class="right" >
        <?=$model->price; ?> 
      </div> 
    </div>
    <div class="row-high">
      <div class="left">
        <?=CHtml::activeLabel($model,"wholesaleprice", []); ?>
      </div>
      <div class="right" >
        <?=$model->wholesaleprice; ?> 
      </div> 
    </div>
    <br>
    <div class="row-high">
      <div class="left">
        <?=CHtml::activeLabel($model,"pricefrom", []); ?>
      </div>
      <div class="right" >
        <?=$model->pricefrom; ?> 
      </div> 
    </div>
    <div style="border-bottom:1px solid #BABABA; width: 470px; height: 4px;"></div>
    <div class="row-high">
      <div class="left">
        <?=CHtml::activeLabel($model,"description", []); ?>
      </div>
      <div class="right" >
        <?=CHtml::activeTextArea($model, "description", ["cols"=>"60", "rows"=>"26", "readonly"=>"readonly"]); ?>
      </div> 
    </div>
    
    
  </div>
</div>