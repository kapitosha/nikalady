<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "categories".
 *
 * The followings are the available columns in table 'categories':
 *
 * @property integer $Id
 * @property string $name
 * @property string $catid
 */
class Categories extends BaseCategories
{
    use RequestOfModel;
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseCategories the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'categories';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
          ['name', 'required'],
          ['name','validateName'],
          ['name', 'length', 'max' => 100],
          ['catid', 'length', 'max' => 40],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'Id'),
			'name' => Yii::t('application', 'name'),
			'catid' => Yii::t('application', 'shname'),
		];
    }

    /**
     * @return CActiveDataProvider
     */
    public function dataProvider()
    {
        $criteria = new CDbCriteria;
        //$criteria->order = 'Id';

        $criteria->compare('Id', $this->Id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('catid', $this->catid, true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => 'Id',
            ]
        ]);
    }
    
  /**
   * проверка поля name на уникальность
   * @param type $attribute
   * 
   */  
  public function validateName ($attribute)
    {
      //$value=mb_strtolower($value);
      if ($attribute=='name') {
        $status=$this->find('LOWER(name)=:name',[':name'=> strtolower($this->name)]);
        if (sizeof ($status)>0) 
        {
            $this->addError('name', ' такое значение уже зарегистрировано!'.$status->name
              //$value
              );
        }    
      };
    
    }
    
  /**
   * устанавливаем параметры фильтрации  на обьект модели dataProvider
   * @param type $dataProvider
   */  
  public function setFilter($dataProvider) {
		$dataProvider->unsetAttributes(); // clear any default values

		if(isset($_GET[get_class($dataProvider)])) {
			foreach ($_GET[get_class($dataProvider)] as $attr => $val) {
				$dataProvider->$attr = $val;
			}
		}    
    }    
    
  /**
	 * @return array list categories
	 */
	public function listCategories()
	{
		$criteria = new CDbCriteria;
		$criteria->select = ['Id', 'name'];
		$criteria->order = 'name';
    $criteria->distinct=true;

		$listCategories = [];
		if ($models = $this->findAll($criteria)) {
			$listCategories = CHtml::listData($models, 'Id', 'name');
		}
		return $listCategories;
	}
  
public function mnuCategories()
  {
  		$criteria = new CDbCriteria;
      $criteria->select = ['Id', 'name', 'catid'];
      //$criteria->order = 'name';
      $criteria->distinct=true;
    $mnuCategories=[];
    if ($models = $this->findAll($criteria)) {
      foreach ($models as $value){
         //підкатегорії
         $mnuSubCategories=[];
         $mnuSubCategories=Subcategories::model()->mnuSubCategories($value['Id']);
         
//         echo $value['Id'].'<br/>';
//         echo '<pre>';
//         print_r($mnuSubCategories);
//         echo '</pre>';
 
        if (count($mnuSubCategories)<1) { 
        array_push($mnuCategories,['label' => $value['name'], 
              'url' => Yii::app()->getBaseUrl(true).'/products/productbycategory/Id/'.$value['Id'], 
              'itemOptions'=>['class'=>'level0 nav-1 level-top parent']]);
        } else {array_push($mnuCategories,['label' => $value['name'], 
              'url' => '', 
              'items'=>$mnuSubCategories, 
              'itemOptions'=>['class'=>'level0 nav-1 level-top parent']]);}
       //, 'class'=>'level0 nav-1 level-top first parent'
      } 
    }  
    return $mnuCategories;
  }
  

    
    
}

