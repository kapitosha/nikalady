<?php
/**
 * @link http://web-systems.com.ua/
 * @link http://price-r.ru/
 * @copyright Copyright (c) 2013 - 2014 price-R.ru
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * Віджет виводу форми авторизації
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class LoginWidget extends CWidget
{
	/**
	 * @var string назва моделі форми для авторизації
	 */
	public $classModelLoginForm = 'LoginForm';

	public function run()
	{
		if (!defined('CRYPT_BLOWFISH') || !CRYPT_BLOWFISH) {
			throw new CHttpException(500, 'This application requires that PHP was compiled with Blowfish support for crypt().');
		}

		$model = new $this->classModelLoginForm;

		// display the login form
		$this->render('signIn', ['model' => $model]);
	}
} 