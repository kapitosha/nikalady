<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "registration".
 *
 * The followings are the available columns in table 'registration':
 *
 * @property integer $Id
 * @property string $email
 * @property string $note
 */
class Registration extends BaseRegistration
{
     use RequestOfModel;
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseRegistration the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'registration';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['email', 'length', 'max' => 100],
      ['email', 'email'],
             ['note', 'safe'],
          
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'Id'),
			'email' => Yii::t('application', 'Email'),
                        'note' => Yii::t('application', 'note'),
		];
    }
    public function dataProvider()
    {
        $criteria = new CDbCriteria;
        //$criteria->order = 'Id';

        $criteria->compare('Id', $this->Id, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('note', $this->note, true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => 'Id',
            ]
        ]);
    }
     protected function sendMessageToUser(Inmessg $model)
    {
        $message = new YiiMailMessage;
        $message->view = 'user/register';
        $message->setSubject('сообщение от'.$model->email);
        $message->setBody(['model' => $model], 'text/html');
        $message->addTo(Yii::app()->params['robotEmailTo'], $model->contactPerson);
        $message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);
        Yii::app()->mail->send($message);
    }
    
    public function sendMessageToAdmin(Inmessg $model)
    {
        $message = new YiiMailMessage;
        $message->view = 'registration';
        $message->setSubject('сообщение от '.$model->email);
        $message->setBody(['model' => $model], 'text/html');
        $message->addTo(Yii::app()->params['robotEmailTo']);
        $message->setFrom(Yii::app()->params['robotEmail'], Yii::app()->name);

        if (Yii::app()->mail->send($message)) {
          //якщо повідомлення відправлена встановлюю йому відповідний статус = 1
          $model->idstatus=2;
          $model->save();
        };
    }       
    
  public function validateEmail($attribute, $value)
    {
      $length = mb_strlen($this->email);
      if ($length<3) {
        $this->addError('email', 'email слишком короткий');
      }
      $instr=stristr($this->email,'@');
      $length = mb_strlen($instr);
      if ( $instr===FALSE ) {
        $this->addError('email', 'Введен неверный email');
      } elseif ($length<2) {
        $this->addError('email', 'Введен неверный email');
      } 
    } 
    /**
   * устанавливаем параметры фильтрации  на обьект модели dataProvider
   * @param type $dataProvider
   */  
  public function setFilter($dataProvider) {
		$dataProvider->unsetAttributes(); // clear any default values

      if(isset($_GET[get_class($dataProvider)])) {
        foreach ($_GET[get_class($dataProvider)] as $attr => $val) {
          $dataProvider->$attr = $val;
        }
      }    
    }
        
}


