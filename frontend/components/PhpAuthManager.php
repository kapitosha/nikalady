<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class PhpAuthManager extends CPhpAuthManager
{
	public function init()
	{
		if ($this->authFile === null) {
			$this->authFile = Yii::getPathOfAlias('application.config.auth') . '.php';
		}

		parent::init();

		if (!Yii::app()->user->isGuest) {
			$this->assign(Yii::app()->user->role, Yii::app()->user->id);
		}
	}
}