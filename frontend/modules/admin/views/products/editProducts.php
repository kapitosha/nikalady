<?php $this->breadcrumbs = [
	'Словарь работы с товарами' => ['ListProducts'],
($model->isNewRecord) ? 'Добавление' : 'Изменение' +' типы товаров'
]; ?>

<?php
/**
 * @var CController $this
 * @var TbActiveForm $form
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
	'Id' => get_class($model),
	'type' => 'horizontal',
	'htmlOptions' => ['class' => 'well', 'enctype' => 'multipart/form-data'],
	'enableAjaxValidation' => true,
]); ?>
<?= $form->dropDownListRow($model, 'category', Categories::model()->listCategories(), ['class' => 'span4', 'onchange'=>'chngSubCat()']); ?>

<?= $form->dropDownListRow($model, 'subcategory', 
  ( ($model->isNewRecord) ? 
      (CHtml::listData(Subcategories::model()->listSubCatgsbyCatg(array_keys(Categories::model()->listCategories())[0]), 'Id', 'name')) 
    : (CHtml::listData(Subcategories::model()->listSubCatgsbyCatg($model->category), 'Id', 'name'))), ['class' => 'span4']); ?>

<?= $form->textFieldRow($model, 'tytle', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'productcode', ['class' => 'span4']); ?>
<div class="control-group">
    <?= $form->labelEx($model, 'description', ['class'=>'control-label']); ?>
  <div class="controls">
    <!--<?= $form->telField($model, 'description', ['class' => 'right']); ?> -->
    <?= $form->textarea($model, 'description', ['cols' => '47', 'rows' => '8', 'class'=>'span4']) ;?>
    </div>
    <?= $form->error($model, 'description'); ?>
    <div class="clear"></div>  
  </div>  

<?= $form->textFieldRow($model, 'price', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'wholesaleprice', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'pricefrom', ['class' => 'span4']); ?>
<div>
<?php echo $form->textFieldRow($model, 'image', ['class' => 'span4']); ?>
    <?php echo $model->image ?>
<?php echo CHtml::activeFileField($model, 'img1'); ?>
</div> 
<div>
<?php echo $form->textFieldRow($model, 'image2', ['class' => 'span4']); ?>
    <?php echo $model->image2 ?>
<?php echo CHtml::activeFileField($model, 'img2'); ?>
</div> 
<div>
<?php echo $form->textFieldRow($model, 'image3', ['class' => 'span4']); ?>
    <?php echo $model->image3 ?>
<?php echo CHtml::activeFileField($model, 'img3'); ?>
</div> 
<div>
<?php echo $form->textFieldRow($model, 'image4', ['class' => 'span4']); ?>
    <?php echo $model->image4 ?>
<?php echo CHtml::activeFileField($model, 'img4'); ?>
</div> 
<div>
<?php echo $form->textFieldRow($model, 'image5', ['class' => 'span4']); ?>
    <?php echo $model->image5 ?>
<?php echo CHtml::activeFileField($model, 'img5'); ?>
</div> 

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => ($model->isNewRecord) ? 'Добавить' : 'Сохранить'
		]); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'reset',
			'label' => 'Сбросить'
		]); ?>
	</div>

<?php $this->endWidget(); ?>
