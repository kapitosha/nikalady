<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "status".
 *
 * The followings are the available columns in table 'status':
 *
 * @property integer $Id
 * @property string $Name
 * @property string $Note
 */
class BaseStatus extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseStatus the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'status';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['Name', 'length', 'max' => 100],
			['Note', 'length', 'max' => 255],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'ID'),
			'Name' => Yii::t('application', 'Назва'),
			'Note' => Yii::t('application', 'примітка'),
		];
    }
}

