<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "subcategories".
 *
 * The followings are the available columns in table 'subcategories':
 *
 * @property integer $Id
 * @property string $name
 * @property string $note
 * @property integer $idcatgs
 */
class Subcategories extends BaseSubcategories
{
     use RequestOfModel;
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseSubcategories the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'subcategories';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
          ['name, idcatgs' , 'required'],
          ['idcatgs', 'numerical', 'integerOnly' => true],
          ['name, note', 'length', 'max' => 255],
          ['name','validateName'], 
          ['idcatgs','default', 'value' => '1']
          
		];
    }
    
	    /**
     * @see CModel::relation()
     */
    public function relations()
    {
        return [
            'catgrR' => [self::BELONGS_TO, 'categories',    'idcatgs',
                'joinType' => 'INNER JOIN', 
                //'on' => 'catgrR.Id=t.idcatgs',
                'together'=>true,],
        ];
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'Id'),
			'name' => Yii::t('application', 'name'),
			'note' => Yii::t('application', 'note'),
			'idcatgs' => Yii::t('application', 'idcatgs'),
		];
    }
    /**
     * @return CActiveDataProvider
     */
public function dataProvider()
    {
        $criteria = new CDbCriteria;
        $criteria->with=['catgrR' => ['together' => true],
                    //=>[
                    //      'joinType'=>'INNER JOIN'],
                         // 'compare'=>['catgrR.name',$this->catgrR.name, true], 
                        ];

        $criteria->compare('t.Id', $this->Id, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.note', $this->note, true);
        $criteria->compare('t.idcatgs', $this->idcatgs, true);
        // $criteria->compare('catgrR.name', $this->catgrR['name'], true);
        

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => 't.Id',
            ]
        ]);
    }
    
 /**
   * проверка поля name на уникальность
   * @param type $attribute
   * 
   */  
  public function validateName ($attribute)
    { 
      //$value=mb_strtolower($value);
      if ($attribute=='name') {
        $status=$this->find('LOWER(name)=:name',[':name'=> strtolower($this->name)]);
        if (sizeof ($status)>0) 
        { if ($this->_oldAttributes['name']!=$this->name) {
            $this->addError('name', ' такое значение уже зарегистрировано!'.$status->name
              //$value
              );
          }
        }    
      };
    
    }
    
  /**
   * устанавливаем параметры фильтрации  на обьект модели dataProvider
   * @param type $dataProvider
   */  
  public function setFilter($dataProvider) {
		$dataProvider->unsetAttributes(); // clear any default values

		if(isset($_GET[get_class($dataProvider)])) {
			foreach ($_GET[get_class($dataProvider)] as $attr => $val) {
				$dataProvider->$attr = $val;
			}
		}    
    }
    
 
 /**
  *доподнительные методы валидации
  * @var type 
  */   
 private $_oldAttributes = array();
 
  public function setOldAttributes($value)
  {
    $this->_oldAttributes = $value;
  }
  public function getOldAttributes()
  {
    return $this->_oldAttributes;
  }

  public function init()
  {
    $this->attachEventHandler("onAfterFind", function ($event)
    {
      $event->sender->OldAttributes = $event->sender->Attributes;
    });
  }
  public function listSubcategories()
	{
		$criteria = new CDbCriteria;
		$criteria->select = ['Id', 'name'];
		$criteria->order = 'name';
    $criteria->distinct=true;

		$listSubcategories = [];
		if ($models = $this->findAll($criteria)) {
			$listSubcategories = CHtml::listData($models, 'Id', 'name');
		}
		return $listSubcategories;
	}
  
  public function listSubCatgsbyCatg($id=NULL)
	{
		$criteria = new CDbCriteria;
		$criteria->select = ['Id', 'name'];
		$criteria->order = 'name';
    $criteria->distinct=true;
      if ($id!=null) {$criteria->addCondition('idcatgs=:idcatgs');
                      $criteria->params['idcatgs']=$id;
      };

    return $this->findAll($criteria);
  }
  
  
public function mnuSubCategories($id=NULL)
  {
  		$criteria = new CDbCriteria;
      $criteria->select = ['Id', 'name', 'idcatgs'];
      //$criteria->order = 'name';
      
      if ($id!=null) {$criteria->addCondition('idcatgs=:idcatgs');
                      $criteria->params['idcatgs']=$id;
      };
      $criteria->distinct=true;

//         echo $id.' <pre>';
//         print_r($criteria->params);
//         echo '</pre>';      
      
      $mnuSubCategories=[];
        if ($models = $this->findAll($criteria)) {
          foreach ($models as $value){
            array_push($mnuSubCategories,['label' => $value['name'], 
                'url' => Yii::app()->getBaseUrl(true).'/products/productbysubcategory/Id/'.$value['Id'],  
                'itemOptions'=>['class'=>'level1']]);  
             //, 'class'=>'level0 nav-1 level-top first parent'
          } 
        }
    
    return $mnuSubCategories;
  }
      
  
  
}    
    



