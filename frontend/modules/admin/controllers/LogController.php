<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class LogController extends AdminController
{
	/**
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return [
			['allow', // allow only admin
				'roles' => ['admin'],
			],
			['deny', // deny all users
				'users' => ['*'],
			],
		];
	}

	public function actionIndex()
	{

		$dataProvider = new ActionLog;  //ActionLog::model()->dataProvider();
		$columns = [
			'id',
			'title',
            [
              'header' => 'Электронная почта',
              'name'=>'vitrEmail',
              'value'=>'$data->user->email',
             ],
            //   'user.email',
			'createdAt',
			[
				'type' => 'html',
				'value' => 'CHtml::link(\'Подробнее\', [\'readMore\', \'id\' => $data->id], [\'class\' => \'btn btn-block btn-warning\'])'
			],
		];
        
        $dataProvider->unsetAttributes(); // clear any default values
        if(isset($_GET['ActionLog'])) {
            foreach ($_GET['ActionLog'] as $attr => $val) {
                $dataProvider->$attr = $val;
            }
        }
        
		$this->render('index', ['dataProvider' => $dataProvider, 'columns' => $columns]);
	}

	public function actionReadMore($id)
	{
		if (!$model = ActionLog::model()->findByPk($id)) {
			throw new CHttpException(404);
		}
		$this->render('readMore', ['model' => $model]);
	}
} 