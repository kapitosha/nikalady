<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

require_once Yii::getPathOfAlias('thumb') . DIRECTORY_SEPARATOR . 'ThumbLib.inc.php';

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class Thumb
{
	/**
	 * @var int максимальна ширина зображення для рисайзу перед збереженням
	 */
	protected $maxWidth = 1980;
	/**
	 * @var int максимальна висота зображення для рисайзу перед збереженням
	 */
	protected $maxHeight = 1024;

	/**
	 * Resize зображення та збереження нової версії на місце попередньої
	 * @param $imagePath string відносний шлях до зображення
	 * відповідно до точки входу. Наприклад (/files/image.jpg)
	 */
	public function resizeImage($imagePath)
	{
		$filePath = Yii::getPathOfAlias('www') . DIRECTORY_SEPARATOR . $imagePath;
		$fullPath = 'http://' . Yii::app()->request->serverName . $imagePath;
		$thumb = PhpThumbFactory::create($fullPath);
		$thumb->resize($this->maxWidth, $this->maxHeight);
		$thumb->save($filePath);
	}

	/**
	 * @param $imagePath string шлях до забраження
	 * (через відносний шлях відносно точки входу, або через віддалений доступ до файлу,
	 * використовуючи інший ресурс)
	 * @param $width int ширина thumb
	 * @param $height int висота thumb
	 */
	public function getPathImageWithResize($imagePath, $width, $height)
	{
		$fileName = $this->getImageNameWithResize($imagePath, $width, $height);
		$filePath = Yii::getPathOfAlias('www.thumbs') . DIRECTORY_SEPARATOR . $fileName;
		if (!file_exists($filePath)) {
			$fullPath = ($this->isRemotePath($imagePath)) ? $imagePath : 'http://' . Yii::app()->request->serverName . $imagePath;
			$thumb = PhpThumbFactory::create($fullPath);
			$thumb->resize($width, $height);
			$thumb->save($filePath);
		}
		$thumb = PhpThumbFactory::create($filePath);
		$thumb->show();
	}

	/**
	 * Is remote path?
	 * @param $path string path
	 * @return bool
	 */
	public function isRemotePath($path)
	{
		return preg_match('/http.*/', $path) != false;
	}

	/**
	 * Отримання назви файлу thumb з параметрами w/h цього thumb
	 * Наприклад my_img_640_380.jpg, де 640 та 380, ширина та висота відповідно
	 * @param $imagePath string шлях до зображення
	 * @param $width string ширина зображення, яка буде підставлена у назву
	 * @param $height string висота зображення, яка буде підставлена у назву
	 * @return string назва файлу для thumb (з параметрами висоти та ширини зображення)
	 */
	protected function getImageNameWithResize($imagePath, $width, $height)
	{
		$imageFullName = $this->getFullNameImage($imagePath);
		$imageElements = explode('.', $imageFullName);
		$typeImage = $imageElements[count($imageElements) - 1];
		$imageName = str_replace($typeImage, '', $imageFullName);

		$imageNameWithResize = "{$imageName}_{$width}_{$height}";

		return $imageNameWithResize . '.' . $typeImage;
	}

	/**
	 * Отримання назви файлу з шляху до цього файла
	 * @param $imagePath string шлях до зображення
	 * @return string назва файлу
	 */
	protected function getFullNameImage($imagePath)
	{
		$imagePathAr = explode('/', $imagePath);
		return $imagePathAr[count($imagePathAr) - 1];
	}

	/**
	 * @return Thumb the static model class
	 */
	public static function model()
	{
		return new self();
	}
} 