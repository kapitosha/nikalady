<?php $this->breadcrumbs = [
	'Словарь работы с обратными звонками' => ['ListFeedback'],
($model->isNewRecord) ? 'Добавление' : 'Изменение' +' типы звонков'
]; ?>

<?php
/**
 * @var CController $this
 * @var TbActiveForm $form
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
	'Id' => get_class($model),
	'type' => 'horizontal',
	'htmlOptions' => ['class' => 'well'],
	'enableAjaxValidation' => true,
]); ?>
<div class="control-group">
  <?= $form->LabelEx($model, 'name', ['class' => 'control-label']); ?>
  <div class = "controls span4 lbl_span"> <?= $model->name; ?>  </div>
</div>
<div class="control-group">
  <?= $form->LabelEx($model, 'phonenumber', ['class' => 'control-label']); ?>
  <div class = "controls span4 lbl_span"> <?= $model->phonenumber; ?>  </div>
</div>

<?= $form->dropDownListRow($model, 'idstatus', Status::model()->listStatus(), ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'note', ['class' => 'span4']); ?>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => ($model->isNewRecord) ? 'Добавить' : 'Сохранить'
		]); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'reset',
			'label' => 'Сбросить'
		]); ?>
	</div>

<?php $this->endWidget(); ?>
