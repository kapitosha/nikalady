<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * Helper class to work with files
 *
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class FileHelper extends CFileHelper
{
	const GIT_IGNORE = '.gitignore';

	/**
	 * Removes a directory recursively.
	 * Since by removing the base directory, and file .gitignore.
	 *
	 * Example of use:
	 * <pre>
	 *  FileHelper::removeDir('dir', true);
	 * </pre>
	 *
	 * @param string $directory directory to be deleted recursively.
	 * @param bool $doNotRemoveBaseDirectory flag for the remission base
	 * directory. By default false.
	 * @param bool $doNotRemoveGitIgnoreFile flag for the remove .gitignore file.
	 * By default true
	 */
	public static function removeDir($directory, $doNotRemoveBaseDirectory = false, $doNotRemoveGitIgnoreFile = true)
	{
		$items = glob($directory . DIRECTORY_SEPARATOR . '{,.}*', GLOB_MARK | GLOB_BRACE);
		foreach ($items as $item) {
			if (!file_exists(
					$directory . DIRECTORY_SEPARATOR . self::GIT_IGNORE
				) && $doNotRemoveGitIgnoreFile === true
			) {
				$doNotRemoveGitIgnoreFile = false;
			}
			if ($doNotRemoveGitIgnoreFile === true) {
				if (basename($item) == '.' || basename($item) == '..' || basename($item) == self::GIT_IGNORE) {
					continue;
				}
			} else {
				if (basename($item) == '.' || basename($item) == '..') {
					continue;
				}
			}
			if (substr($item, -1) == DIRECTORY_SEPARATOR) {
				self::removeDir($item);
			} else {
				unlink($item);
			}
		}
		if (is_dir($directory)) {
			if ($doNotRemoveBaseDirectory === false) {
				rmdir($directory);
			}
		}
	}

	/**
	 * Recursively of removal of the list of directories, with a set of
	 * parameters for removal.
	 *
	 * Example of use:
	 * <pre>
	 *   FileHelper::removeDirectoryByList(array('dir1, dir2, dir3'), array(
	 *   'doNotRemoveBaseDirectory' => true,
	 *   'doNotRemoveGitIgnoreFile' => false
	 *   ));
	 * </pre>
	 *
	 * @param array $listDirectory list of directories that will be removed.
	 * @param array $options options for directory remove. Valid options are:
	 * <ul>
	 * <li>doNotRemoveBaseDirectory - flag for the remission base directory. By default false;</li>
	 * <li>doNotRemoveGitIgnoreFile - flag for the remove .gitignore file. By default true.</li>
	 * </ul>
	 */
	public static function removeDirectoryByList(array $listDirectory, array $options = [])
	{
		$doNotRemoveBaseDirectory = false;
		$doNotRemoveGitIgnoreFile = true;
		extract($options);
		foreach ($listDirectory as $directory) {
			if (file_exists($directory)) {
				self::removeDir($directory, $doNotRemoveBaseDirectory, $doNotRemoveGitIgnoreFile);
			}
		}
	}

	/**
	 * Chmod files by list, with a set mode param.
	 *
	 * Example of use:
	 * <pre>
	 *  FileHelper::chmodByList(array('dir1, dir2, dir3'), 0755);
	 * </pre>
	 *
	 * @param array $listFileNames list files that well be chmod
	 * @param int $mode note that mode is not automatically assumed to be an
	 * octal value, so strings (such as "g+w") will not work properly.
	 * To ensure the expected operation, you need to prefix mode with a zero (0):
	 */
	public static function chmodByList(array $listFileNames, $mode = 0777)
	{
		foreach ($listFileNames as $fileName) {
			if (file_exists($fileName)) {
				chmod($fileName, $mode);
			}
		}
	}

	/**
	 * Exists file by url
	 *
	 * @param $url string url file
	 * @return bool
	 */
	public static function existsUrl($url)
	{
		return (@get_headers($url)[0] == 'HTTP/1.0 404 Not Found') ? false : true;
	}
}
