<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class SiteController extends Controller
{
	/**
	 * @see CController::actions()
	 */
	public function actions()
	{
		return [
			'page' => [
				'class' => 'ViewAction',
			],
		];
	}
    
        public function actionAbout() {
            
            $this->render('about');  
        }
        public function actionCategories() {
             $this->render('categories');
          }
        public function actionSubcategories() {
             $this->render('subcategories');
          }
        public function actionProducts() {
             $this->render('products');
          }
        public function actionFeedback() {
             $this->render('feedback');
          }
        public function actionInmessg() {
             $this->render('inmessg');
          }
        public function actionCheckout() {
             $this->render('checkout');
          }
        public function actionContactus() {
            
            $this->render('formfeedback');
        }

     public function actionRegistration ($email = NULL)
    {
      $model = new Registration;
      
      $model->email=$email;
     
     try {
      
      if ($model->save()) {
        //echo $email; exit;
        $this->renderPartial('registrationYes');
        
      } else {
        //echo $email; exit;
        $this->renderPartial('registrationNo');
      }
     } catch (Exception $e) {
       $this->renderPartial('registrationNo');
     };
        
    }
    
  public function actionCallBack ($popup = false)
    {
      $model = new Feedback;

      Yii::app()->clientScript->scriptMap = array(
          // В карте отключаем загрузку core-скриптов, УЖЕ подключенных до ajax загрузки
          'jquery.js' => false,
          'jquery.ui.js' => false,
          'jquery-ui.min.js' => false,
          'jquery-ui-no-conflict.min.js'=>false,

      );
    if ($model->idstatus==NULL) {$model->idstatus = 0;};     
      //виконується перевірка даних якщо описана функція валідації
      $this->performAjaxValidation($model,  get_class($model) );
      
      //якщо є форма то зберігаю дані
      if (isset($_POST[ get_class($model)])) {
        $model->attributes = $_POST[ get_class($model)];
        //тут статус "необроблено"=0 для нових повідомлень
        $model->idstatus = 0;
        //якщо треба то тут розкодовується номер
        //$model->phoneNumber=str_replace('-','', str_replace(' ', '', $model->phoneNumber));
        
        if ($model->save()) {
            //відправка повідомлення на пошту
            Feedback::model()->sendMessageToAdmin($model);
            //перенаправляю на іншу сторінку після збереження
            $this->redirect('/');
          Yii::app()->end(); 
        }     
      }             
      //echo '$popup'; exit;  
        
      if ($popup==true) {
       // echo $popup; exit;

        $this->renderPartial('callback', ['model' => $model], false, true);
        
      } else {
        
        $this->render('callback', ['model' => $model]);  
      }
      
    }
    public function actionReviewes ($popup = false)
    {
      $model = new Reviewes;

      Yii::app()->clientScript->scriptMap = array(
          // В карте отключаем загрузку core-скриптов, УЖЕ подключенных до ajax загрузки
          'jquery.js' => false,
          'jquery.ui.js' => false,
          'jquery-ui.min.js' => false,
          'jquery-ui-no-conflict.min.js'=>false,

      );
    //if ($model->idstatus==NULL) {$model->idstatus = 0;};     
      //виконується перевірка даних якщо описана функція валідації
      $this->performAjaxValidation($model,  get_class($model) );
      
      //якщо є форма то зберігаю дані
      if (isset($_POST[ get_class($model)])) {
        $model->attributes = $_POST[ get_class($model)];
        //тут статус "необроблено"=0 для нових повідомлень
       //$model->idstatus = 0;
        //якщо треба то тут розкодовується номер
        //$model->phoneNumber=str_replace('-','', str_replace(' ', '', $model->phoneNumber));
        
        if ($model->save()) {
            //відправка повідомлення на пошту
            Reviewes::model()->sendMessageToAdmin($model);
            //перенаправляю на іншу сторінку після збереження
            $this->redirect('/');
          Yii::app()->end(); 
        }     
      }             
      //echo '$popup'; exit;  
        
      if ($popup==true) {
       // echo $popup; exit;

        $this->renderPartial('reviewes', ['model' => $model], false, true);
        
      } else {
        
        $this->render('reviewes', ['model' => $model]);  
      }
      
    }
    
public function actionFormfeedback ($popup = false)
    {
      $model = new Inmessg;
      Yii::app()->clientScript->scriptMap = array(
          // В карте отключаем загрузку core-скриптов, УЖЕ подключенных до ajax загрузки
          'jquery.js' => false,
          'jquery.ui.js' => false,
          'jquery-ui.min.js' => false,
          'jquery-ui-no-conflict.min.js'=>false,

      );
      
      
    if ($model->idstatus==NULL) {$model->idstatus = 1;};  
    //виконується перевірка даних якщо описана функція валідації
      $this->performAjaxValidation($model, get_class($model));
      
      
      //якщо є форма то зберігаю дані
      if (isset($_POST[get_class($model)])) {
        $model->attributes = $_POST[get_class($model)];
        //тут треба додати статус "необроблено"=0
        $model->idstatus = 0;
        
        if ($model->save()) {
            //тут треба добавити відправку повідомлення на пошту
            $model->SendMessageToAdmin($model);
            //перенаправляю на іншу сторінку після збереження
            $this->redirect('/');
          Yii::app()->end(); 
        }     
      }             
      //echo '$popup'; exit;  
        
      if ($popup==true) {
       // echo $popup; exit;
        
        $this->renderPartial('formfeedback', ['model' => $model], false, true);
        //$this->redirect('formfeedback', ['model' => $model]);
      } else {
      $this->render('formfeedback', ['model' => $model]);  
      }
      
    }
  public function actionAbout1(){
      $model = CoPartners::model()->findAll();
      
      $this->render('about1',['model'=>$model]);
    }

    /**
	 * This is the default 'index' action that is invoked
	 */
	public function actionIndex()
	{
  	//$model = new Registration;
    
    // $this->performAjaxValidation($model, 'Registration');
//    
//        if ($model->setFromPost() && $model->save()) {
//                $this->redirect(['Index']);
      
		//$this->render('index', ['model' => $model]);
    $this->render('index');
	}
        
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}
    /** 
     * попап око если пользователь не активирован
     */
    public function actionPopUser () {
      if (Yii::app()->getRequest()->isAjaxRequest)
                $this->renderPartial('popupuser');
              else $this->render('popupuser'); 
      
    }

    /** 
     * попап око если пользователь не оплатил использование рсурса
     */
    public function actionPopPay () {
      if (Yii::app()->getRequest()->isAjaxRequest)
                $this->renderPartial('popuppay');
              else $this->render('popuppay');       
    }

    
  public function actionNewsLetter($email)
    {
    
    }
}