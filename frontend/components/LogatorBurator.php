<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class LogatorBurator extends CComponent
{
	public function init()
	{}

	public function setOldParams(array $params)
	{
		Yii::app()->session['old_params'] = $params;
	}

	public function getOldParams()
	{
		if (empty(Yii::app()->session['old_params'])) {
			return false;
		}
		return Yii::app()->session['old_params'];
	}

	public function log($title, $message = null, array $oldParams = [])
	{
//		if (Yii::app()->user->role != User::ROLE_OPERATOR) {
//			return false;
//		}
		if (!empty($oldParams)) {
			$this->setOldParams($oldParams);
		} else {
			$oldParams = $this->oldParams;
		}

		$viewName = null;
		$params = [];

		if (is_array($message)) {
			extract($message);
			if (empty($viewName)) {
				$viewName = Yii::app()->controller->action->id;
			}
            
			$viewPath = '//logator/' . Yii::app()->controller->id . '/' . $viewName;

			$messageText = Yii::app()->controller->renderPartial($viewPath, ['params' => $params, 'oldParams' => $oldParams], true);
		} else {
			$messageText = $message;
		}

		$model = new ActionLog;
		$model->userid = Yii::app()->user->id;
		$model->createdAt = new CDbExpression('NOW()'); //date('c');
		$model->title = $title;
		$model->message = $messageText;

		$model->save();
	}
}