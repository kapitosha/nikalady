<?php if (Yii::app()->getRequest()->isAjaxRequest) : ?>
<span class="box-modal_close arcticmodal-close" >x</span>
<?php endif; ?>  

<div class="form popup">
  <?php $form = $this->beginWidget('CActiveForm', [
                  'id' => get_class($model),
                  'enableAjaxValidation' => true,
                  'clientOptions' => [
                          'validateOnSubmit' => true,
                          'validateOnChange' => true,
                  ],
                  'htmlOptions' => [
                         // 'enctype' => 'multipart/form-data',
                  ],
          ]); ?>
  
    <div class="row-high">
      <?= $form->labelEx($model, 'name', ['class'=>'left']); ?>
      <?= $form->textField($model, 'name', ['class' => 'right']); ?>
      <?= $form->error($model, 'name'); ?>
      <div class="clear"></div>  
    </div>
    <div class="row-high">
      <?= $form->labelEx($model, 'phonenumber', ['class'=>'left']); ?>
      <?= $form->telField($model, 'phonenumber', ['class' => 'right phone', //'id'=>'phoneNumber',
          'placeholder'=>'Введите номер вашего телефона'
        ]); ?>
      <?= $form->error($model, 'phonenumber'); ?>
      <div class="clear"></div>  
    </div>  
  
    <div class="row btns">
      <input class="btn-opacity btn-do" type="submit" name="yt0" value="<?= Yii::t('application','btsend')?>"/>
    </div>    
    
  <?php $this->endWidget(); ?>  
</div>

<script type="text/javascript">
  jQuery('.phone').attr({'placeholder':'+38(___) ___-__-__'});
</script>