<?php $this->breadcrumbs = [
	'Ошибка ' . $code,
]; ?>

<h2></h2>

<div class="error error404">
    <div class="error-img">
    	
    </div>
    <p>Запрашиваемая Вами страница не найдена, или такой страницы не существует.</p>
    <p>Проверьте правильности набранного вами адреса.<br>Если URL верен, то попробуйте зайти позже.</p>
    <a class="btn-opacity" href="<?= Yii::app()->getBaseUrl(true) ?>">На главную страницу</a>
</div>