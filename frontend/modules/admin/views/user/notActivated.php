<?php $this->breadcrumbs = [
	'Пользователи' => ['index'],
	'Не проверенные пользователи',
]; ?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
	'dataProvider' => $dataProvider,
	'fixedHeader' => true,
	'type'=>'striped bordered condensed',
	'columns' => $columns,
	'responsiveTable' => true,
	'pager' => [
		'class' => 'bootstrap.widgets.TbPager',
		'displayFirstAndLast' => true
	]
]); ?>
