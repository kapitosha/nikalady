<?php
/**
 * @var $this Controller
 */
?>

<?php $this->beginContent('admin.views.layouts.base'); ?>
<?php $this->widget('bootstrap.widgets.TbNavbar', [
	'type' => 'inverse',
	'brand' => 'Dashboard',
	'collapse' => true,
	'fixed' => false,
	'items' => [
		[
			'class' => 'bootstrap.widgets.TbMenu',
			'htmlOptions' => ['class' => 'pull-right'],
			'items' => [
				['label' => 'Главная', 'url' => ['/index']],
			],
		],
	],
]);?>
<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', [
	'links' => $this->breadcrumbs,
	'htmlOptions' => [
		'class' => 'breadcrumb'
	],
]);?>
<?php $this->widget('bootstrap.widgets.TbAlert', [
	'block' => true,
	'fade' => true,
	'userComponentId' => 'user',
	'alerts' => [
		'success',
		'info',
		'warning',
		'error'
	],
]);?>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<?php $this->widget('bootstrap.widgets.TbMenu', [
				'activateItems' => true,
				'activateParents' => true,
				'type' => 'list',
				'items' => [
					'---',
					['label' => 'Товары ' , 'url' => ['products/index']],
					'---',
					['label' => 'Рассылка ','url' => ['dicts/listregistration']],
					'---',
                                        ['label' => 'Сообщения', 'url'=> ['mess/listinmessg']],
					'---',
                                        ['label' => 'Обратный звонок', 'url'=> ['mess/listfeedback']],
					'---',
					['label' => 'Словари', 'itemOptions' => ['class' => 'nav-header']],
					'---',
					['label' => 'Состояния обработки', 'url' => ['dicts/liststatus']],
					'---',
					['label' => 'Категории товаров', 'url' => ['dicts/listcategories']],
                                        '---',
                                        ['label' => 'Подкатегории товаров', 'url' => ['dicts/listsubcategories']],
                                        '---',
                                        ['label' => 'Работа с товарами', 'url' => ['products/listproducts']],
				],
				'htmlOptions' => ['class' => 'well']
			]);?>
		</div>
		<div class="span9" id="content">
			<?= $content ?>
		</div>
	</div>
	<hr>
	<footer>
		<p class="text-center"><strong><?= Yii::app()->name; ?></strong> / <?= date('Y') ?><br/><?= Yii::powered()?></p>
	</footer>
<?php $this->endContent(); ?>