<?php $this->breadcrumbs = [
	'Словарь Состояний обработки',
]; ?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup', [
	'type' => 'primary',
	'buttons' => [
	   ['label' => 'Добавить состояние', 'url' => ['AddStatus']],
	],
]);?>

<?php $this->widget('bootstrap.widgets.TbExtendedGridView', [
    'dataProvider' => $dataProvider->dataProvider(),
    'fixedHeader' => true,
    'filter' => $dataProvider,
    'type'=>'striped bordered condensed',
    'columns' => $columns,
    'responsiveTable' => true,
    'pager' => [
        'class' => 'bootstrap.widgets.TbPager',
        'displayFirstAndLast' => true
    ]
]); ?>
