<?php

/* 
 * контролер відображення словників
 */

class MessController extends AdminController {
  
  //відображення гріда статусів
  public function actionListFeedback()
	{
    //Yii::app()->getDateFormatter()->formatDateTime($data->date, "short", "short");
    
    //готую дані з моделі
    $dataProvider = new Feedback('dataProvider');
    //формую перелік колонок для відображення
		$columns = [
			['name'=>'Id',
       'htmlOptions'=>['style'=>'width: 130px;'],
      ],
      ['name'=>'date',
       'type'=>'datetime',
       //'value'=>'Yii::app()->getDateFormatter()->formatDateTime($data->date, "short", "short")'
       //'datetimeFormat'=>'d.m.Y h:i:s A', 
        
      ],
      'name',
      'phonenumber',
			'note',
                       //'idstatus',
      ['name'=>'statuS.name', 'filter'=>true,
       'header' => 'Статус',
      ],
			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}', //{delete}
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'editFeedback\', [\'Id\' => $data->Id])',
				//'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeFeedback\', [\'Id\' => $data->Id])',
			]
		];    
    //устанавливаю значения по атрибутах
    Feedback::model()->setFilter($dataProvider); 
    
   //викликаю в'юшку і передаю підготовлені дані і опис колонок 
   $this->render('listFeedback',['dataProvider' => $dataProvider, 'columns' => $columns]); 
    }
    
  //видаляю запис з таблиці  
  public function actionRemoveFeedback($Id)
	{
    //видаляю запис з таблиці
    Feedback::model()->deleteByPk($Id);
    //перемальовую форму
    $this->redirect('/');
  }  

  //викликаю форму зміни запису в таблиці  
  public function actionEditFeedback($Id)
    {
        if (!$model = Feedback::model()->findByPk($Id)) {
                throw new CHttpException(404, 'Такого типа состояния не найдено в базе');
        }
        
        $this->performAjaxValidation($model, 'Feedback');
        
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['ListFeedback']);
        }
        $this->render('editFeedback', ['model' => $model]);
    }
    public function actionAddFeedback()
    {
  	$model = new Feedback;
                
      $this->performAjaxValidation($model, 'Feedback');
    
      
      
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['ListFeedback']);
        }
        $this->render('addFeedback', ['model' => $model]);
    
    }
     
    //відображення гріда статусів
  public function actionListInmessg()
	{
    //готую дані з моделі
    $dataProvider = new Inmessg; //('dataProvider');
    //формую перелік колонок для відображення
		$columns = [
			'Id',
      ['name'=>'date',
       'type'=>'datetime',
      ],
      'email',
      'subject',
      'text',
      ['name'=>'statuS.name', 'filter'=>true, ],
			//'idstatus',
      'note',
  			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}{sendmessg}',  //{delete}
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'editInmessg\', [\'Id\' => $data->Id])',
				//'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeInmessg\', [\'Id\' => $data->Id])',
        'buttons'=>[
          'sendmessg'=>[
            'label'=>'Отправить сообщение',
            'url' => 'Yii::app()->createUrl("/admin/mess/sendmess/", ["Id" => $data->Id])',
            'icon'=>'icon-envelope',
            //'options'=>['class'=>'btn btn-small',],
          ],
        ],  
			]
		];    
    
    //устанавливаю значения по атрибутах
    Inmessg::model()->setFilter($dataProvider);    
    
   //викликаю в'юшку і передаю підготовлені дані і опис колонок 
   $this->render('listInmessg',['dataProvider' => $dataProvider, 'columns' => $columns]); 
    }
    
  //видаляю запис з таблиці  
  public function actionRemoveInmessg($Id)
	{
    //видаляю запис з таблиці
    Inmessg::model()->deleteByPk($Id);
    //перемальовую форму
    $this->redirect('/');
  }  

  //викликаю форму зміни запису в таблиці  
  public function actionEditInmessg($Id)
    {
        if (!$model = Inmessg::model()->findByPk($Id)) {
                throw new CHttpException(404, 'Такого типа состояния не найдено в базе');
        }
        
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['ListInmessg']);
        }
        $this->render('editInmessg', ['model' => $model]);
    }
    public function actionAddInmessg()
    {
  	$model = new Inmessg;
    
      $this->performAjaxValidation($model, 'Inmessg');
    
      if ($model->setFromPost() && $model->save()) {
        $this->redirect(['ListInmessg']);
      }
        $this->render('AddInmessg', ['model' => $model]);
    
    }
 
  public function actionSendmess($Id)
    {
      $model = new Outmessg;
      
      if ($Imodel = Inmessg::model()->findbyPk($Id)) {
        if ($model->email==NULL) {$model->email=$Imodel->email;};
        if ($model->idmessg==null) {$model->idmessg=$Id;};
        if ($model->subject==NULL) {$model->subject='Сообщение сайта NikaLady';};
      };
      
        $this->performAjaxValidation($model, 'Outmessg');

        if ($model->setFromPost() && $model->save()) {
          Outmessg::model()->sendMessageToUser($model);
          $this->redirect(['ListInmessg']);
          Yii::app()->end();
        }
          $this->render('sendOutmessg', ['model' => $model]);
    
    }
    
}