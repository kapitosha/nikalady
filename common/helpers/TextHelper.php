<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * Helper class to work with text content
 *
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @author Volodymyr Gamula <v.gamula@gmail.com>
 * @since 0.2
 */
class TextHelper
{
	const TYPE_LETTERS = 1;
	const TYPE_WORDS = 2;
	const TYPE_SENTENCES = 3;

	/**
	 * Converting the Cyrillic alphabet in Latin alphabet
	 *
	 * Example of use:
	 * <pre>
	 *   TextHelper::cyrillic2Latin('Василь пішов купив чаю ;))', array(
	 *   'toLower' => false,
	 *   'otherCharacters' => true,
	 *   ));
	 * </pre>
	 *
	 * @param $inputText string input text content
	 * @param array $options options for convert. Valid options are:
	 * <ul>
	 * <li>toLower - flag to make a string lowercase. By default true;</li>
	 * <li>spacesReplace - flag to replace all characters on the "-".
	 * By default true.</li>
	 * <li>otherCharacters - flag the presence of other characters not included
	 * in the vocabulary. By default false.</li>
	 * </ul>
	 *
	 * @return mixed|string the result of converting
	 */
	public static function cyrillic2Latin($inputText, $options = [])
	{
		$toLower = true;
		$spacesReplace = true;
		$otherCharacters = false;

		extract($options);

		$vocabulary = [
			'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
			'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
			'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K',
			'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
			'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
			'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
			'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Shh', 'Ъ' => 'J',
			'Ы' => 'Y', 'Ь' => 'Q', 'Э' => 'Eh', 'Ю' => 'Ju',
			'Я' => 'Ja', 'І' => 'I', 'Ї' => 'Yi',

			'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g',
			'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
			'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k',
			'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
			'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
			'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
			'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => 'j',
			'ы' => 'y', 'ь' => 'q', 'э' => 'eh', 'ю' => 'ju',
			'я' => 'ja', 'і' => 'i', 'ї' => 'yi'
		];

		$outputText = strtr(trim($inputText), $vocabulary);

		$pattern = '/[^-0-9a-z';
		if ($toLower === true) {
			$outputText = strtolower($outputText);
		} else {
			$pattern .= 'A-Z';
		}
		if ($spacesReplace === true) {
			$outputText = str_replace(' ', '-', preg_replace('/\s{2,}/', ' ', $outputText));
		} else {
			$pattern .= '\s';
		}
		$pattern .= ']/';

		if ($otherCharacters === false) {
			$outputText = preg_replace($pattern, '', $outputText);
		}

		return $outputText;
	}

	/**
	 * This method returns parts of text by users demand.
	 * It can return letters, words or sentences.
	 *
	 * Example of use:
	 * <pre>
	 * $str1 = getPartOfText('Blah blah blah!', 4, TYPE_LETTERS); //returns 'Blah'
	 * $str2 = getPartOfText('Blah Blah Blah! La la la. Whop, Whop.', 3, TYPE_WORDS); //returns 'Blah Blah Blah!'
	 * $str3 = getPartOfText('Blah Blah Blah! Choeu-choeu! La la la. Whop, Whop.', 2, TYPE_SENTENCES); //returns 'Blah Blah Blah! Choeu-choeu!'
	 * </pre>
	 *
	 * @param $text string it contains text, which user wants to process and return
	 * @param $count int it contains count of letters or words or sentences which user wants to return
	 * @param int $type it contains type of users demand
	 *
	 * There are 3 constants, which can mean type
	 * First constant is TYPE_LETTERS . Function will return count of letters
	 * First constant is TYPE_WORDS . Function will return count of words
	 * First constant is TYPE_SENTENCES . Function will return count of sentences
	 *
	 * @return string, it is result of function.
	 */
	public static function getPartOfText($text, $count, $type = self::TYPE_SENTENCES)
	{
		if ($type == self::TYPE_LETTERS) {
			$text = substr($text, ($count < strlen($text) ? $count : strlen($text)));
		} elseif ($type == self::TYPE_WORDS) {
			$arr = explode(' ', $text);
			$text = '';
			for ($i = 0; $i < ($count < count($arr) ? $count : count($arr)); $i++) {
				$text .= $arr[$i] . ' ';
			}
		} elseif ($type == self::TYPE_SENTENCES) {
			$arr = preg_split('/(?<=[.?!:;])\s+/', $text);
			$text = '';
			for ($i = 0; $i < ($count < count($arr) ? $count : count($arr)); $i++) {
				$text .= $arr[$i] . ' ';
			}
		}
		return $text;
	}
}