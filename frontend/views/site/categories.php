<!--START OF MAIN CONTENT-->
    <div class="main-container col2-left-layout">
      <div class="main">
        <div class="col-main"> 
          <!--Category Title-->
          <div class="page-title category-title">
            <h1>Женская одежда</h1>
          </div>
          <!--Category Image-->
          <p class="category-image"><img src="images/women.jpg" alt="Womens" title="Womens"/></p>
          <div class="category-products"> 
            <!--Start toolbar-->
            <div class="toolbar">
              <div class="pager">
                <div class="limiter">
                  <label>Показать</label>
                  <select>
                    <option selected="selected"> 9 </option>
                    <option> 15 </option>
                    <option> 30 </option>
                  </select>
                </div>
              </div>
              <div class="sorter">
                <p class="view-mode">
                  <label>Вид:</label>
                  <strong title="Grid" class="grid">Сетка</strong>&nbsp; <a href="#" title="List" class="list">Список</a>&nbsp; </p>
              </div>
              <div class="pagination">
                <div class="pages"> <strong>Страница:</strong>
                  <ol>
                    <li class="current">1</li>
                    <li><a href="#">2</a></li>
                    <li> <a class="next i-next" href="#" title="Next"></a> </li>
                  </ol>
                </div>
              </div>
            </div>
            <!--End toolbar--> 
            
            <!--Start Category Product List-->
            <ul class="products-grid first odd">
              <li class="item first"> <a href="#" title="Lorem ipsum dolor sit amet," class="product-image"><img src="./images/pro1.jpg" alt="Lorem ipsum dolor sit amet,"/></a>
                <h2 class="product-name"> <a href="#" title="Lorem ipsum dolor sit amet,">Название,</a> </h2>
                <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                <div class="actions">
                  <button type="button" title="Add to Cart" class="button btn-cart"><span><span>В корзину</span></span></button>
                  <a href="#" class="fancybox quick_view">Быстрый просмотр</a>
                  <ul class="add-to-links">
                    <li><a href="#" class="link-wishlist">В список желаний</a></li>
                    <li class="last"><a href="#" class="link-compare">Сравнить</a></li>
                  </ul>
                </div>
              </li>
              <li class="item first"> <a href="#" title="Lorem ipsum dolor sit amet," class="product-image"><img src="./images/pro14.jpg" alt="Lorem ipsum dolor sit amet,"/></a>
                <h2 class="product-name"> <a href="#" title="Lorem ipsum dolor sit amet,">Название,</a> </h2>
                <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                <div class="actions">
                  <button type="button" title="Add to Cart" class="button btn-cart"><span><span>В корзину</span></span></button>
                  <a href="#" class="fancybox quick_view">Быстрый просмотр</a>
                  <ul class="add-to-links">
                    <li><a href="#" class="link-wishlist">В список желаний</a></li>
                    <li class="last"><a href="#" class="link-compare">Сравнить</a></li>
                  </ul>
                </div>
              </li>
              <li class="item last"> <a href="#" title="Lorem ipsum dolor sit amet," class="product-image"><img src="./images/pro22.jpg" alt="Lorem ipsum dolor sit amet,"/></a>
                <h2 class="product-name"> <a href="#" title="Lorem ipsum dolor sit amet,">Название,</a> </h2>
                <div class="price-box"> <span class="regular-price" > <span class="price">0.00</span> </span> </div>
                <div class="actions">
                  <button type="button" title="Add to Cart" class="button btn-cart"><span><span>В корзину</span></span></button>
                  <a href="#" class="fancybox quick_view">Быстрый просмотр</a>
                  <ul class="add-to-links">
                    <li><a href="#" class="link-wishlist">В список желаний</a></li>
                    <li class="last"><a href="#" class="link-compare">Сравнить</a></li>
                  </ul>
                </div>
              </li>
            </ul>
            <ul class="products-grid first odd">
              <li class="item first"> <a href="#" title="Lorem ipsum dolor sit amet," class="product-image"><img src="./images/pro9.jpg" alt="Lorem ipsum dolor sit amet,"/></a>
                <h2 class="product-name"> <a href="#" title="Lorem ipsum dolor sit amet,">Название,</a> </h2>
                <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                <div class="actions">
                  <button type="button" title="Add to Cart" class="button btn-cart"><span><span>В корзину</span></span></button>
                  <a href="#" class="fancybox quick_view">Быстрый просмотр</a>
                  <ul class="add-to-links">
                    <li><a href="#" class="link-wishlist">В список желаний</a></li>
                    <li class="last"><a href="#" class="link-compare">Сравнить</a></li>
                  </ul>
                </div>
              </li>
              <li class="item first"> <a href="#" title="Lorem ipsum dolor sit amet," class="product-image"><img src="./images/pro11.jpg" alt="Lorem ipsum dolor sit amet,"/></a>
                <h2 class="product-name"> <a href="#" title="Lorem ipsum dolor sit amet,">Название,</a> </h2>
                <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                <div class="actions">
                  <button type="button" title="Add to Cart" class="button btn-cart"><span><span>В корзину</span></span></button>
                  <a href="#" class="fancybox quick_view">Быстрый просмотр</a>
                  <ul class="add-to-links">
                    <li><a href="#" class="link-wishlist">В список желаний</a></li>
                    <li class="last"><a href="#" class="link-compare">Сравнить</a></li>
                  </ul>
                </div>
              </li>
              <li class="item last"> <a href="#" title="Lorem ipsum dolor sit amet," class="product-image"><img src="./images/pro6.jpg" alt="Lorem ipsum dolor sit amet,"/></a>
                <h2 class="product-name"> <a href="#" title="Lorem ipsum dolor sit amet,">Название,</a> </h2>
                <div class="price-box"> <span class="regular-price"> <span class="price">0.00</span> </span> </div>
                <div class="actions">
                  <button type="button" title="Add to Cart" class="button btn-cart"><span><span>В корзину</span></span></button>
                  <a href="#" class="fancybox quick_view">Быстрый просмотр</a>
                  <ul class="add-to-links">
                    <li><a href="#" class="link-wishlist">В список желаний</a></li>
                    <li class="last"><a href="#" class="link-compare">Сравнить</a></li>
                  </ul>
                </div>
              </li>
            </ul>
            <!--End Category Product List--> 
            
            <!--Start toolbar bottom-->
            <div class="toolbar-bottom">
              <div class="toolbar">
                <div class="pager">
                  <div class="limiter">
                    <label>Показать</label>
                    <select>
                      <option selected="selected"> 9 </option>
                      <option> 15 </option>
                      <option> 30 </option>
                    </select>
                  </div>
                </div>
                <div class="sorter">
                  <p class="view-mode">
                    <label>Вид:</label>
                    <strong title="Grid" class="grid">Сетка</strong>&nbsp; <a href="#" title="List" class="list">Список</a>&nbsp; </p>
                </div>
                <div class="pagination">
                  <div class="pages"> <strong>Страница:</strong>
                    <ol>
                      <li class="current">1</li>
                      <li><a href="#">2</a></li>
                      <li> <a class="next i-next" href="#" title="Next"></a> </li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
            <!--End toolbar bottom--> 
          </div>
        </div>
        <div class="col-left sidebar"> 
          <!--Start Magic Category Block-->
          <div class="magicat-container">
            <div class="block">
              <div class="block-title cat_heading"> <strong><span>Женская одежда</span></strong> </div>
              <ul id="magicat">
                <li class="first level0-inactive level0 inactive"><span class="magicat-cat"><a href="#"><span>Женские кофты,туники,блузы</span></a></span> </li>
                <li class="level0-inactive level0 inactive"><span class="magicat-cat"><a href="#"><span>Пиджаки,болеро женские</span></a></span> </li>
                <li class="level0-inactive level0 inactive"><span class="magicat-cat"><a href="#"><span>Женские платья,сарафаны</span></a></span> </li>
              </ul>
            </div>
          </div>
          <!--End Magic Category Block--> 
          
          <!--Start Layered nav-->
          <div class="block block-layered-nav">
            <div class="block-title"> <strong><span>Упорядочить по:</span></strong> </div>
            <div class="block-content">
              <div id="narrow-by-list">
                <div>
                  <div class="last collapse" id="filter_heading">Цена</div>
                  <div class="last odd" id="filter_content">
                    <ul>
                      <li> <a href="#"><span class="price">0.00</span> - <span class="price">0.00</span></a> (2) </li>
                      <li> <a href="#"><span class="price">0.00</span> - <span class="price">0.00</span></a> (9) </li>
                    </ul>
                  </div>
                  <script type="text/javascript">
						jQuery('#filter_heading').click(function() {
					      jQuery('#filter_content').slideToggle('slow');
						   jQuery(this).toggleClass("highlight");
					    });
					</script> 
                </div>
              </div>
            </div>
          </div>
          <!--End Layered nav--> 
          
          <!--Start Compare Products-->
          <div class="block block-list block-compare">
            <div class="block-title"><strong><span>Сравнить товары</span></strong> </div>
            <div class="block-content">
              <p class="empty">У Вас нет товаров для сравнения</p>
            </div>
          </div>
          <!--End Compare Products--> 
        </div>
      </div>
      <div style="display: none;" id="back-top"> <a href="#"><img alt="" src="images/backtop.gif"/></a> </div>
    </div>
    <!--END OF MAIN CONTENT--> 