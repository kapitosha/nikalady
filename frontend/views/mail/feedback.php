<?php
$this->pageTitle = $model->email . ', сообщение от пользователя сайта';
?>
<p>от пользователя сайта с адресом електронной почты : <?= CHtml::encode($model->email); ?> отправлено сообщение</p>
<br/>
<p>Тема : <?= CHtml::encode($model->subject); ?></p>
<br/>
<p>Сообщение:</p>
<p><?= CHtml::encode($model->text); ?></p>
<br/>
