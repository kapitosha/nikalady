<?php $this->breadcrumbs = [
	'Словарь работы с рассылкой' => ['ListRegistration'],
($model->isNewRecord) ? 'Добавление' : 'Изменение' +' типы рассылок'
]; ?>

<?php
/**
 * @var CController $this
 * @var TbActiveForm $form
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
	'Id' => get_class($model),
	'type' => 'horizontal',
	'htmlOptions' => ['class' => 'well'],
	'enableAjaxValidation' => true,
]); ?>

<?= $form->textFieldRow($model, 'email', ['class' => 'span4', 'placeholder'=>'Введите электронный адрес']); ?>
<?= $form->textFieldRow($model, 'subject', ['class' => 'span4']); ?>

<div class="control-group">
    <?= $form->labelEx($model, 'text', ['class'=>'control-label']); ?>
  <div class="controls">
    <!--<?= $form->telField($model, 'text', ['class' => 'right']); ?> -->
    <?= $form->textarea($model, 'text', ['cols' => '47', 'rows' => '8', 'class'=>'span4']) ;?>
    </div>
    <?= $form->error($model, 'text'); ?>
    <div class="clear"></div>  
</div>  

<!--
<div class="control-group">
    <?= $form->labelEx($model, 'note', ['class'=>'control-label']); ?>
  <div class="controls">
    <?= $form->textarea($model, 'note', ['cols' => '47', 'rows' => '8', 'class'=>'span4']) ;?>
    </div>
    <?= $form->error($model, 'note'); ?>
    <div class="clear"></div>  
</div>  
-->
  <div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => 'Отправить',
		]); ?>
		<?php 
//    $this->widget('bootstrap.widgets.TbButton', [
//			'buttonType' => 'reset',
//			'label' => 'Сбросить'
//		]); 
    ?>
	</div>

<?php $this->endWidget(); ?>
