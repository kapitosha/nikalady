       <!--START OF MAIN CONTENT-->
    <div class="main-container col1-layout">
      <div class="main">
        <div class="col-main">
          <div id="messages_product_view"></div>
          <div class="product-view">
            <div class="product-essential"> 
              <!--Start Product Information Right-->
              <div class="product-shop"> 
                <!--Prev/Next Code Start-->
                <div class="f-fix mb-10"><a href="#" class="prod-prev">ПРЕДЫДУЩИЙ</a> <a class="prod-next" href="#">СЛЕДУЮЩИЙ</a> </div>
                <!--Prev/Next Code End--> 
                <!--Product Title-->
                <div class="product-name">
                  <h1>Название</h1>
                </div>
                <p class="availability in-stock"><span>Наличие</span></p>
                <div class="price-box"> <span class="regular-price" id="product-price-167"> <span class="price">0.00</span> </span> </div>
                <div class="pro-left">
                  <div class="short-description">
                    <h2>Краткая информация</h2>
                    <div class="std">Текст краткой информации, </div>
                  </div>
                  <div class="review">
                    <div class="size_guide"><a class="ajax" href="./images/size_chart.gif"><span>Нажмите чтобы посмотреть таблицу размеров</span> </a></div>
                  </div>
                </div>
                <div class="pro-right">
                  <ul class="add-to-links">
                    <li><a href="#" class="link-wishlist">В список желаний</a></li>
                    <li><span class="separator">|</span> <a href="#" class="link-compare">Сравнить</a></li>
                  </ul>
                  <div class="add-to-box">
                    <div class="add-to-cart">
                      <label for="qty">Количество:</label>
                      <input name="qty" id="qty" maxlength="12" value="1" title="Qty" class="input-text qty" type="text" />
                      <a href="javascript:void(0)" id="reset_btn">Сброс</a>
                      <button type="button" title="Add to Cart" class="button btn-cart" onclick="productAddToCartForm.submit(this)"><span><span>В корзину</span></span></button>
                      <div class="add">›</div>
                      <div class="dec add">‹</div>
                    </div>
                  </div>
                </div>
              </div>
              <!--End Product Information Right--> 
              
              <!--Start Product Image Zoom Left-->
              <div class="product-img-box">
                <p class="product-image product-image-zoom"> <a href='./images/zoom1.jpg' class = 'cloud-zoom' id='zoom1'
            rel="adjustX: 10, adjustY:-4"> <img style="max-height:400px; width:400px;" src="./images/zoom1.jpg" alt='' title="Optional title display" /> </a> </p>
                <div class="more-views">
                  <div class=" jcarousel-skin-tango">
                    <ul id="more_view">
                      <li><a href='./images/zoom2.jpg' class='cloud-zoom-gallery' title='Thumbnail 1'
        	rel="useZoom: 'zoom1', smallImage: './images/zoom2.jpg' "> <img style="max-height:92px; width:92px;" src="./images/thumbzoom2.jpg" alt = "Thumbnail 1"/></a></li>
                      <li> <a href='./images/zoom3.jpg' class='cloud-zoom-gallery' title='Thumbnail 2'
        	rel="useZoom: 'zoom1', smallImage: ' ./images/zoom3.jpg'"> <img style="max-height:90px; width:90px;" src="./images/thumbzoom3.jpg" alt = "Thumbnail 2"/></a> </li>
                      <li><a href='./images/zoom4.jpg' class='cloud-zoom-gallery' title='Thumbnail 3'
        	rel="useZoom: 'zoom1', smallImage: './images/zoom4.jpg' "> <img style="max-height:90px; width:90px;" src="./images/thumbzoom4.jpg" alt = "Thumbnail 3"/></a></li>
                      <li><a href='./images/zoom5.jpg' class='cloud-zoom-gallery' title='Thumbnail 3'
        	rel="useZoom: 'zoom1', smallImage: './images/zoom5.jpg' "> <img style="max-height:90px; width:90px;" src="./images/thumbzoom5.jpg" alt = "Thumbnail 3"/></a></li>
                      <li><a href='./images/zoom6.jpg' class='cloud-zoom-gallery' title='Thumbnail 3'
        	rel="useZoom: 'zoom1', smallImage: './images/zoom6.jpg' "> <img style="max-height:90px; width:90px;" src="./images/thumbzoom6.jpg" alt = "Thumbnail 3"/></a></li>
                    </ul>
                  </div>
                </div>
                <script type="text/javascript">
     	jQuery(document).ready(function(){
			jQuery('#product_tabs_description').click(function(){
				jQuery('#product_tabs_description_contents').css('display','block');
				jQuery('#product_tabs_product').css('display','none');
				jQuery('#product_tabs_form_contents').css('display','none');
				});
			jQuery('#product_tabs_product_tag').click(function(){
				jQuery('#product_tabs_description_contents').css('display','none');
				jQuery('#product_tabs_product').css('display','block');
				jQuery('#product_tabs_form_contents').css('display','none');
				});
			jQuery('#product_tabs_form').click(function(){
				jQuery('#product_tabs_description_contents').css('display','none');
				jQuery('#product_tabs_product').css('display','none');
				jQuery('#product_tabs_form_contents').css('display','block');
				});
			
			});
     </script> 
              </div>
              <!--End Product Image Zoom Left-->
              <div class="clearer"></div>
            </div>
            
            <!--Start Product Tabs-->
            <div class="product-collateral">
              <ul class="product-tabs">
                <li id="product_tabs_description" class=" active first"><a href="javascript:void(0)">Описание</a></li>
              </ul>
              <div class="product-tabs-content" id="product_tabs_description_contents">
                <h2>Детали</h2>
                <div class="std"> Описание товара </div>
              </div>
            </div>
            <!--End Product Tabs--> 
          </div>
        </div>
      </div>
      <div style="display: none;" id="back-top"> <a href="#"><img alt="" src="images/backtop.gif" /></a> </div>
    </div>
    <!--END OF MAIN CONTENT--> 