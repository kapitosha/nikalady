<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 *
 * @property integer $Id
 * @property string $productСode
 * @property string $tytle
 * @property string $description
 * @property string $price
 * @property string $wholesaleprice
 * @property string $image
 * @property string $category
 * @property string $subcategory
 * @property string $image2
 * @property string $image3
 * @property string $image4
 * @property string $image5
 * @property string $pricefrom
 */
class BaseProducts extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseProducts the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'products';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['description', 'required'],
            ['productСode', 'length', 'max' => 50],
			['tytle, image, category, subcategory, image2, image3, image4', 'length', 'max' => 100],
			['price', 'length', 'max' => 10],
			['wholesaleprice', 'length', 'max' => 15],
			['image5', 'length', 'max' => 255],
            ['pricefrom', 'length', 'max' => 20],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'ID'),
            'productСode' => Yii::t('application', 'productCode'),
			'tytle' => Yii::t('application', 'Название'),
			'description' => Yii::t('application', 'Описание'),
			'price' => Yii::t('application', 'Цена'),
			'wholesaleprice' => Yii::t('application', 'Оптовая цена'),
			'image' => Yii::t('application', 'вставить изображение'),
			'category' => Yii::t('application', 'Категория'),
			'subcategory' => Yii::t('application', 'Подкатегория'),
			'image2' => Yii::t('application', 'вставить изображение'),
			'image3' => Yii::t('application', 'вставить изображение'),
			'image4' => Yii::t('application', 'вставить изображение'),
			'image5' => Yii::t('application', 'вставить изображение'),
            'pricefrom' => Yii::t('application', 'pricefrom'),
		];
    }
}

