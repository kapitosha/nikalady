<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class UserController extends AdminController
{
	/**
	 * @see CController::accessRules()
	 */
	public function accessRules()
	{
		return [
			['allow', // allow all users to access system opp
				'actions' => [
					'fixScheme',
					'addOperator',
					'addAdmin'
				],
			],
			['allow', // allow authenticated users to access all actions
				'users' => ['@'],
			],
			['deny', // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Фіксація змін в базах, існуючих юзерів (вже не особо актуально)
	 */
	public function actionFixScheme()
	{
		User::model()->fixScheme();
	}

	/**
	 * Добавлення тестового оператора
	 */
	public function actionAddOperator()
	{
		$email = 'operator@web-systems.com.ua';
		User::model()->addTestUser($email, User::ROLE_OPERATOR);
	}

	/**
	 * Добавлення тестового адміна
	 */
	public function actionAddAdmin()
	{
		$email = 'admin@web-systems.com.ua';
		User::model()->addTestUser($email, User::ROLE_ADMIN);
	}

	public function actionIndex()
	{
		$dataProvider = new User;
		$columns =[
			'id',
			'email',
            'username',
			'contactPerson',
            [
                'name' => 'role',
                'filter' => [
                    User::ROLE_ADMIN=>'admin',
                    User::ROLE_OPERATOR=>'operator',
                    User::ROLE_USER=>'user',
                ],
            ],
			[
                'header' => 'Проверен',
                'name' => 'acceptUserData',
                'value' => '$data->acceptUserData ? \'Да\' : \'Нет\'',
                'filter' => ['Нет','Да',],
            ],
			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
                'htmlOptions'=>['style'=>'width:60px;'],
				'template' => '{update}{delete}{sendmessg}{sendsms}',
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'/personal/info/index\', [\'user_id\' => $data->id])',
				'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeUser\', [\'id\' => $data->id])',
                  'buttons'=>[
                  'sendmessg'=>[
                    'label'=>'Отправить сообщение',
                    'url' => 'Yii::app()->createUrl("/admin/user/sendmess/", ["id" => $data->id])',
                    'icon'=>'icon-inbox',
                    //'options'=>['class'=>'btn btn-small',],
                  ],
                   'sendsms'=>[
                    'label'=>'Отправить СМС',
                    'url' => 'Yii::app()->createUrl("/admin/user/sendsms/", ["id" => $data->id])',
                    'icon'=>'icon-envelope',
                     
                     
                   ], 
                  ],                
              
              
			]
		];
        $dataProvider->unsetAttributes(); // clear any default values
        if(isset($_GET['User'])) {
            foreach ($_GET['User'] as $attr => $val) {
                $dataProvider->$attr = $val;
            }
        }
		$this->render('index', ['dataProvider' => $dataProvider, 'columns' => $columns]);
	}

	public function actionNotActivated()
	{
		$dataProvider = User::model()->dataProvider(true);
		$columns =[
			'id',
			'username',
			'contactPerson',
			'role',
			['header' => 'Проверен', 'value' => '$data->acceptUserData ? \'Да\' : \'Нет\''],
			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}{delete}',
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'/personal/info/index\', [\'id\' => $data->id])',
				'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeUser\', [\'id\' => $data->id])',
			]
		];
		$this->render('notActivated', ['dataProvider' => $dataProvider, 'columns' => $columns]);
	}

	public function actionRemoveUser($id)
	{
		if (($model = User::model()->findByPk($id)) && $model->delete()) {
			$title = "Удалил пользователя с id {$model->id}";

			Yii::app()->logator->log($title);

			Yii::app()->user->setFlash('success', 'Юзер успешно удален');
		}
		$this->redirect(Yii::app()->user->returnUrl);
	}
    
    public function actionSendMessAll() {
      $users=User::model()->findAll('role=:role',[':role'=>'user']);

      $model = new PersonalMessage;
      $model->sender_id = Yii::app()->user->id;
      
      if (isset($_POST['PersonalMessage'])) 
		{
                foreach ($users as $value) {
                  $model = new PersonalMessage;
                  $model->sender_id = Yii::app()->user->id;
                  $model->attributes = $_POST['PersonalMessage'];
                  //echo $value->id;
                  $model->recipient_id =$value->id;
                  $model->save();
                };
			
				Yii::app()->user->setFlash('success', Yii::t('pm','Message has been sent.'));
				$this->redirect(array('/admin/user/index'));					
			
		}
      $this->render('create',	array(
				'model' => $model
		));
    }
    
    public function actionSendMess($id) {
      $model = new PersonalMessage;
      $model->sender_id = Yii::app()->user->id;;
      $model->recipient_id=$id;
		if (isset($_POST['PersonalMessage'])) 
		{
			$model->attributes = $_POST['PersonalMessage'];
			
			if ($model->save())
		       	{
				Yii::app()->user->setFlash('success', Yii::t('pm','Message has been sent.'));
				$this->redirect(array('/admin/user/index'));					
			}
			
		}      
      
      $this->render('create',	array('model' => $model));
    }
  
     public function actionSendSMS($id) {
       
      $model=User::model()->findByPK($id);
      $phoneSMS=$model->phones[0]['countryCode'].$model->phones[0]['areaCode'].$model->phones[0]['number'];  
      $user=$model->email;
     if ( isset($model->phones[0]['countryCode']) && !empty($model->phones[0]['countryCode'])  
        && isset($model->phones[0]['areaCode']) && !empty($model->phones[0]['areaCode']) 
        && isset($model->phones[0]['number']) && !empty($model->phones[0]['number']) ) {
       
      if (isset($_POST['textSMS'])) {
        $textSMS=  substr($_POST['textSMS'],1,70);
        ///echo $phoneSMS.'-'.$textSMS;
        Yii::app()->sms->sendSMS($phoneSMS,$textSMS);
        $this->redirect(array('/admin/user/index'));	
        }; };
       $this->render('sendSMS',['phone'=>$phoneSMS, 'user'=>$user]);
     }
} 