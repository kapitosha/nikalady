<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

Yii::import('zii.widgets.CMenu');

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class UserMenu extends CMenu
{
	public $linkLabelTemplate;
	/**
	 * Recursively renders the menu items.
	 *
	 * @param array $items the menu items to be rendered recursively
	 */
	protected function renderMenuRecursive($items)
	{
		$count = 0;
		foreach ($items as $item) {
			$count++;
			$options = isset($item['itemOptions']) ? $item['itemOptions'] : array();

			echo CHtml::openTag('li', $options);

			$menu = $this->renderMenuItem($item, ($item['active'] && $this->activeCssClass != '') ? true : false);
			if (isset($this->itemTemplate) || isset($item['template'])) {
				$template = isset($item['template']) ? $item['template'] : $this->itemTemplate;
				echo strtr($template, array('{menu}' => $menu));
			} else {
				echo $menu;
			}

			echo CHtml::closeTag('li') . "\n";
		}
	}

	/**
	 * Renders the content of a menu item.
	 * Note that the container and the sub-menus are not rendered here.
	 *
	 * @param array $item the menu item to be rendered. Please see {@link items} on what data might be in the item.
	 * @return string
	 * @since 1.1.6
	 */
	protected function renderMenuItem($item, $isActive = false)
	{
		if (isset($item['url'])) {
			$label = $this->linkLabelWrapper === null ? $item['label'] : CHtml::tag($this->linkLabelWrapper, $this->linkLabelWrapperHtmlOptions, $item['label']);
			$options = isset($item['linkOptions']) ? $item['linkOptions'] : array();
			$class = array();
			if ($isActive === true) {
				$class[] = $this->activeCssClass;
			}
			if ($class !== array()) {
				if (empty($options['class'])) {
					$options['class'] = implode(' ', $class);
				} else {
					$options['class'] .= ' ' . implode(' ', $class);
				}
			}
			if (isset($this->linkLabelTemplate)) {
				$label = strtr($this->linkLabelTemplate, ['{label}' => $label]);
			}
			return CHtml::link($label, $item['url'], $options);
		} else {
			return CHtml::tag('span', isset($item['linkOptions']) ? $item['linkOptions'] : array(), $item['label']);
		}
	}
}