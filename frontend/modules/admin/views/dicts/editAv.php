<?php $this->breadcrumbs = [
	'Словарь состояний обработки' => ['listAvailability'],
($model->isNewRecord) ? 'Добавление' : 'Изменение' +' типы состояния обработки'
]; ?>

<?php
/**
 * @var CController $this
 * @var TbActiveForm $form
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
	'id' => get_class($model),
	'type' => 'horizontal',
	'htmlOptions' => ['class' => 'well'],
	'enableAjaxValidation' => true,
]); ?>

<?= $form->textFieldRow($model, 'name', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'note', ['class' => 'span4']); ?>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => ($model->isNewRecord) ? 'Добавить' : 'Сохранить'
		]); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'reset',
			'label' => 'Сбросить'
		]); ?>
	</div>

<?php $this->endWidget(); ?>
