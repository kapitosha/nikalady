<?php $this->beginContent('admin.views.layouts.main'); ?>

<?php $this->widget('zii.widgets.CMenu', [
	'items' =>[
		['label' => 'Марки', 'url' => ['/admin/dictTechnics/listBrand']],
		[
			'label' => DictModel::model()->getLabelByType(DictModel::TYPE_TRACTOR),
			'url' => ['/admin/dictTechnics/listTechnics', 'type' => DictModel::TYPE_TRACTOR]
		],
		[
			'label' => DictModel::model()->getLabelByType(DictModel::TYPE_COMBINE),
			'url' => ['/admin/dictTechnics/listTechnics', 'type' => DictModel::TYPE_COMBINE]
		],
		[
			'label' => DictModel::model()->getLabelByType(DictModel::TYPE_TRANSPORT),
			'url' => ['/admin/dictTechnics/listTechnics', 'type' => DictModel::TYPE_TRANSPORT]
		],
		[
			'label' => DictModel::model()->getLabelByType(DictModel::TYPE_SPRINKLER),
			'url' => ['/admin/dictTechnics/listTechnics', 'type' => DictModel::TYPE_SPRINKLER]
		],
		[
			'label' => DictModel::model()->getLabelByType(DictModel::TYPE_LOADER),
			'url' => ['/admin/dictTechnics/listTechnics', 'type' => DictModel::TYPE_LOADER]
		],
		[
			'label' => DictModel::model()->getLabelByType(DictModel::TYPE_AGGREGATE),
			'url' => ['/admin/dictTechnics/listTechnics', 'type' => DictModel::TYPE_AGGREGATE]
		],
		[
			'label' => DictModel::model()->getLabelByType(DictModel::TYPE_ANOTHER_TECHNIQUES),
			'url' => ['/admin/dictTechnics/listTechnics', 'type' => DictModel::TYPE_ANOTHER_TECHNIQUES]
		],
	],
	'htmlOptions' => ['class' => 'nav nav-tabs'],
]);?>

<?= $content; ?>

<?php $this->endContent(); ?>