<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * Is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 *
 * @see CController
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
abstract class CommonController extends CController
{
	/**
	 * @var string the default layout for the controller view.
	 * Defaults to '//layouts/main',
	 * @see CController::layout
	 */
	public $layout = '//layouts/main';

	/**
	 * @var array context menu items. This property will be assigned
	 * to {@link CMenu::items} or {@link TbNavbar::items}.
	 */
	public $menu = [];

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link TbBreadcrumbs::links}.
	 * Please refer to {@link TbBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = [];

	/**
	 * Performs the AJAX validation.
	 *
	 * @param $models CModel|array name models to be validation
	 * @param $nameForm string name form
	 * @return void
	 */
	protected function performAjaxValidation($models, $nameForm)
	{
    
		if (isset($_POST['ajax']) && $_POST['ajax'] === $nameForm) {
			echo CActiveForm::validate($models);
			Yii::app()->end();
		}
	}

	/**
	 * Get current version this application
	 *
	 * @return string current version application
	 */
	public function getVersion()
	{
		return '0.01';
	}
}