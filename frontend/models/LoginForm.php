<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class LoginForm extends CFormModel
{
	public $email;
	public $password;
	public $rememberUser;

	private $_identity;

	/**
	 * @see CModel::rules()
	 */
	public function rules()
	{
		return [
			['email, password', 'required'],
			['email', 'email'],
			['password', 'length', 'min' => 5, 'max' => 20],
			['password', 'authenticate'],
            ['rememberUser', 'boolean']
		];
	}

	/**
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels()
	{
		return [
			'email' => 'Email',
			'password' => Yii::t('application', 'Password'),
            'rememberUser'=>Yii::t('application','Remember User'),
		];
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute, $params)
	{
		$this->_identity = new UserIdentity($this->email, $this->password);
		if ($this->_identity->authenticate() !== UserIdentity::ERROR_NONE) {
            if ($this->_identity->errorCode == UserIdentity::ERROR_USERNAME_INVALID) {
                $this->addError('email', 'Не верный email');
            } elseif ($this->_identity->errorCode == UserIdentity::ERROR_PASSWORD_INVALID) {
                $this->addError('password', 'Не верный пароль');
			}
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 *
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if (!$this->validate()) {
			return false;
		}
		if ($this->_identity === null) {
			$this->_identity = new UserIdentity($this->email, $this->password);
			$this->_identity->authenticate();
		}
		if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
			$duration =$this->rememberUser ? 3600*24*30 : 3600*24*30;// 3600 * 24 * 30; // 30 days
                
			Yii::app()->user->login($this->_identity, $duration); 
            //print_r($_SESSION);
			return true;
		} else {
			return false;
		}
	}
}