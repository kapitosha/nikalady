<?php
return CMap::mergeArray(
	[
        // path aliases
        'aliases' => [
            'root' => realpath(__DIR__ . '/../..'),
            'common' => realpath(__DIR__ . '/../../common'),
        ],
        // autoloading model and component classes
        'import' => [
            'common.models.*',
            'common.models.structure.*',
            'common.components.*',
            'common.helpers.*',
            'application.extensions.*',
        ],
        // application components
        'components' => [
			'authManager' => [
				'class' => 'PhpAuthManager',
				'defaultRoles' => ['guest'],
			],
            // configuration urlManager
            'urlManager' => [
                'urlFormat' => 'path',
                'showScriptName' => false,
                'urlSuffix' => '/',
            ],
            'image'=>[
                'class'=>'application.extensions.image.CImageComponent',
                // GD or ImageMagick
                'driver'=>'GD',
                // ImageMagick setup path
                'params'=>array('directory'=>'/usr/local/bin'),
            ],
            // config db
            'db' => [
                'emulatePrepare' => true,
                'enableParamLogging' => YII_DEBUG,
                'charset' => 'utf8'
            ],

			'dbVK' => [
				'class'=>'CDbConnection',
				'emulatePrepare' => true,
				'enableParamLogging' => YII_DEBUG,
				'charset' => 'utf8'
			],
        ],
    ],
    ((YII_DEBUG && is_file(__DIR__ . '/env/dev.php')) ? require(__DIR__ . '/env/dev.php') : require(__DIR__ . '/env/prod.php'))
);


