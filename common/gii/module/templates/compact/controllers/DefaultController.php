<?php echo "<?php\n"; ?>
/**
* @link http://web-systems.com.ua/
* @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
*/

/**
* @author <?php echo Yii::app()->params['author'] . "\n"; ?>
* @since 0.1
*/
class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
}