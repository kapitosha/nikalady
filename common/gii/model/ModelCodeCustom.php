<?php
/**
 * @link http://web-systems.com.ua/
 * @link http://price-r.ru/
 * @copyright Copyright (c) 2013 - 2014 price-R.ru
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

Yii::import('system.gii.generators.model.ModelCode');

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class ModelCodeCustom extends ModelCode
{
	public $modelPath = 'common.models';
	public $baseClass = 'CActiveRecord';

	public function prepare()
	{
		if (($pos = strrpos($this->tableName, '.')) !== false) {
			$schema = substr($this->tableName, 0, $pos);
			$tableName = substr($this->tableName, $pos + 1);
		} else {
			$schema = '';
			$tableName = $this->tableName;
		}
		if ($tableName[strlen($tableName) - 1] === '*') {
			$tables = Yii::app()->{$this->connectionId}->schema->getTables($schema);
			if ($this->tablePrefix != '') {
				foreach ($tables as $i => $table) {
					if (strpos($table->name, $this->tablePrefix) !== 0) {
						unset($tables[$i]);
					}
				}
			}
		} else
			$tables = array($this->getTableSchema($this->tableName));

		$this->files = array();
		$templatePath = $this->templatePath;
		$this->relations = $this->generateRelations();

		foreach ($tables as $table) {
			$tableName = $this->removePrefix($table->name);
			$className = $this->generateClassName($table->name);
			$params = array(
				'tableName' => $schema === '' ? $tableName : $schema . '.' . $tableName,
				'modelClass' => $className,
				'columns' => $table->columns,
				'labels' => $this->generateLabels($table),
				'rules' => $this->generateRules($table),
				'relations' => isset($this->relations[$className]) ? $this->relations[$className] : array(),
				'connectionId' => $this->connectionId,
			);
			$this->files[] = new CCodeFile(
				Yii::getPathOfAlias($this->modelPath) . '/structure/' . $className . '.php',
				$this->render($templatePath . '/model.php', $params)
			);
		}
	}

	public function generateRules($table)
	{
		$rules = array();
		$required = array();
		$integers = array();
		$numerical = array();
		$length = array();
		$safe = array();
		foreach ($table->columns as $column) {
			if ($column->autoIncrement) {
				continue;
			}
			$r = !$column->allowNull && $column->defaultValue === null;
			if ($r) {
				$required[] = $column->name;
			}
			if ($column->type === 'integer') {
				$integers[] = $column->name;
			} elseif ($column->type === 'double') {
				$numerical[] = $column->name;
			} elseif ($column->type === 'string' && $column->size > 0) {
				$length[$column->size][] = $column->name;
			} elseif (!$column->isPrimaryKey && !$r) {
				$safe[] = $column->name;
			}
		}
		if ($required !== array()) {
			$rules[] = "['" . implode(', ', $required) . "', 'required']";
		}
		if ($integers !== array()) {
			$rules[] = "['" . implode(', ', $integers) . "', 'numerical', 'integerOnly' => true]";
		}
		if ($numerical !== array()) {
			$rules[] = "['" . implode(', ', $numerical) . "', 'numerical']";
		}
		if ($length !== array()) {
			foreach ($length as $len => $cols) {
				$rules[] = "['" . implode(', ', $cols) . "', 'length', 'max' => $len]";
			}
		}
		if ($safe !== array()) {
			$rules[] = "['" . implode(', ', $safe) . "', 'safe']";
		}

		return $rules;
	}

	protected function generateRelations()
	{
		if (!$this->buildRelations) {
			return array();
		}

		$schemaName = '';
		if (($pos = strpos($this->tableName, '.')) !== false) {
			$schemaName = substr($this->tableName, 0, $pos);
		}

		$relations = array();
		foreach (Yii::app()->{$this->connectionId}->schema->getTables($schemaName) as $table) {
			if ($this->tablePrefix != '' && strpos($table->name, $this->tablePrefix) !== 0) {
				continue;
			}
			$tableName = $table->name;

			if ($this->isRelationTable($table)) {
				$pks = $table->primaryKey;
				$fks = $table->foreignKeys;

				$table0 = $fks[$pks[0]][0];
				$table1 = $fks[$pks[1]][0];
				$className0 = $this->generateClassName($table0);
				$className1 = $this->generateClassName($table1);

				$unprefixedTableName = $this->removePrefix($tableName);

				$relationName = $this->generateRelationName($table0, $table1, true);
				$relations[$className0][$relationName] = "[self::MANY_MANY, '$className1', '$unprefixedTableName($pks[0], $pks[1])']";

				$relationName = $this->generateRelationName($table1, $table0, true);

				$i = 1;
				$rawName = $relationName;
				while (isset($relations[$className1][$relationName])) {
					$relationName = $rawName . $i++;
				}

				$relations[$className1][$relationName] = "[self::MANY_MANY, '$className0', '$unprefixedTableName($pks[1], $pks[0])']";
			} else {
				$className = $this->generateClassName($tableName);
				foreach ($table->foreignKeys as $fkName => $fkEntry) {
					// Put table and key name in variables for easier reading
					$refTable = $fkEntry[0]; // Table name that current fk references to
					$refKey = $fkEntry[1]; // Key in that table being referenced
					$refClassName = $this->generateClassName($refTable);

					// Add relation for this table
					$relationName = $this->generateRelationName($tableName, $fkName, false);
					$relations[$className][$relationName] = "[self::BELONGS_TO, '$refClassName', '$fkName']";

					// Add relation for the referenced table
					$relationType = $table->primaryKey === $fkName ? 'HAS_ONE' : 'HAS_MANY';
					$relationName = $this->generateRelationName($refTable, $this->removePrefix($tableName, false), $relationType === 'HAS_MANY');
					$i = 1;
					$rawName = $relationName;
					while (isset($relations[$refClassName][$relationName])) {
						$relationName = $rawName . ($i++);
					}
					$relations[$refClassName][$relationName] = "[self::$relationType, '$className', '$fkName']";
				}
			}
		}
		return $relations;
	}
}
