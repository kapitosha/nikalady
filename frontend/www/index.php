<?php
if ($_SERVER['SERVER_ADDR'] === '::1' || $_SERVER['SERVER_ADDR'] === '127.0.0.1') {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    // specify how many levels of call stack should be shown in each log message
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
}

// change the following paths if necessary
$yii = dirname(__FILE__) . '/../../common/vendor/yiisoft/yii/framework/yii.php';
$config = dirname(__FILE__) . '/../../frontend/config/main.php';

//$composerAutoload = dirname(__FILE__) . '/../../common/vendor/autoload.php';
//require_once($composerAutoload);
require_once($yii);
$app = Yii::createWebApplication($config);
$app->run();