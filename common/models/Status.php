<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "status".
 *
 * The followings are the available columns in table 'status':
 *
 * @property integer $Id
 * @property string $name
 * @property string $note
 */
class Status extends BaseStatus
{
     use RequestOfModel;
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseStatus the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'status';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
      ['name' , 'required'],
			['name', 'length', 'max' => 100],
			['note', 'length', 'max' => 255],
      ['name','validateName'],    
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'Id'),
			'name' => Yii::t('application', 'name'),
			'note' => Yii::t('application', 'note'),
		];
    }
    
    /**
     * @return CActiveDataProvider
     */
    public function dataProvider()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('Id', $this->Id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('note', $this->note, true);

        return new CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => 'Id',
            ]
        ]);
    }
  /**
   * проверка поля name на уникальность
   * @param type $attribute
   * 
   */  
  public function validateName ($attribute)
    {
      //$value=mb_strtolower($value);
      if ($attribute=='name') {
        $status=$this->find('LOWER(name)=:name',[':name'=> strtolower($this->name)]);
        if (sizeof ($status)>0) 
        {
            $this->addError('name', ' такое значение уже зарегистрировано!'.$status->name
              //$value
              );
        }    
      };
    
    }
    
  /**
   * устанавливаем параметры фильтрации  на обьект модели dataProvider
   * @param type $dataProvider
   */  
  public function setFilter($dataProvider) {
		$dataProvider->unsetAttributes(); // clear any default values

      if(isset($_GET[get_class($dataProvider)])) {
        foreach ($_GET[get_class($dataProvider)] as $attr => $val) {
          $dataProvider->$attr = $val;
        }
      }    
    }
    
    public function listStatus()
	{
		$criteria = new CDbCriteria;
		$criteria->select = ['Id', 'name'];
		$criteria->order = 'Id';
    $criteria->distinct=true;

		$listStatus = [];
		if ($models = $this->findAll($criteria)) {
			$listStatus = CHtml::listData($models, 'Id', 'name');
		}
		return $listStatus;
	}
    
    
}

