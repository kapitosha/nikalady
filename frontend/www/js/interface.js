jQuery.noConflict();

jQuery(function() {
				jQuery('#mix_container').gridnav({
					type	: {
						mode		: 'disperse', 	// use def | fade | seqfade | updown | sequpdown | showhide | disperse | rows
						speed		: 500,			// for fade, seqfade, updown, sequpdown, showhide, disperse, rows
						easing		: '',			// for fade, seqfade, updown, sequpdown, showhide, disperse, rows	
						factor		: '',			// for seqfade, sequpdown, rows
						reverse		: ''			// for sequpdown
					}
				});
			});

function empty(mixedValue) {
    switch (typeof mixedValue) {
        case 'undefined':
            return true;
        case 'object':
            for (val in mixedValue)return false;
            return true;
        case 'string':
        case 'array':
            return (mixedValue.length == 0 ? true : false);
        case 'boolean':
            return mixedValue;
        case 'number':
            return isNaN(mixedValue);
        case 'function':
            return false;
        default:
            return true;
    }
}

//функция загрузки файла
function sendForm(env){

var fd = new FormData();
//fd.append('img', $('#imgFile')[0].files[0]);
//fd.append('file', env.target.files[0]);
fd.append('file',env.files[0]);

$.ajax({
  cache: false,
  type: 'POST',
  url: '/personal/info/loadlogo/',
  data: fd,
  processData: false,
  contentType: false,
  //dataType: 'json',
  success: function(html) {
   $('#your-logo').html(html);
   $('#your-avatar').html(html);
    //alert('success');
  },
  error: function(html) {
    alert('ошибка обовления.');
  }
});
};    

//открытие окна загрузки файла
jQuery('#callBack').click(function(){
    jQuery.ajax({"type":"POST",
           "success":function(html) {
                   jQuery("#main_popup").html(html);
                   jQuery("#main_popup").arcticmodal();
                   },
           "url":"/site/callback/popup/1",
           "cache":false,
         });  
    //alert('btn submit');
});  
    //открытие окна загрузки файла
jQuery(document).on('click', '#formfeedback', function(){
    jQuery.ajax({"type":"POST",
           "success":function(html) {
                   jQuery("#main_popup").html(html);
                   jQuery("#main_popup").arcticmodal();
                   },
           "url":"/site/formfeedback/popup/1",
           "cache":false,
         });  
    //alert('btn submit');
}); 
    //открытие окна загрузки файла
jQuery(document).on('click', '#freeshipping', function(){
    jQuery.ajax({"type":"POST",
           "success":function(html) {
                   jQuery("#main_popup").html(html);
                   jQuery("#main_popup").arcticmodal();
                   },
           "url":"/site/reviewes/popup/1",
           "cache":false,
         });  
    //alert('btn submit');
}); 

jQuery(document).on('click', '.ask-popup .popup-close', function(){
  if (jQuery('#greetingInRegister .ask-popup').length>0) {
    jQuery('#greetingInRegister').remove()
    }
  else {
  jQuery('.ask-popup').remove();
   };  
});

// закриття всіх arcticmodal
jQuery(document).on("click",".arcticmodal-close", function () {
    jQuery.arcticmodal('close');
});

//очищаем поле для ввода email для рассылки
jQuery(document).on('focus', '#newsletter', function(){
  if(this.value=='Введите Ваш e-mail') {this.value=''};
});

//если поле email для рассылки пустое  вставляе описание
jQuery(document).on('blur', '#newsletter', function(){
  if(this.value=='') {this.value='Введите Ваш e-mail'};
  
    var email = jQuery("#newsletter").val();
  
    if(email != 0 || email!='Введите Ваш e-mail')
    {
    if(isValidEmailAddress(email))
    {
    jQuery("#validEmail").css({
        "background-image": "url('/images/validYes.png')"
    });
    } else {
    jQuery("#validEmail").css({
        "background-image": "url('/images/validNo.png')"
    });
    }
    } else {
    jQuery("#validEmail").css({
        "background-image": "none"
    }); 
    }
  
    });
  

//сохранаяем полученнный email из newsletter
jQuery(document).on('click', '#submitEmail', function(){
    var email=jQuery('#newsletter').val();
     
//    alert (email);
  
    jQuery.ajax({type: 'get',
            url:"/site/registration",
            cache:false,
            dataType: 'html',
            data:{"email":email},
//          "error": function(html) {
//                    alert('ошибка доступа '+html);
//                    },            
           success:function(html) {
                   jQuery("#main_popup").html(html);
                   jQuery("#main_popup").arcticmodal();
                   },
//           "complete":function(html) {
//                   alert('complete. Вы подписались на новости от Nika Lady '+html);
//                   },
         });  
    //alert('btn submit');
});  


//expires время жизни куков по умолчанию год = new Date( new Date().getTime() + 185*24*60*60*1000 );
function setCookie (name, value, path, expires, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "expires=" +new Date( new Date().getTime() + 365*24*60*60*1000 )) +
        ((path) ? "; path=" + path : "path=/") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}


function deleteCookie(name) {
  setCookie(name, "", { expires: -1 })
}

jQuery(function($){
//      if($('.phone').exists()){
//        $('.phone').each(function(){
//          $(this).mask("+38(999) 999-99-99");
//        });
//        $('.phone').attr({'placeholder':'+38(___) ___ __ __'});
//      }  
      
   //$(".phone").mask("+38(999) 999-9999");
   //$(".email").mask("+38(999) 999-9999");
   });

jQuery(document).ready(function() {  
  
	  jQuery('#slider').nivoSlider();
      jQuery('.mycarousel_related').jcarousel({
		scroll: 1,
        wrap: 'circular'
    });  
  
	  jQuery('#more_view').jcarousel({
        start: 1,
		scroll:1,
		wrap: 'circular'
    });  
  
  
 //      <!--Minicart JS--> 
	jQuery('.pagebox_btn').click(function(){
		if(parseInt($('.page_pan').css('left')) < 0)
		{
			jQuery('.page_pan').animate({ left: '0' }, 600, 'easeOutQuint');
		}
		else{
			jQuery('.page_pan').animate({ left: '-100px' }, 600, 'easeOutQuint');
			}
		});

  
 jQuery("#newsletter").keyup(function(){
    
    //alert('88');
    
    var email = jQuery("#newsletter").val();
  
    if(email != 0 || email!='Введите Ваш e-mail')
    {
    if(isValidEmailAddress(email))
    {
    jQuery("#validEmail").css({
        "background-image": "url('/images/validYes.png')"
    });
    } else {
    jQuery("#validEmail").css({
        "background-image": "url('/images/validNo.png')"
    });
    }
    } else {
    jQuery("#validEmail").css({
        "background-image": "none"
    }); 
    }
  
    });
  
    });
  
    function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
    }

