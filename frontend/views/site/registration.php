<?php if (Yii::app()->getRequest()->isAjaxRequest) : ?>
<span class="box-modal_close arcticmodal-close" >x</span>
<?php endif; ?>  

<div class="form popup">
  <?php $form = $this->beginWidget('CActiveForm', [
                  'id' => get_class($model),
                  'enableAjaxValidation' => true,
                  'clientOptions' => [
                          'validateOnSubmit' => true,
                          'validateOnChange' => true,
                  ],
                  'htmlOptions' => [
                         // 'enctype' => 'multipart/form-data',
                  ],
          ]); ?>
  
 <div class="row-high">
      <?= $form->labelEx($model, 'email', ['class'=>'left']); ?>
      <div class="right">
      <?= $form->textField($model, 'email', ['style'=>'margin-left: 0px; width: 98%;']); ?>
      </div>  
      <?= $form->error($model, 'email'); ?>
      <div class="clear"></div>  
    </div>
    <div class="control-group">
    <?= $form->labelEx($model, 'note', ['class'=>'control-label']); ?>
  <div class="controls">
    <!--<?= $form->telField($model, 'note', ['class' => 'right']); ?> -->
    <?= $form->textarea($model, 'note', ['cols' => '47', 'rows' => '8', 'class'=>'span4']) ;?>
    </div>
    <?= $form->error($model, 'note'); ?>
    <div class="clear"></div>  
  </div>  
  
  
    <div class="row btns">
      <input class="btn-opacity btn-do" type="submit" name="yt0" value="<?= Yii::t('application','btsend')?>"/>
    </div>    
    
  <?php $this->endWidget(); ?>  
</div>
