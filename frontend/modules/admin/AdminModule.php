<?php

/**
* @author Vadim Poplavskiy <im@demetrodon.com>
* @since 0.1
*/
class AdminModule extends CWebModule
{
	public $defaultController = 'products';

	public function init()
	{
		// import the module-level models and controllers
		$this->setImport([
			'admin.components.*',
			'admin.controllers.*',
		]);
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			Yii::app()->bootstrap;
			return true;
		} else {
			return false;
		}
	}
}
