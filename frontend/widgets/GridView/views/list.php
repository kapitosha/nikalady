<div class="text__content">
	<table>
		<tbody>
		<tr>
			<?php foreach ($headers as $header) : ?>
				<?= CHtml::tag('th', [], $header); ?>
			<?php endforeach; ?>
		</tr>
			<?php $this->widget('ListWidget', [
				'data' => $models,
				'itemView' => $itemView,
			]); ?>
		</tbody>
	</table>
</div>
<?php $this->widget('CLinkPager', [
	'pages' => $pages,
]); ?>