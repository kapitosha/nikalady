<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "registration".
 *
 * The followings are the available columns in table 'registration':
 *
 * @property integer $Id
 * @property string $email
 */
class BaseRegistration extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseRegistration the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'registration';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['email', 'length', 'max' => 100],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'ID'),
			'email' => Yii::t('application', 'ввод emailа'),
		];
    }
}

