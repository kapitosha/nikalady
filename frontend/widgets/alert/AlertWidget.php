<?php
/**
 * @link http://web-systems.com.ua/
 * @link http://price-r.ru/
 * @copyright Copyright (c) 2013 - 2014 price-R.ru
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class AlertWidget extends CWidget
{
	public function run()
	{
		$messages = Yii::app()->user->flashes;
		if (!empty($messages)) {
			foreach($messages as $message) {
				$this->render('alert', ['message' => $message]);
			}
		}
	}
} 