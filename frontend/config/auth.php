<?php return [
	'guest' => [
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Guest',
		'bizRule' => null,
		'data' => null
	],
	'user' => [
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'User',
		'children' => [
			'guest',
		],
		'bizRule' => 'return isset($params["user_id"])?Yii::app()->user->id==$params["user_id"]:true;',
		'data' => null
	],
	'operator' => [
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Operator',
		'children' => [
			'user',
		],
		'bizRule' => null,
		'data' => null
	],
	'admin' => [
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Administrator',
		'children' => [
			'operator',
		],
		'bizRule' => null,
		'data' => null
	],
];
