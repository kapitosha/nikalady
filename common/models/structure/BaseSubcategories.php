<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "subcategories".
 *
 * The followings are the available columns in table 'subcategories':
 *
 * @property integer $Id
 * @property string $name
 * @property string $note
 * @property integer $idcatgs
 */
class BaseSubcategories extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseSubcategories the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'subcategories';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['idcatgs', 'numerical', 'integerOnly' => true],
			['name, note', 'length', 'max' => 255],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'ID'),
			'name' => Yii::t('application', 'Ім'),
			'note' => Yii::t('application', 'примітки'),
			'idcatgs' => Yii::t('application', 'категория'),
		];
    }
}

