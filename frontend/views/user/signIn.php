<div id="formInRegister" class="curved-hz-1" style="width: 390px; left: 30%" >
        <div class="popup-logo">
            <img src="/images/logo.png" alt=""/>
        </div>
        
	<div class="form wide enter-popup">
		<?php /** @var $form CActiveForm */?>
		<?php $form = $this->beginWidget('CActiveForm', [
			'id' => get_class($model),
			'action' => '/user/signIn',
			'enableAjaxValidation' => true,
			'clientOptions' => [
				'validateOnSubmit' => true,
				'validateOnChange' => false, // true,
			],
		]); ?>

		<?= CHtml::hiddenField('from', 'page'); ?>
      <div class="form mini">
		<div class="row">
			<?= $form->labelEx($model, 'email'); ?>
			<?= $form->emailField($model, 'email'); ?>
            <br/>
			<?= $form->error($model, 'email'); ?>
			<div class="clear"></div>
		</div>

		<div class="row">
			<?= $form->labelEx($model, 'password'); ?>
			<?= $form->passwordField($model, 'password'); ?>
            <br/>
			<?= $form->error($model, 'password'); ?>
                          <span id="btn-remind-password" class="btn-remind-password">Забыли пароль?</span>
                                <div class="remember-user">
                                  <?php echo $form->checkBox($model,'rememberUser'); ?>
                                  <?php echo $form->label($model,'rememberUser'); ?>
                                  <?php echo $form->error($model,'rememberUser'); ?>
                                  
<!--                                  <input type="checkbox" checked id="rememberUser"/>
                                    <label for="rememberUser">Запомнить</label>-->
                                </div>
			<div class="clear"></div>
		</div>

      <div class="row buttons">
        <input class="btn-green" type="submit" value="Войти"/>
        <a class="back-to-main" href="/" title=""><i></i>На главную</a>		</div>
      </div>
		<?php $this->endWidget(); ?>
	</div>
	<div style="text-align: center;margin-top: 8px;">
    Для работы в этом разделе сайта Вам необходимо авторизироваться.
  </div>
</div>