<?php

/* 
 * контролер відображення словників
 */

class DictsController extends AdminController {
  
  //відображення гріда статусів
  public function actionListStatus()
	{
    //готую дані з моделі
    $dataProvider = new Status('dataProvider');
    //формую перелік колонок для відображення
		$columns = [
      ['name'=>'Id',
       'htmlOptions'=>['style'=>'width: 130px;'],
      ],
			'name',
			'note',
			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}{delete}',
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'editStatus\', [\'Id\' => $data->Id])',
				'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeStatus\', [\'Id\' => $data->Id])',
			]
		];    
    
    Status::model()->setFilter($dataProvider);
  
//		$dataProvider->unsetAttributes(); // clear any default values
//
//		if(isset($_GET[get_class($dataProvider)])) {
//			foreach ($_GET[get_class($dataProvider)] as $attr => $val) {
//				$dataProvider->$attr = $val;
//			}
//		}    
   //викликаю в'юшку і передаю підготовлені дані і опис колонок 
   $this->render('listSt',['dataProvider' => $dataProvider, 'columns' => $columns]); 
    }
    
  //видаляю запис з таблиці  
  public function actionRemoveStatus($Id)
	{
    //видаляю запис з таблиці
    Status::model()->deleteByPk($Id);
    //перемальовую форму
    $this->redirect('/');
  }  

  //викликаю форму зміни запису в таблиці  
  public function actionEditStatus($Id)
    {
        if (!$model = Status::model()->findByPk($Id)) {
                throw new CHttpException(404, 'Такого типа состояния не найдено в базе');
        }
        
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['listStatus']);
        }
        $this->render('editSt', ['model' => $model]);
    
    }
  
  public function actionAddStatus()
    {
  	$model = new Status;
                $this->performAjaxValidation($model, 'Status');
    
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['listStatus']);
        }
        $this->render('editSt', ['model' => $model]);
    
    }

  //відображення гріда категорій
  public function actionListCategories()
	{
    //готую дані з моделі
    $dataProvider = new Categories('dataProvider');
    //формую перелік колонок для відображення
		$columns = [
			['name'=>'Id',
       'htmlOptions'=>['style'=>'width: 130px;'],
      ],
			'name',
			'catid',
			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}{delete}',
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'editCategories\', [\'Id\' => $data->Id])',
				'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeCategories\', [\'Id\' => $data->Id])',
			]
		];    
    
   Categories::model()->setFilter($dataProvider); 
    
   //викликаю в'юшку і передаю підготовлені дані і опис колонок 
   $this->render('listCatgs',['dataProvider' => $dataProvider, 'columns' => $columns]); 
    }
    
  //видаляю запис з таблиці  
  public function actionRemoveCategories($Id)
	{
    //видаляю запис з таблиці
    Categories::model()->deleteByPk($Id);
    //перемальовую форму
    $this->redirect('/');
  }  

  //викликаю форму зміни запису в таблиці  
  public function actionEditCategories($Id)
    {
        if (!$model = Categories::model()->findByPk($Id)) {
                throw new CHttpException(404, 'Такой категоии товаров не найдено в базе');
        }
        
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['ListCategories']);
        }
        $this->render('editCatgs', ['model' => $model]);
    
    }
  
  public function actionAddCategories()
    {
  	$model = new Categories;
                $this->performAjaxValidation($model, 'Categories');
    
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['ListCategories']);
        }
        $this->render('editCatgs', ['model' => $model]);
    
    }
    
    
    //відображення гріда підкатегорій
  public function actionListSubcategories()
	{
    //готую дані з моделі
    $dataProvider = new Subcategories('dataProvider');
    //формую перелік колонок для відображення
		$columns = [
			['name'=>'Id',
       'htmlOptions'=>['style'=>'width: 130px;'],
      ],
      ['name'=>'catgrR.name', 'filter'=>true, 
       'header' => 'Категория',
       //'class'=>'bootstrap.widgets.TbRelationalColumn',
      ],
			'name',
			'note',
      //'idcatgs',
			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}{delete}',
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'editSubcategories\', [\'Id\' => $data->Id])',
				'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeSubcategories\', [\'Id\' => $data->Id])',
			]
		];    
    
    Subcategories::model()->setFilter($dataProvider); 
   //викликаю в'юшку і передаю підготовлені дані і опис колонок 
   $this->render('listScatgs',['dataProvider' => $dataProvider, 'columns' => $columns]); 
    }
    
  //видаляю запис з таблиці  
  public function actionRemoveSubcategories($Id)
	{
    //видаляю запис з таблиці
    Subcategories::model()->deleteByPk($Id);
    //перемальовую форму
    $this->redirect('/');
  }  

  //викликаю форму зміни запису в таблиці  
  public function actionEditSubcategories($Id)
    {
        if (!$model = Subcategories::model()->findByPk($Id)) {
                throw new CHttpException(404, 'Такой категоии товаров не найдено в базе');
        }
        
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['ListSubcategories']);
        }
        $this->render('editScatgs', ['model' => $model]);
    
    }
  
  public function actionAddSubcategories()
    {
  	$model = new Subcategories;
                $this->performAjaxValidation($model, 'Subcategories');
    
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['ListSubcategories']);
        }
        $this->render('editScatgs', ['model' => $model]);
    
    }
    public function actionListRegistration()
	{
    //готую дані з моделі
    $dataProvider = new Registration('dataProvider');
    //формую перелік колонок для відображення
		$columns = [
			['name'=>'Id',
       'htmlOptions'=>['style'=>'width: 130px;'],
      ],
			'email',
                        'note',
			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}{sendmessg}{delete}',
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'editRegistration\', [\'Id\' => $data->Id])',
				'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeRegistration\', [\'Id\' => $data->Id])',
                               'buttons'=>[
          'sendmessg'=>[
            'label'=>'Отправить сообщение',
            'url' => 'Yii::app()->createUrl("/admin/dicts/sendmess/", ["Id" => $data->Id])',
            'icon'=>'icon-envelope',
            //'options'=>['class'=>'btn btn-small',],
          ],
        ],  
			]
		];    
    
                Registration::model()->setFilter($dataProvider); 
   //викликаю в'юшку і передаю підготовлені дані і опис колонок 
   $this->render('listRegistration',['dataProvider' => $dataProvider, 'columns' => $columns]); 
    }
    
  //видаляю запис з таблиці  
  public function actionRemoveRegistration($Id)
	{
    //видаляю запис з таблиці
    Registration::model()->deleteByPk($Id);
    //перемальовую форму
    $this->redirect('/');
  }  

  //викликаю форму зміни запису в таблиці  
  public function actionEditRegistration($Id)
    {
        if (!$model = Registration::model()->findByPk($Id)) {
                throw new CHttpException(404, 'Такой категоии товаров не найдено в базе');
        }
        
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['ListRegistration']);
        }
        $this->render('editRegistration', ['model' => $model]);
    
    }
  
  public function actionAddRegistration()
    {
  	$model = new Registration;
                $this->performAjaxValidation($model, 'Registration');
    
        if ($model->setFromPost() && $model->save()) {
                $this->redirect(['ListRegistration']);
        }
        $this->render('AddRegistration', ['model' => $model]);
    
    }
     public function actionSendmess($Id)
    {
      $model = new Dispatch;
      
      if ($Imodel = Registration::model()->findbyPk($Id)) {
        if ($model->email==NULL) {$model->email=$Imodel->email;};
        if ($model->subject==NULL) {$model->subject='Сообщение сайта NikaLady';};
      };
      
        $this->performAjaxValidation($model, 'Dispatch');

        if ($model->setFromPost() && $model->save()) {
            Dispatch::model()->sendMessageToUser($model);
          $this->redirect(['ListRegistration']);
          Yii::app()->end();
        }
          $this->render('sendDispatch', ['model' => $model]);
    
    }
}