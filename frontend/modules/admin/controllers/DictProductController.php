<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * @author Vadim Poplavskiy <im@demetrodon.com>
 * @since 0.1
 */
class DictProductController extends AdminController
{
	public $layout = 'work';

	public function actionListWork($type)
	{
		$dataProvider = DictWork::model()->dataProvider($type);
		$columns = [
			'id',
			['name' => 'name', 'header' => 'Вид работы'],
			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}{delete}',
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'editWork\', [\'type\' =>$data->globalType, \'id\' => $data->id])',
				'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeWork\', [\'id\' => $data->id])',
			]
		];
		$this->render('listWork', ['dataProvider' => $dataProvider, 'columns' => $columns]);
	}

	public function actionAddWork($type)
	{
		$model = new DictWork;
        $this->performAjaxValidation($model, get_class($model));
		if ($model->setFromPost()) {
			$model->globalType = $type;
			if ($model->save()) {
                            Yii::app()->logator->log('Добавлена работа', 'Добавлена работа ' . CHtml::link($model->name, ['/admin/dictWork/EditWork', 'type'=>$type , 'id' => $model->id]));
				Yii::app()->user->setFlash('success', 'Работа успешно добавленна');
				$this->redirect(['listWork', 'type' => $type]);
			}
		}
		$this->render('addWork', ['model' => $model]);
	}

	public function actionEditWork($type, $id)
	{
		if (!$model = DictWork::model()->findByPk($id)) {
			throw new CHttpException(404, 'Такой работы не найдено в базе');
		}
		if ($model->setFromPost() && $model->save()) {
                        Yii::app()->logator->log('Изменена работа', 'Изменена работа ' . CHtml::link($model->name, ['/admin/dictWork/EditWork', 'type'=>$type , 'id' => $id]));
			Yii::app()->user->setFlash('success', 'Работа успешно измененна');
			$this->redirect(['listWork', 'type' => $type]);
		}
		$this->render('editWork', ['model' => $model]);
	}

	public function actionRemoveWork($id)
	{
		if (DictWork::model()->deleteByPk($id)) {
                    Yii::app()->logator->log('Удалена работа', 'Удалена работа ' . $id);
			Yii::app()->user->setFlash('success', 'Работа успешно удалена');
		}
		$this->redirect(Yii::app()->user->returnUrl);
	}

	public function actionListNeedWork($type)
	{
		$dataProvider = DictWorkNeed::model()->dataProvider($type);
		$columns = [
			'id',
			['name' => 'name', 'header' => 'Потребность'],
			[
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}{delete}',
				'updateButtonUrl' => 'Yii::app()->controller->createUrl(\'editNeedWork\', [\'type\' => $data->globalType, \'id\' => $data->id])',
				'deleteButtonUrl' => 'Yii::app()->controller->createUrl(\'removeNeedWork\', [\'id\' => $data->id])',
			]
		];
		$this->render('listNeedWork', ['dataProvider' => $dataProvider, 'columns' => $columns]);
	}

	public function actionAddNeedWork($type)
	{
		$model = new DictWorkNeed;
        $this->performAjaxValidation($model, get_class($model));
		if ($model->setFromPost()) {
			$model->globalType = $type;
			if ($model->save()) {
                            Yii::app()->logator->log('Добавлена потребность к работе', 'Добавлена потребность к работе ' . CHtml::link($model->name, ['/admin/dictWork/EditNeedWork', 'type'=>$type , 'id' => $model->id]));
				Yii::app()->user->setFlash('success', 'Потребность к работе успешно добавленна');
				$this->redirect(['listNeedWork', 'type' => $type]);
			}
		}
		$this->render('addNeedWork', ['model' => $model]);
	}

	public function actionEditNeedWork($type, $id)
	{
		if (!$model = DictWorkNeed::model()->findByPk($id)) {
			throw new CHttpException(404, 'Такой потребности не найдено в базе');
		}
		if ($model->setFromPost() && $model->save()) {
                    Yii::app()->logator->log('Изменена потребность к работе', 'Изменена потребность к работе ' . CHtml::link($model->name, ['/admin/dictWork/EditNeedWork', 'type'=>$type , 'id' => $model->id]));
			Yii::app()->user->setFlash('success', 'Потребность к работе успешно измененна');
			$this->redirect(['listNeedWork', 'type' => $type]);
		}
		$this->render('editNeedWork', ['model' => $model]);
	}

	public function actionRemoveNeedWork($id)
	{
		if (DictWorkNeed::model()->deleteByPk($id)) {
                    Yii::app()->logator->log('Удалена потребность к работе', 'Удалена потребность к работе ' . $id);
			Yii::app()->user->setFlash('success', 'Потребность успешно удалена');
		}
		$this->redirect(Yii::app()->user->returnUrl);
	}
} 
