<?php
/**
 * @link http://web-systems.com.ua/
 * @license http://opensource.org/licenses/BSD-3-Clause The BSD 3-Clause License
 */

/**
 * This is the model class for table "outmessg".
 *
 * The followings are the available columns in table 'outmessg':
 *
 * @property integer $Id
 * @property string $date
 * @property string $subject
 * @property string $text
 * @property string $email
 * @property integer $idmessg
 * @property string $note
 */
class BaseOutmessg extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
	 *
     * @param string $className active record class name.
     * @return BaseOutmessg the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'outmessg';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
			['date, text', 'required'],
			['idmessg', 'numerical', 'integerOnly' => true],
			['subject', 'length', 'max' => 100],
			['email', 'length', 'max' => 200],
			['note', 'safe'],
		];
    }
	
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
			'Id' => Yii::t('application', 'идентификатор'),
			'date' => Yii::t('application', 'дата'),
			'subject' => Yii::t('application', 'тема'),
			'text' => Yii::t('application', 'текст повідомлення'),
			'email' => Yii::t('application', 'електронна адреса одержувача'),
			'idmessg' => Yii::t('application', 'Idmessg'),
			'note' => Yii::t('application', 'примітка'),
		];
    }
}

