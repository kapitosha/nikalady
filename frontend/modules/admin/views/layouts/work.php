<?php $this->beginContent('admin.views.layouts.main'); ?>

<?php $this->widget('zii.widgets.CMenu', [
	'items' =>[
		[
			'label' => DictWork::model()->getLabelByType(DictWork::TYPE_OPERATION_TRACTOR),
			'url' => ['/admin/dictWork/listWork', 'type' => DictWork::TYPE_OPERATION_TRACTOR]
		],
		[
			'label' => DictWork::model()->getLabelByType(DictWork::TYPE_CLEANING),
			'url' => ['/admin/dictWork/listWork', 'type' => DictWork::TYPE_CLEANING]
		],
		[
			'label' => DictWork::model()->getLabelByType(DictWork::TYPE_TRANSPORTATION),
			'url' => ['/admin/dictWork/listWork', 'type' => DictWork::TYPE_TRANSPORTATION]
		],
		[
			'label' => DictWork::model()->getLabelByType(DictWork::TYPE_FILLING),
			'url' => ['/admin/dictWork/listWork', 'type' => DictWork::TYPE_FILLING]
		],
		[
			'label' => DictWork::model()->getLabelByType(DictWork::TYPE_SPRAYING),
			'url' => ['/admin/dictWork/listWork', 'type' => DictWork::TYPE_SPRAYING]
		],
//		[
//			'label' => DictWork::model()->getLabelByType(DictWork::TYPE_OTHER_SERVICES),
//			'url' => ['/admin/dictWork/listWork', 'type' => DictWork::TYPE_OTHER_SERVICES]
//		],
	],
	'htmlOptions' => ['class' => 'nav nav-tabs'],
]);?>

<?= $content; ?>

<?php $this->endContent(); ?>