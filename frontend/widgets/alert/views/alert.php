<?php
/**
 * @var CActiveForm $form
 */
?>
<div id="greetingInRegister" class="curved-hz-1 message-popup">
	<div class="ask-popup">
		<span class="popup-close">x</span>
		<p><?= $message; ?></p>
	</div>
</div>