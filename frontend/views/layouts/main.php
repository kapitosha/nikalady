<?php
Yii::app()->getClientScript()
    ->registerMetaTag('', 'description')
    ->registerMetaTag('', 'keywords')
    ->registerMetaTag(null, null, null, ['charset' => Yii::app()->charset])
    //->registerPackage('fancybox')
    ->registerScriptFile(Yii::app()->baseURL.'/js/jquery.arcticmodal-0.3.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/jquery.maskedinput.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/fadeSlideShow.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/interface.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/additional-methods.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/jquery.validate.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/prototype.js', CClientScript::POS_END)
    //->registerScriptFile(Yii::app()->baseURL.'/js/jquery-1.6.1.min.js', CClientScript::POS_END)
    //->registerScriptFile(Yii::app()->baseURL.'/js/common.js', CClientScript::POS_END)
    //->registerScriptFile(Yii::app()->baseURL.'/js/jquery.js',  CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/menu.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/banner_pack.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/light_box.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/cloud-zoom.1.0.2.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/jquery.easing.1.3.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/jquery.jcarousel.min.js', CClientScript::POS_END)
    ->registerScriptFile(Yii::app()->baseURL.'/js/jquery.mix.js', CClientScript::POS_END)
  ->registerCssFile(Yii::app()->baseURL.'/css/styles.css', 'all')
  ->registerCssFile(Yii::app()->baseURL.'/css/skin.css', 'all')
  ->registerCssFile(Yii::app()->baseURL.'/css/cloud-zoom.css', 'all')
  ->registerCssFile(Yii::app()->baseURL.'/css/light_box.css', 'all')
  ->registerCssFile(Yii::app()->baseURL.'/css/mix.css', 'all')
  ->registerCssFile(Yii::app()->baseURL.'/css/banner.css', 'all')
  ->registerCssFile(Yii::app()->baseURL.'/css/magicat.css' ,'all');
?><!DOCTYPE html>
<html>
<head>

    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <!--[if lt IE 9]>
      <link rel="stylesheet" href="css/ie-style.css">
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <link rel="stylesheet" href="/reject/reject.css" media="all" />
      <script type="text/javascript" src="/reject/reject.min.js"></script>
    <![endif]-->  
<script type="text/javascript" src="//vk.com/js/api/openapi.js?124"></script>
</head>
<body>

    <div class="wrapper">
      <h2><b>ПРИНОСИМ СВОИ ИЗВИНЕНИЯ!!! САЙТ В РЕЖИМЕ ДОРАБОТКИ. НЕ ВСЕ ФУНКЦИИ ДОСТУПНЫ.</b></h2>  
        <div class="page">   
    <header>
<!--        <a href="<?= Yii::app()->getBaseUrl(true) ?>" id="logo">
        <img src="<?= Yii::app()->getHomeUrl()?>/images/logo.png"/>
        </a>
-->
        <!--START OF HEADER-->
    <div class="header-container">
      <div class="quick-access"> 
        <!--Start Block Cart-->
        <div class="block block-cart header_cart">
          <div class="block-content_pan">
            <div class="summary trigger-minicart">
              <h2 class="classy"> <span class="cart_icon"><img alt="" src="<?= Yii::app()->getBaseUrl(true) ?>/images/basket2.png" /></span><a href="/checkout">Нет товаров в корзине</a> </h2>
            </div>
            <div class="remain_cart" id="minicart">
                <p class="empty">Товары в корзине отсутствуют.<</p>
              <div class="actions">
                <p class="subtotal"> <span class="label">Всего:</span> <span class="price">0.00</span> </p>
                <button type="button" title="Checkout" class="button"><span><span>Купить</span></span></button>
              </div>
            </div>
          </div>
        </div>  
        <!--End Block Cart--> 
        
        <!--Start Toplinks-->
        <ul class="links">
        </ul>
        <!--End Toplinks--> 
        
        <!--Start Language-->
        <div class="form-language">
          <div class="language" id="select-language">
            <a title="Українська" class="flag" href="#" style="background: url(<?= Yii::app()->getBaseUrl(true) ?>/images/Ukraine.png) no-repeat scroll 0% 0% transparent;">Ukraine</a> 
            <a title="Русский" class="flag" href="#" style="background: url(<?= Yii::app()->getBaseUrl(true) ?>/images/Russia.png) no-repeat scroll 0% 0% transparent;">Russian</a>
          </div>
        </div>
        <!--End Language--> 
      </div>
      <!--Start Header Content-->
      <div class="header">
        <ul id="logo">
          <!--Left-->
          <li class="head-container"> 
            <h2 class="classy">Добро пожаловать </h2>
            
            <p class="top-welcome-msg"> Сдесь должна быть акция </p>
          </li>
          <!--Left--> 
          <!--Center Logo-->
          <li class="logo-box" onClick='location.href="http://nika-lady"'>
            <h1 class="logo"><strong>Швейная фабрика детской и женской одежды Ника</strong><a href="#" title="Швейная фабрика детской и женской одежды Ника" class="logo"><img src="<?= Yii::app()->getBaseUrl(true) ?>/images/logo.png" alt="Nika" /></a></h1>
          </li>
          <!--Center Logo--> 
          
          <!--Right-->
          <li class="head-container"> 
              <h2 class="classy">Позвоните нам </h2> <br/>+380973496608,<br/>+380959365026,<br/>+380636309346
            <div id="search-bar">
              <div class="top-bar">
                <form id="search_mini_form" action="">
                  <div class="form-search">
                    <input
                     onfocus="if(this.value=='Поиск') {this.value=''};" onblur="if(this.value=='') {this.value='Поиск'};"
                     id="search" name="q" value="Поиск" class="input-text" type="text" />
                    <button type="submit" title="Найти" class="button">Go</button>
                  </div>
                </form>
              </div>
            </div>
          </li>
          <!--Right-->
        </ul>

        
        <!--Start of Navigation-->
        <div class="nav-container">
            <?php 
              $mnuItms[]=['label' => 'Главная', 'url' => 'http://nika-lady'];
              $mnuItms=array_merge($mnuItms,Categories::model ()->mnuCategories());
              $this->widget('zii.widgets.CMenu', [
                'items' =>$mnuItms,
                'submenuHtmlOptions'=>['class'=>'level0'],
                'id'=>"nav",
            ]);  
            ?>  

        </div>
.      </div>
      <!--End Header Content--> 
    </div>
    <!--END OF HEADER--> 
    </header>
    <div id="content" class="g-clearfix">

    <!--START OF MAIN CONTENT-->
    <div class="main-container col1-layout"> 

        <?= $content; ?>
    </div>
    <!--END OF MAIN CONTENT-->  

    </div>
    <!-- content end -->

    <footer>
    <!--START OF FOOTER-->
      <div class="footer-container">
        <div class="footer">
          <div class="f-fix">
            <div class="frame">.</div>
            <!--Shipping Block-->
            <div id="freeshipping" class="free-shipping"><a href="guestbook">Написать отзыв:</a><span>Для того чтобы оставить свой отзыв <span> нажмите на картинку </span></span></div>

            <!--Shipping Block-->
            
            
            <!--Newsletter
            <form method="post" id="newsletter-validate-detail" action="">
            -->
              <div class="form-subscribe">
                <div class="form-subscribe-header">Подписаться на рассылку</div>
                <div class="input-box">
                  <input 
                  value="Введите Ваш e-mail" name="email" id="newsletter" title="Подписаться на рассылку" class="input-text required-entry validate-email" type="text" />
                  
                  <button style="width: 80px;" id="submitEmail"  title="Отправить" class="button"><span>Отправить</span></button>
                  <span id="validEmail" style="width: 16px;"></span>
                </div>
              </div>
            <!--Newsletter
            </form> 
            -->
           
          </div>
            <div class="f-left bottom_links">
              <div class="footer-content">
                <div class="block_1" id="formfeedback">
                  <ul class="footer_links">     
                      <li><h3><span class="hreff" ><?= Yii::t('application','layMailBack') ?></span></h3></li>
                  </ul>   
                </div>
                <div class="block_1" id="callBack">
                  <ul class="footer_links">     
                    <li><h3><span class="hreff" ><?= Yii::t('application','layCallBack') ?></span></h3></li>
                  </ul>   
                </div>
                <div class="block_1">
                  <ul class="footer_links">
                    <li> <a href="/about"><h3><?= Yii::t('application','layAbout') ?></h3></a></li>
                  </ul>
                </div>
                <div class="block_2">
                  <h3>Мы в соцсетях</h3>
                  <!-- VK Widget -->
                  <div id="vk_contact_us"></div> 
                    <script type="text/javascript">
                    VK.Widgets.ContactUs("vk_contact_us", {redesign: 1, text: "Вконтакте"}, -123465085); 
                    </script>
                </div>
                <!--Payment Icons-->
              </div>
            </div>
          </div>
        </div>
    
        <address>
            <p> <center>© 2016 NikaShop. Дизайн & Разработка: Karmeluck</center></p>
        </address>
      </div>
    <!--END OF FOOTER-->
    </footer>
  </div>  
  

<!-- popup forms -->    
<div class="g-hidden">
    <div id="main_popup" class="box-modal">
    </div>
</div>

    <!--pages box-->
<div class="page_pan" style="left:-100px;">
	<div class="page_box">
    	<a href="http://www.magicdesignlabs.in/santana_html/index.html">Главная</a>
        <a href="http://www.magicdesignlabs.in/santana_html/category.html">Категории</a>
        <a href="http://www.magicdesignlabs.in/santana_html/product.html">Товары</a>
        <a href="http://www.magicdesignlabs.in/santana_html/checkout.html">Заказать</a>
        <a href="http://www.magicdesignlabs.in/santana_html/contacts.html">Свяжитесь с нами</a>
    </div>
    <div class="pagebox_btn">
        <span><b>С</b></span>
        <span><b>Т</b></span>
        <span><b>Р</b></span>
        <span><b>А</b></span>
        <span><b>Н</b></span>
        <span><b>И</b></span>
        <span><b>Ц</b></span>
        <span><b>Ы</b></span>
    </div>
    
</div>
<!--end pages box-->
</body>
</html>