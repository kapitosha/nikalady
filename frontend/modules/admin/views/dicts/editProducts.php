<?php $this->breadcrumbs = [
	'Словарь работы с товарами' => ['ListProducts'],
($model->isNewRecord) ? 'Добавление' : 'Изменение' +' типы товаров'
]; ?>

<?php
/**
 * @var CController $this
 * @var TbActiveForm $form
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
	'Id' => get_class($model),
	'type' => 'horizontal',
	'htmlOptions' => ['class' => 'well'],
	'enableAjaxValidation' => true,
]); ?>
<?= $form->textFieldRow($model, 'productСode', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'tytle', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'description', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'price', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'wholesaleprice', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'image', ['class' => 'Item extends CActiveRecord']); ?>
<?= $form->textFieldRow($model, 'image2', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'image3', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'image4', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'image5', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'category', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'subcategory', ['class' => 'span4']); ?>
<?= $form->textFieldRow($model, 'wholesalepriseFrom', ['class' => 'span4']); ?>
		 
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => ($model->isNewRecord) ? 'Добавить' : 'Сохранить'
		]); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'reset',
			'label' => 'Сбросить'
		]); ?>
	</div>

<?php $this->endWidget(); ?>
