<?php $this->breadcrumbs = [
	'Словарь работы с обратными звонками' => ['ListFeedback'],
($model->isNewRecord) ? 'Добавление' : 'Изменение' +' типы звонков'
]; ?>

<?php
/**
 * @var CController $this
 * @var TbActiveForm $form
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
	'Id' => get_class($model),
	'type' => 'horizontal',
	'htmlOptions' => ['class' => 'well'],
	'enableAjaxValidation' => true,
]); ?>

<?= $form->textFieldRow($model, 'name', ['class' => 'span4']); ?>

<div class="control-group" >
    <?= $form->labelex($model, 'phonenumber', ['class' => 'control-label'] ); ?>
    <div class="control" >
        <?php
            $this->widget('CMaskedTextField', array(
                'model' => $model,
                'attribute' => 'phonenumber',
                'mask' => '+38(999) 999-99-99',
                'placeholder' => '*',
                'completed' => 'function(){console.log("ok");}',
                'htmlOptions' => ['class' => 'span4',
                                  'style'=>'width: 26.5%; margin-left: 20px;'
                                 ],
            ));
        ?>
      <?= $form->error($model, 'phonenumber' ); ?>
    </div> 
</div>


<?= $form->textFieldRow($model, 'note', ['class' => 'span4']); ?>
<?= $form->dropDownListRow($model, 'idstatus', Status::model()->listStatus(), ['class' => 'span4']); ?>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => ($model->isNewRecord) ? 'Добавить' : 'Сохранить'
		]); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', [
			'buttonType' => 'reset',
			'label' => 'Сбросить'
		]); ?>
	</div>

<?php $this->endWidget(); ?>
