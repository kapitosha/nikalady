<?php
/**
 * Description of Products
 *
 * @author vitaliyk
 */
class ProductsController extends Controller
{
  
public function actionIndex() {
  echo 'help me';
}
  
public function actionGetProduct($Id) {
  if (!$model = Products::model()->findByPk($Id)) {
      throw new CHttpException(404, 'Товар не найден в базе');
  };
  
    $this->renderPartial('getproduct', ['model' => $model], false, true);
    
    //$this->render('getproduct', ['model' => $model]);
}

public function actionProductDetail($Id) {
  if (!$model = Products::model()->findByPk($Id)) {
      throw new CHttpException(404, 'Товар не найден в базе');
  };
  
    //$this->renderPartial('getproduct', ['model' => $model], false, true);
    
    $this->render('productdetail', ['model' => $model]);
}

public function actionProductbyCategory($Id) {
 $criteria = new CDbCriteria;
  $criteria->addColumnCondition(['subcategory'=>$Id]);

  $criteria->with=[ 'catgrR' => ['together' => true],
                    'subcatgrR' => ['together' => true]];
  
  //print_r($criteria); exit;
  
  $dataProvider= new CActiveDataProvider('Products', [
                'criteria' => $criteria,
                //'pagination' => ['pageSize' => 15,],
            ]);
    
  $model=Categories::model()->findbyPk($Id)->name;
  
  $this->render('productlist', ['dataProvider'=>$dataProvider, 'model' => $model]);
   }

  
public function actionProductbySubCategory($Id) {
  $criteria = new CDbCriteria;
  $criteria->addColumnCondition(['subcategory'=>$Id]);

  $criteria->with=[ 'catgrR' => ['together' => true],
                    'subcatgrR' => ['together' => true]];
  
  //print_r($criteria); exit;
  
  $dataProvider= new CActiveDataProvider('Products', [
                'criteria' => $criteria,
                //'pagination' => ['pageSize' => 15,],
            ]);
    
  $model=Subcategories::model()->findbyPk($Id)->name;
  
  $this->render('productlist', ['dataProvider'=>$dataProvider, 'model' => $model]);
  }
  
}
